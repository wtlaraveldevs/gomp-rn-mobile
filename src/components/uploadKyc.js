import React, {Component} from 'react';
import { 
  ActivityIndicator, StyleSheet, Text, View, ScrollView,
  Dimensions, Image, Alert
} from 'react-native';
import { 
    Container, Content, Header, Button, Card, CardItem, 
    Textarea, Input, Form, Item, Picker 
} from 'native-base';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Grid, Row, Col } from 'react-native-easy-grid';
import Dialog, { DialogContent } from 'react-native-popup-dialog';

// Scaling Button
import ScalingButton from './util/ScalingButton';
import { uploadKycDocument } from '../redux/actions';
import ImagePicker from 'react-native-image-crop-picker';

const options = {
    title: 'Select Avatar',
    customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };

class UploadKyc extends Component {

    state = {
        img : '',
        visible: true,
        liked: false,
        loader: false,
        avatarSource: '',
        selected: '',

        userInfo: {},
        // variables
        title: '',
        description: '',
        location: 'UK',
        categories_id: '',
        // description_label : "Tell people what you are saving for and why you would like a Gift Of Money instead ofa present Eg. Wedding, deposite for a house, honeymoon, holiday, skiing lessons, driving lessons, University."
        description_label : "Hi Everyone, I don't expect a gift from anyone, but to those of you who intend to buy me a gift anyway, may I please request GYFTmoni instead of a present to pay for .......... e.g. deposite for my first house? It's something I really want and your Gift Of Money will help me get it. Thank you. Love,"
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.kycResponse.status == true)
        {
            this.setState({loader: false});
            Alert.alert(
                "Success",
                "You have successfully uploaded KYC document. It will take 24 hours to validate the document. Thank you.",
                [
                  {
                    text: "Ok",
                    onPress: () => this.props.navigation.pop(),
                    style: "cancel",
                  },
                ],
            );
        }
        else if(nextProps.kycResponse.status == false)
        {
            this.setState({loader: false})
            setTimeout(()=>{
                alert(nextProps.kycResponse.message);
            }, 400);
        }
    }

    _openCameraPicker = () =>{
        ImagePicker.openCamera({
            width: 330,
            height: 500,
            includeBase64: true,
        }).then(image => {
            console.log(image);
            // const source = { uri: `data:${image.mime};base64,${image.data}`};
            const source = { uri: image.path };
            this.setState({avatarSource: source, img: image.path});
        });
    }

    _openImagePicker = () =>{
        ImagePicker.openPicker({
            width: 330,
            height: 500,
            includeBase64: true,
        }).then(image => {
            console.log(image);
            // const source = { uri: `data:${image.mime};base64,${image.data}`};
            const source = { uri: image.path };
            this.setState({avatarSource: source, img: image.path});
        });
    }

    _renderHeader(){
        return(
            <Header style={{marginLeft: -2, width: '102%', backgroundColor: 'transparent' }}>
            <Grid>
                <Row>
                <Col size={15} style={{justifyContent: 'center', alignItems: 'center'}}>
                    <Button transparent onPress={()=>{
                        this.props.navigation.goBack()
                    }}>
                    <Icon name="chevron-left" size={30} color={'#FCC438'} />
                    </Button>
                </Col>
                <Col size={65} style={{justifyContent: 'center', alignItems: 'center'}}>
                    <Text style={{fontSize: 20}}>Upload Document</Text>
                </Col>
                <Col size={15} style={{justifyContent: 'center', alignItems: 'center'}}>
                </Col>
                </Row>
            </Grid>
            </Header>
        )
    }

    _renderCreationView(){
        return (
            <View style={{flex: 1, backgroundColor: 'transparent'}}>
                <ScrollView style={{padding: 15, }}>
                <Form style={{alignItems: 'center'}}>
                    <Card style={{flex: 1, width: 330,  height: Dimensions.get('screen').height / 1.6, marginBottom: 10}}>
                        <CardItem button onPress={() => {
                            Alert.alert(
                                "",
                                "Select an option to upload an image.",
                                [
                                  {
                                    text: "Open Gallery",
                                    onPress: () => this._openImagePicker() ,
                                    style: "cancel"
                                  },
                                  { text: "Open Camera", onPress: () => this._openCameraPicker() }
                                ]
                            );
                            ;
                        }} style={{flex: 1, justifyContent:'center', alignItems:'center'}}>
                            {
                                this.state.avatarSource != '' ?
                                <Image source={this.state.avatarSource} style={{width: 330,  height: Dimensions.get('screen').height / 1.6}} /> 
                                :
                                <View style={{justifyContent:'center', alignItems:'center'}}>
                                    <View style={{padding: 20, borderRadius: 50, borderColor: '#f6b93b', justifyContent:'center', alignItems:'center',
                                    borderWidth: 2, width:100, marginBottom: 10}}>
                                        <Icon name={"cloud-upload"} size={50} color={"#f6b93b"} />
                                    </View>
                                    <View style={{justifyContent:'center', alignItems:'center'}}>
                                        <Text style={{color: '#ddd', width: 200, textAlign:'center'}}>Upload front image of the document.</Text>
                                        {/* <Text style={{color: '#ddd'}}>OR</Text>
                                        <Text style={{color: '#ddd'}}>The Occasion?</Text> */}
                                    </View>
                                </View>
                            }
                        </CardItem>
                    </Card>
                </Form>
                <View style={{alignItems: 'center', justifyContent:'center'}}>
                    {
                        this.state.loader == false ?
                        <ScalingButton label="Give" 
                            onPress={this._handlePress.bind(this)}
                            styles={{button: styles.animated_button, label: styles.button_label}}
                        >
                            <Text style={{color: 'white', fontSize: 20}}>Upload Document</Text>
                        </ScalingButton>
                        :
                        <View styles={{button: styles.animated_button, label: styles.button_label}} >
                            <ActivityIndicator color={"white"} size={"large"} />
                        </View>
                    }
                </View>
                </ScrollView>
            </View>
        )
    }

    _handlePress = () => {
        // alert("welcome");
        if(this.state.img == '')
        {
            alert("Please provide a valid Image.")
        }
        else {

            this.setState({loader: true})

            var obj = {
                "kyc_doc": this.state.img,
                "user_id": this.props.loggedUser.id
            };

            this.props.uploadDocument(obj);
        }
        // this.props.navigation.navigate('Payment');
    }

    _renderLoader = () =>{
        return(
            <View>
                <Dialog
                visible={this.state.loader}
                dialogStyle={{width: 200, padding: 20}}
                >
                <DialogContent>
                    <ActivityIndicator size={"large"} color={"golden"} />
                    <Text style={{fontSize: 20}}>Please wait...</Text>
                </DialogContent>
                </Dialog>
            </View>
        )
    }

    render() {
        return (
            <Container>
                <View style={{width: Dimensions.get('window').width, height: 60, backgroundColor: '#f6b93b', 
                justifyContent:'center', alignItems: 'flex-end', flexDirection:'row', paddingBottom: 15}}>
                    <Image source={require('../assets/img/logo.png')} style={{width: 32, height: 32, paddingBottom: 3, 
                        marginRight: 4, tintColor: '#fff'}} />
                    <Image source={require('../assets/img/complete_logo.png')} style={{width: 144, height: 20, tintColor: '#fff'}} />
                </View>
                <Content contentContainerStyle={{flex: 1}}>
                    {this._renderHeader()}
                    {this._renderLoader()}
                    {this._renderCreationView()}
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    bottom:{
        flex: 1, backgroundColor: 'white', 
        paddingTop: 25,
        paddingBottom: 20,
        paddingLeft: 30,
        paddingRight: 30, 
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30
    },
    title:{
        fontSize: 26,
        fontWeight: 'bold',
        color: 'black'
    },
    animated_button : {
        backgroundColor: '#f6b93b',
        borderRadius: 10,
        width: 300
    },
    button_label: {
        color: '#fff',
        fontSize: 18
    },
    uploadAvatar : {
        width: Dimensions.get('window').width,
        height: 150
    }
});

const mapStateToProps = (state) => {
    return {
        loggedUser: state.loggedUser,
        kycResponse: state.kycResponse
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        uploadDocument: (x) => {
            dispatch(uploadKycDocument(x))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UploadKyc);
