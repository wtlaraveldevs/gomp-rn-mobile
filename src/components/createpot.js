import React, {Component} from 'react';
import { 
  ActivityIndicator, StyleSheet, Text, View, ScrollView,
  Dimensions, Image, TouchableWithoutFeedback, Platform
} from 'react-native';
import { 
    Container, Content, Header, Button, Card, CardItem, 
    Textarea, Input, Form, Item, Picker 
} from 'native-base';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Grid, Row, Col } from 'react-native-easy-grid';
import Dialog, { DialogContent } from 'react-native-popup-dialog';

// Scaling Button
import ScalingButton from './util/ScalingButton';
import { createPot, getAccountInfo } from '../redux/actions';
import ImagePicker from 'react-native-image-crop-picker';

const options = {
    title: 'Select Avatar',
    customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };

class CreatePot extends Component {

    state = {
        img : '',
        visible: true,
        liked: false,
        loader: false,
        avatarSource: '',
        selected: '',

        userInfo: {},
        // variables
        title: '',
        description: '',
        location: 'UK',
        categories_id: '',
        // description_label : "Tell people what you are saving for and why you would like a Gift Of Money instead ofa present Eg. Wedding, deposite for a house, honeymoon, holiday, skiing lessons, driving lessons, University."
        description_label : "Hi Everyone, I don't expect a gift from anyone, but to those of you who intend to buy me a gift anyway, may I please ask for GYFTmoni instead of a present to put towards for .......... e.g. deposite for my first house? It's something I really want and your Gift Of Money will help me get it. Thank you. Love,"
    }

    componentWillMount(){
        // get account information from server
        this.props.getAccount({ user_id: this.props.loggedUser.id });
    }

    componentWillReceiveProps(nextProps){
        console.log("NextProps",nextProps);
        if(nextProps.selected_gom != this.state.img){
            this.setState({img: this.props.selected_gom.large_image})
        }
        if(nextProps.newPot == true)
        {
            this.setState({loader: false});
            setTimeout(()=>{
                this.props.navigation.replace("PotView");
            }, 250);
        }
        else if(nextProps.apiStatus == "failed")
        {
            this.setState({loader: false});
            var message = nextProps.apiMessage != "" || nextProps.apiMessage != null ? nextProps.apiMessage : "We are unable to create the pot at the moment. Please contact GYFTmoni administration. Thank you." ;
            alert(message);
        }

        if(nextProps.userInfo != this.state.userInfo){
            this.setState({
                fullname: nextProps.userInfo.name,
                selected: nextProps.userInfo.country_id,
                description_label: this.state.description_label + " " +  nextProps.userInfo.name
            })
        }
    }

    _openImagePicker = () =>{
        ImagePicker.openPicker({
            width: 300,
            height: 400,
            // cropping: true,
            includeBase64: true,
          }).then(image => {
            console.log(image);
            // const source = { uri: `data:${image.mime};base64,${image.data}`};
            const source = { uri: image.path };
            this.setState({avatarSource: source, img: image.path});
          });
        // ImagePicker.showImagePicker(options, (response) => {
        //     console.log('Response = ', response);
          
        //     if (response.didCancel) {
        //       console.log('User cancelled image picker');
        //     } else if (response.error) {
        //       console.log('ImagePicker Error: ', response.error);
        //     } else if (response.customButton) {
        //       console.log('User tapped custom button: ', response.customButton);
        //     } else {
        //       const source = { uri: response.uri };
          
        //       // You can also display the image using data:
        //       // const source = { uri: 'data:image/jpeg;base64,' + response.data };
          
        //       this.setState({
        //         avatarSource: source,
        //       });
        //     }
        // });
    }

    _renderHeader(){
        return(
            <View style={{height: 60}}>
                <Grid style={{alignItems: 'center', paddingHorizontal: 10}}>
                    <Row>
                    <Col size={15} style={{justifyContent: 'center', alignItems: 'center'}}>
                        <Button transparent onPress={()=>{
                            this.props.navigation.goBack()
                        }}>
                        <Icon name="chevron-left" size={30} color={'#FCC438'} />
                        </Button>
                    </Col>
                    <Col size={65} style={{justifyContent: 'center', alignItems: 'center'}}>
                        <Text style={{fontSize: 20}}>Create GYFTmoni Pot</Text>
                    </Col>
                    <Col size={15} style={{justifyContent: 'center', alignItems: 'center'}}>
                    </Col>
                    </Row>
                </Grid>
            </View>
        )
    }

    _renderCreationView(){
        return (
            <View style={{flex: 1, backgroundColor: 'transparent'}}>
                <ScrollView style={{padding: 15, flex: 1}} keyboardShouldPersistTaps={true}>
                <Form>
                    <Card style={{flex: 1, height: 200, marginBottom: 20}}>
                        <CardItem button onPress={this._openImagePicker.bind(this)} style={{flex: 1, justifyContent:'center', alignItems:'center'}}>
                            {
                                this.state.avatarSource != '' ?
                                <Image source={this.state.avatarSource} style={styles.uploadAvatar} /> 
                                :
                                <View style={{justifyContent:'center', alignItems:'center'}}>
                                    <View style={{padding: 20, borderRadius: 50, borderColor: '#f6b93b', justifyContent:'center', alignItems:'center',
                                    borderWidth: 2, width:100, marginBottom: 10}}>
                                        <Icon name={"cloud-upload"} size={50} color={"#f6b93b"} />
                                    </View>
                                    <View style={{justifyContent:'center', alignItems:'center'}}>
                                        <Text style={{color: '#ddd', width: 200, textAlign:'center'}}>Upload an image of what you want to buy OR of the occasion OR just any image you like.</Text>
                                        {/* <Text style={{color: '#ddd'}}>OR</Text>
                                        <Text style={{color: '#ddd'}}>The Occasion?</Text> */}
                                    </View>
                                </View>
                            }
                        </CardItem>
                    </Card>
                    <Item regular style={{backgroundColor: 'transparent', borderColor: 'black', borderRadius: 7,  
                    marginBottom: 10,  width: Dimensions.get('screen').width - 30, paddingLeft: 10 }}>
                    <Icon name="email" size={23} style={{marginRight: 5, color: '#f6b93b'}} />
                    <Input placeholderTextColor={'#ddd'} style={{color: 'black'}} placeholder={"Name your GYFTmoni Pot"} 
                    onChangeText = {(text)=>{this.setState({title: text})}} />
                    </Item>
                    <Item regular style={{backgroundColor: 'transparent', borderColor: 'black', borderRadius: 7,
                    marginBottom: 10,  width: Dimensions.get('screen').width - 30, paddingLeft: 10 }}>
                        <Icon name="lock" size={23} style={{marginRight: 5,color: '#f6b93b'}} />
                        <Picker
                        note
                        mode="dropdown"
                        itemTextStyle={{color: 'black'}}
                        style={{ width: undefined }}
                        iosIcon={<Icon name="ios-arrow-down" size={30} />}
                        selectedValue={this.state.selected}
                        onValueChange={(item)=>{this.setState({selected: item})}}
                        >
                            <Picker.Item label="Select a category" value="-1" />
                            <Picker.Item label="Birthday" value="1"/>
                            <Picker.Item label="Baby Shower" value="13"/>
                            <Picker.Item label="Eid, Christmas" value="16" />
                            <Picker.Item label="Engagement" value="12"/>
                            <Picker.Item label="New Job" value="18" />
                            <Picker.Item label="New Year" value="15" />
                            <Picker.Item label="Retirement" value="11"/>
                            <Picker.Item label="University / Graduation" value="3"/>
                            <Picker.Item label="Wedding" value="2"/>
                            <Picker.Item label="Other" value="14"/>
                        </Picker>
                    {/* <Icon name="lock" size={23} style={{marginRight: 5,color: '#f6b93b'}} />
                    <Input placeholderTextColor={'#f6b93b'} style={{color: 'black'}} 
                    placeholder={"Select one"} onChangeText = {()=>{}} /> */}
                    </Item>
                    {/* <Item regular style={{backgroundColor: 'transparent', borderColor: 'black', borderRadius: 7,
                    marginBottom: 10,  width: Dimensions.get('screen').width - 30, paddingLeft: 10 }}>
                        <Icon name="message-text" size={23} style={{marginRight: 5,color: '#f6b93b'}} />
                        <Input placeholderTextColor={'#f6b93b'} style={{color: 'black'}} placeholder={"Location"} 
                        onChangeText = {(text)=>{this.setState({location: text})}} />
                    </Item> */}
                    <Item regular style={{backgroundColor: 'transparent', borderColor: 'black', borderRadius: 7,
                    marginBottom: 10,  width: Dimensions.get('screen').width - 30, paddingLeft: 10 }}>
                        <Icon name="message-text" size={23} style={{marginRight: 5,color: '#f6b93b'}} />
                        <Textarea rowSpan={5} placeholderTextColor={'#ddd'} style={{flex: 1, color: 'black', flexWrap: 'wrap'}}
                        value={this.state.description_label} onChangeText = {(text)=>{ this.setState({description_label: text})}} />
                    </Item>
                </Form>
                <View style={{alignItems: 'center', justifyContent:'center'}}>
                    {
                        this.state.loader ? 
                        <View style={{backgroundColor: "#f6b93b", borderRadius: 30,
                        width: 300, paddingVertical: 15, marginTop: 20}}
                        >
                            <ActivityIndicator color={"#fff"} size={"large"} />
                        </View> :
                        <ScalingButton label="Give" 
                            onPress={this._handlePress.bind(this)}
                            styles={{button: styles.animated_button, label: styles.button_label}}
                        >
                            <Text style={{color: 'white', fontSize: 20}}>Create</Text>
                        </ScalingButton>
                    }
                </View>
                </ScrollView>
            </View>
        )
    }

    _handlePress = () => {
        // alert("welcome");
        if(this.state.img == '')
        {
            alert("Please upload a valid image. thank you.")
        }
        else if(this.state.title == '')
        {
            alert("Please add a valid title. thank you.")
        }
        else if(this.state.description_label == '')
        {
            alert("Please add a valid description. thank you.")
        }
        else if(this.state.location == '')
        {
            alert("Please add a valid location. thank you.")
        }
        else if(this.state.selected == '')
        {
            alert("Please select a valid category. thank you.")
        }
        else {

            this.setState({ loader: true })

            var obj = {
                "title": this.state.title,
                "description": this.state.description_label,
                // "photo": this.state.avatarSource,
                "photo": this.state.img,
                "location": this.state.location,
                "categories_id": this.state.selected,
                "user_id": this.props.loggedUser.id
            };
            this.props.createNewPot(obj);
        }
        // this.props.navigation.navigate('Payment');
    }

    _renderLoader = () =>{
        return(
            <View>
                <Dialog
                visible={this.state.loader}
                dialogStyle={{width: 200, padding: 20}}
                >
                <DialogContent>
                    <ActivityIndicator size={"large"} color={"golden"} />
                    <Text style={{fontSize: 20}}>Please wait...</Text>
                </DialogContent>
                </Dialog>
            </View>
        )
    }

    render() {
        return (
            <Container>
                <Content contentContainerStyle={{flex: 1}}>
                    <View style={{width: Dimensions.get('window').width, height: Platform.OS == "android" ? 60 : 85, backgroundColor: '#f6b93b', 
                    justifyContent:'center', alignItems: 'flex-end', flexDirection:'row', paddingBottom: 15,
                    }}>
                        {/* <Image source={require('../assets/img/logo.png')} style={{width: 32, height: 32, paddingBottom: 3, 
                            marginRight: 4, tintColor: '#fff'}} /> */}
                        <Image source={require('../assets/img/complete_logo.png')} style={{width: 144, height: 20, tintColor: '#fff'}} />
                    </View>
                    {this._renderHeader()}
                    {/* {this._renderLoader()} */}
                    {this._renderCreationView()}
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    bottom:{
        flex: 1, backgroundColor: 'white', 
        paddingTop: 25,
        paddingBottom: 20,
        paddingLeft: 30,
        paddingRight: 30, 
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30
    },
    title:{
        fontSize: 26,
        fontWeight: 'bold',
        color: 'black'
    },
    animated_button : {
        backgroundColor: '#f6b93b',
        borderRadius: 10,
        width: 300
    },
    button_label: {
        color: '#fff',
        fontSize: 18
    },
    uploadAvatar : {
        width: Dimensions.get('window').width,
        height: 200
    }
});

const mapStateToProps = (state) => {
    return {
        selected_gom: state.selected_gom,
        loggedUser: state.loggedUser,
        userInfo: state.userInfo,
        newPot: state.newPot,
        apiStatus: state.apiStatus,
        apiMessage: state.apiMessage
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        createNewPot: (x) => {
            dispatch(createPot(x))
        },
        getAccount: (x) => {
            dispatch(getAccountInfo(x))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreatePot);
