/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
  Image, StyleSheet, TouchableWithoutFeedback, Text, View, ActivityIndicator, SafeAreaView,
  Dimensions,
  Platform
} from 'react-native';
import { Container, Content,  Button, Label } from 'native-base';
import { Grid, Row, Col } from 'react-native-easy-grid';
import Dialog, { DialogContent } from 'react-native-popup-dialog';
import { FlatList } from 'react-native-gesture-handler';
import { connect } from 'react-redux';
import { search, setGOM } from '../../redux/actions';
import SearchBar from 'react-native-search-bar';
import { config } from '../../config/index'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

class Search extends Component {

  state = {
    visible: false,
    resultText:"",
    results: [],
    hasData: false,
    _initiatedSearch: false,
  }
  constructor(props)
  {
    super(props);
  }

  componentWillReceiveProps(nextProps){
    this.setState({ _initiatedSearch: true })
    if(nextProps.searchResult.length > 0){
      this.setState({ results: nextProps.searchResult, visible: false, hasData: true })
    }
    else{
      this.setState({ results: [], visible: false, hasData: false })
    }
  }

  _handleTextChange(text){
    this.setState({resultText: text})
  }

  _renderLoader = () =>{
    return(
      <View>
        <Dialog
          visible={this.state.visible}
          dialogStyle={{width: 200, padding: 20}}
        >
          <DialogContent>
            <ActivityIndicator size={"large"} color={"golden"} />
            <Text style={{fontSize: 20}}>Please wait...</Text>
          </DialogContent>
        </Dialog>
      </View>
    )
  }

  render() {
    const navigation = this.props.navigation;
    return (
        <Container>
          <View style={{width: Dimensions.get('window').width, height: Platform.OS == "android" ? 60 : 85, backgroundColor: '#f6b93b', 
        justifyContent:'center', alignItems: 'flex-end', flexDirection:'row', 
        paddingBottom: 15,}}>
            {/* <Image source={require('../../assets/img/logo.png')} style={{width: 32, height: 32, paddingBottom: 3, 
                marginRight: 4, tintColor: '#fff'}} /> */}
            <Image source={require('../../assets/img/complete_logo.png')} style={{width: 144, height: 20, tintColor: '#fff'}} />
        </View>
        <Content style={{paddingLeft: 10, paddingRight: 10}}>
          <SafeAreaView>
          {this._renderLoader()}
          <View style={{flexDirection:'row', alignItems:'center', marginTop: 5}}>
            <Button transparent style={{marginTop: 10}} onPress={()=>{
                this.props.navigation.goBack()
            }}>
              <Icon name="chevron-left" size={30} color={'#FCC438'} />
            </Button>
            <SearchBar
              cancelButtonText={"Cancel"}
              ref="searchBar"
              placeholder="Search"
              style={{height: 60, width: '90%'}}
              onChangeText={(text)=>{ this.setState({resultText: text }) }}
              onSearchButtonPress={()=>{ 
                this.setState({visible: true})
                this.props.searchQuery({text: this.state.resultText})
              }}
              onCancelButtonPress={()=>{}}
            />
          </View>
          {
            this.state._initiatedSearch == false ?
            null
            : this.state.hasData ? 
            <FlatList
              data={this.state.results}
              extraData={this.state}
              keyExtractor={item => item.id}
              renderItem={(item)=>{
                let url = `${config.smallImg}/${item.item.small_image}`;
                return(
                  <TouchableWithoutFeedback onPress={()=>{
                    this.props.setGM(item.item);
                    setTimeout(()=>{
                      this.props.navigation.navigate('PotView');
                    }, 500);
                  }}>
                    <Row style={{borderBottomColor: '#ddd', borderBottomWidth: 1, paddingBottom:10}}>
                        <Col size={40} style={{marginRight: 7}}>
                            <Image source={{uri: url}}
                                style = {{height: 130, resizeMode : 'stretch', margin: 5 }}/>
                        </Col>
                        <Col size={60} style={{padding:5}}>
                            <Row size={15}>
                            </Row>
                            <Row size={85}>
                            <Label style={{fontSize: 24, color: 'black'}}>
                                {item.item.title}
                            </Label>
                            </Row>
                        </Col>
                    </Row>
                  </TouchableWithoutFeedback>
                )
              }}
            /> :  
            <View style={{width: Dimensions.get('screen').width, height: Dimensions.get('screen').height/1.5,
             justifyContent: 'center', alignItems: 'center'}}>
              <View style={{borderWidth: 2, borderColor:'#f45302', padding: 25, borderRadius: 50, marginBottom: 10  }}>
                <Icon name="magnify" size={50} color={'#f45302'} />
              </View>
              <Text style={{fontSize: 20}}>No result have been found.</Text>
            </View>
          }
          </SafeAreaView>
        </Content>
      </Container>
      );  
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  resultText: {
    fontSize: 30,
    color: 'black'
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

const mapStateToProps = (state) => {
  return {
    searchResult: state.searchResult
  }
}

const mapDispatchToProps = (dispatch) => {
  return{
    searchQuery: (query) => {
      dispatch(search(query))
    },
    setGM: (data) =>{
      dispatch(setGOM(data))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Search);
