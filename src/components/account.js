import React, {Component} from 'react';
import { 
  ActivityIndicator, StyleSheet, Text, View, ScrollView,
  Dimensions, Image, Platform, 
} from 'react-native';
import { 
    Container, Content, Header, Button, Card, CardItem, 
    Input, Form, Item, Picker 
} from 'native-base';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Grid, Row, Col } from 'react-native-easy-grid';
import Dialog, { DialogContent } from 'react-native-popup-dialog';

// Scaling Button
import ScalingButton from './util/ScalingButton';
import { getAccountInfo, updateAccountInfo, resetRedirectionToMyAccount } from '../redux/actions';
import ImagePicker from 'react-native-image-crop-picker';

const options = {
    title: 'Select Avatar',
    customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };

class Account extends Component {

    state = {
        img : '',
        visible: true,
        loader: false,
        avatarSource: '',
        selected: '',

        fullname: '',
        country: '',
        password : '',
        confirmpassword: '',
        userInfo: '',
    }

    componentWillMount(){
        // get account information from server
        this.props.getAccount({ user_id: this.props.loggedUser.id });
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.userInfo != this.state.userInfo){
            this.setState({
                fullname: nextProps.userInfo.name,
                selected: nextProps.userInfo.country_id,
            })
        }
    }

    _openImagePicker = () =>{
        ImagePicker.openPicker({
            width: 300,
            height: 400,
            // cropping: true,
            includeBase64: true,
        }).then(image => {
            console.log(image);
            // const source = { uri: `data:${image.mime};base64,${image.data}`};
            const source = { uri: image.path };
            this.setState({avatarSource: source});
        });
    }

    _renderHeader(){
        return(
            <Header style={{marginLeft: -2, width: '102%', backgroundColor: 'transparent' }}>
            <Grid>
                <Row>
                <Col size={15} style={{justifyContent: 'center', alignItems: 'center'}}>
                    <Button transparent onPress={()=>{
                        this.props.reset();
                        setTimeout(()=>{
                            this.props.navigation.goBack()
                        }, 50)
                    }}>
                    <Icon name="chevron-left" size={30} color={'#FCC438'} />
                    </Button>
                </Col>
                <Col size={65} style={{justifyContent: 'center', alignItems: 'center'}}>
                    <Text style={{fontSize: 20}}>My Account</Text>
                </Col>
                <Col size={15} style={{justifyContent: 'center', alignItems: 'center'}}>
                </Col>
                </Row>
            </Grid>
            </Header>
        )
    }

    _renderCreationView(){
        return (
            <View style={{flex: 1, backgroundColor: 'transparent'}}>
                <ScrollView style={{padding: 15, }}>
                <Form>
                    <Card style={{flex: 1, height: 150, marginBottom: 20}}>
                        <CardItem button onPress={this._openImagePicker.bind(this)} style={{flex: 1, justifyContent:'center', alignItems:'center'}}>
                            {
                                this.state.avatarSource != '' ?
                                <Image source={this.state.avatarSource} style={styles.uploadAvatar} /> 
                                :
                                <View style={{padding: 20, borderRadius: 50, borderColor: '#f6b93b', 
                                    borderWidth: 2}}>
                                    <Icon name={"camera"} size={50} color={"#f6b93b"} />
                                </View>
                            }
                        </CardItem>
                    </Card>
                    <Item regular style={{backgroundColor: 'transparent', borderColor: 'black', borderRadius: 7,  
                    marginBottom: 10,  width: Dimensions.get('screen').width - 30, paddingLeft: 10 }}>
                        <Icon name="account" size={23} style={{marginRight: 5, color: '#f6b93b'}} />
                        <Input placeholderTextColor={'#f6b93b'} style={{color: 'black'}} placeholder={"Full Name"} 
                        onChangeText = {(text)=>{ this.setState({ fullname: text }) }} value={this.state.fullname} />
                    </Item>
                    <Item regular style={{backgroundColor: 'transparent', borderColor: 'black', borderRadius: 7,
                    marginBottom: 10,  width: Dimensions.get('screen').width - 30, paddingLeft: 10 }}>
                        <Icon name="alpha-c" size={23} style={{marginRight: 5, marginLeft: 3,color: '#f6b93b'}} />
                        <Picker
                            note
                            mode="dialog"
                            itemTextStyle={{color: 'black'}}
                            style={{ width: undefined }}
                            selectedValue={this.state.selected}
                            onValueChange={(item)=>{this.setState({ selected: item });}}
                        >
                            <Picker.Item label="Select One" value="-1" />
                            <Picker.Item value="-1" label="Select your country"/>
                            <Picker.Item value="3" label="Afghanistan"/>
                            <Picker.Item value="4" label="Albania" />
                            <Picker.Item value="5" label="Algeria"></Picker.Item>
                            <Picker.Item value="6" label="American Samoa"></Picker.Item>
                            <Picker.Item value="7" label="Andorra"></Picker.Item>
                            <Picker.Item value="8" label="Angola"></Picker.Item>
                            <Picker.Item value="9" label="Anguilla"></Picker.Item>
                            <Picker.Item value="10" label="Antarctica"></Picker.Item>
                            <Picker.Item value="11" label="Antigua and/or Barbuda"></Picker.Item>
                            <Picker.Item value="12" label="Argentina"></Picker.Item>
                            <Picker.Item value="13" label="Armenia"></Picker.Item>
                            <Picker.Item value="14" label="Aruba"></Picker.Item>
                            <Picker.Item value="15" label="Australia"></Picker.Item>
                            <Picker.Item value="16" label="Austria"></Picker.Item>
                            <Picker.Item value="17" label="Azerbaijan"></Picker.Item>
                            <Picker.Item value="18" label="Bahamas"></Picker.Item>
                            <Picker.Item value="19" label="Bahrain"></Picker.Item>
                            <Picker.Item value="20" label="Bangladesh"></Picker.Item>
                            <Picker.Item value="21" label="Barbados"></Picker.Item>
                            <Picker.Item value="22" label="Belarus"></Picker.Item>
                            <Picker.Item value="23" label="Belgium"></Picker.Item>
                            <Picker.Item value="24" label="Belize" ></Picker.Item>
                            <Picker.Item value="25" label="Benin"></Picker.Item>
                            <Picker.Item value="26" label="Bermuda"></Picker.Item>
                            <Picker.Item value="27" label="Bhutan"></Picker.Item>
                            <Picker.Item value="28" label="Bolivia"></Picker.Item>
                            <Picker.Item value="29" label="Bosnia and Herzegovina"></Picker.Item>
                            <Picker.Item value="30" label="Botswana"></Picker.Item>
                            <Picker.Item value="31" label="Bouvet Island"></Picker.Item>
                            <Picker.Item value="32" label="Brazil"></Picker.Item>
                            <Picker.Item value="33" label="British lndian Ocean Territory"></Picker.Item>
                            <Picker.Item value="34" label="Brunei Darussalam"></Picker.Item>
                            <Picker.Item value="35" label="Bulgaria"></Picker.Item>
                            <Picker.Item value="36" label="Burkina Faso"></Picker.Item>
                            <Picker.Item value="37" label="Burundi"></Picker.Item>
                            <Picker.Item value="38" label="Cambodia"></Picker.Item>
                            <Picker.Item value="39" label="Cameroon"></Picker.Item>
                            <Picker.Item value="2" label="Canada"></Picker.Item>
                            <Picker.Item value="40" label="Cape Verde"></Picker.Item>
                            <Picker.Item value="41" label="Cayman Islands"></Picker.Item>
                            <Picker.Item value="42" label="Central African Republic"></Picker.Item>
                            <Picker.Item value="43" label="Chad"></Picker.Item>
                            <Picker.Item value="44" label="Chile"></Picker.Item>
                            <Picker.Item value="45" label="China"></Picker.Item>
                            <Picker.Item value="46" label="Christmas Island"></Picker.Item>
                            <Picker.Item value="47" label="Cocos (Keeling) Islands"></Picker.Item>
                            <Picker.Item value="48" label="Colombia"></Picker.Item>
                            <Picker.Item value="49" label="Comoros"></Picker.Item>
                            <Picker.Item value="50" label="Congo"></Picker.Item>
                            <Picker.Item value="51" label="Cook Islands"></Picker.Item>
                            <Picker.Item value="52" label="Costa Rica"></Picker.Item>
                            <Picker.Item value="53" label="Croatia (Hrvatska)"></Picker.Item>
                            <Picker.Item value="54" label="Cuba"></Picker.Item>
                            <Picker.Item value="55" label="Cyprus"></Picker.Item>
                            <Picker.Item value="56" label="Czech Republic"></Picker.Item>
                            <Picker.Item value="57" label="Denmark"></Picker.Item>
                            <Picker.Item value="58" label="Djibouti"></Picker.Item>
                            <Picker.Item value="59" label="Dominica"></Picker.Item>
                            <Picker.Item value="60" label="Dominican Republic"></Picker.Item>
                            <Picker.Item value="61" label="East Timor"></Picker.Item>
                            <Picker.Item value="62" label="Ecuador"></Picker.Item>
                            <Picker.Item value="63" label="Egypt"></Picker.Item>
                            <Picker.Item value="64" label="Egypt">x</Picker.Item>
                            <Picker.Item value="65" label="Equatorial Guinea"></Picker.Item>
                            <Picker.Item value="66" label="Eritrea"></Picker.Item>
                            <Picker.Item value="67" label="Estonia"></Picker.Item>
                            <Picker.Item value="68" label="Ethiopia"></Picker.Item>
                            <Picker.Item value="69" label="Falkland Islands (Malvinas)"></Picker.Item>
                            <Picker.Item value="70" label="Faroe Islands"></Picker.Item>
                            <Picker.Item value="71" label="Fiji"></Picker.Item>
                            <Picker.Item value="72" label="Finland"></Picker.Item>
                            <Picker.Item value="73" label="France"></Picker.Item>
                            <Picker.Item value="74" label="France, Metropolitan"></Picker.Item>
                            <Picker.Item value="75" label="French Guiana"></Picker.Item>
                            <Picker.Item value="76" label="French Polynesia" />
                            <Picker.Item value="77" label="French Southern Territories"></Picker.Item>
                            <Picker.Item value="78" label="Gabon"></Picker.Item>
                            <Picker.Item value="79" label="Gambia"></Picker.Item>
                            <Picker.Item value="80" label="Georgia"></Picker.Item>
                            <Picker.Item value="81" label="Germany"></Picker.Item>
                            <Picker.Item value="82" label="Ghana"></Picker.Item>
                            <Picker.Item value="83" label="Gibraltar"></Picker.Item>
                            <Picker.Item value="84" label="Greece"></Picker.Item>
                            <Picker.Item value="85" label="Greenland"></Picker.Item>
                            <Picker.Item value="86" label="Grenada"></Picker.Item>
                            <Picker.Item value="87" label="Guadeloupe"></Picker.Item>
                            <Picker.Item value="88" label="Guam"></Picker.Item>
                            <Picker.Item value="89" label="Guatemala"></Picker.Item>
                            <Picker.Item value="90" label="Guinea"></Picker.Item>
                            <Picker.Item value="91" label="Guinea-Bissau"></Picker.Item>
                            <Picker.Item value="92" label="Guyana"></Picker.Item>
                            <Picker.Item value="93" label="Haiti"></Picker.Item>
                            <Picker.Item value="94" label="Heard and Mc Donald Islands"></Picker.Item>
                            <Picker.Item value="95" label="Honduras"></Picker.Item>
                            <Picker.Item value="96" label="Hong Kong"></Picker.Item>
                            <Picker.Item value="97" label="Hungary"></Picker.Item>
                            <Picker.Item value="98" label="Iceland"></Picker.Item>
                            <Picker.Item value="99" label="India"></Picker.Item>
                            <Picker.Item value="100" label="Indonesia"></Picker.Item>
                            <Picker.Item value="101" label="Iran (Islamic Republic of)"></Picker.Item>
                            <Picker.Item value="102" label="Iraq"></Picker.Item>
                            <Picker.Item value="103" label="Ireland"></Picker.Item>
                            <Picker.Item value="104" label="Israel"></Picker.Item>
                            <Picker.Item value="105" label="Italy"></Picker.Item>
                            <Picker.Item value="106" label="Ivory Coast"></Picker.Item>
                            <Picker.Item value="107" label="Jamaica"></Picker.Item>
                            <Picker.Item value="108" label="Japan"></Picker.Item>
                            <Picker.Item value="109" label="Jordan"></Picker.Item>
                            <Picker.Item value="110" label="Kazakhstan"></Picker.Item>
                            <Picker.Item value="111" label="Kenya"></Picker.Item>
                            <Picker.Item value="112" label="Kiribati"></Picker.Item>
                            <Picker.Item value="113" label="Korea, Democratic People's Republic of"></Picker.Item>
                            <Picker.Item value="114" label="Korea, Republic of"></Picker.Item>
                            <Picker.Item value="115" label="Kosovo"></Picker.Item>
                            <Picker.Item value="116" label="Kuwait"></Picker.Item>
                            <Picker.Item value="117" label="Kyrgyzstan"></Picker.Item>
                            <Picker.Item value="118" label="Lao People's Democratic Republic"></Picker.Item>
                            <Picker.Item value="119" label="Latvia"></Picker.Item>
                            <Picker.Item value="120" label="Lebanon"></Picker.Item>
                            <Picker.Item value="121" label="Lesotho"></Picker.Item>
                            <Picker.Item value="122" label="Liberia"></Picker.Item>
                            <Picker.Item value="123" label="Libyan Arab Jamahiriya"></Picker.Item>
                            <Picker.Item value="124" label="Liechtenstein"></Picker.Item>
                            <Picker.Item value="125" label="Lithuania"></Picker.Item>
                            <Picker.Item value="126" label="Luxembourg"></Picker.Item>
                            <Picker.Item value="127" label="Macau"></Picker.Item>
                            <Picker.Item value="128" label="Macedonia"></Picker.Item>
                            <Picker.Item value="129" label="Madagascar"></Picker.Item>
                            <Picker.Item value="130" label="Malawi"></Picker.Item>
                            <Picker.Item value="131" label="Malaysia"></Picker.Item>
                            <Picker.Item value="132" label="Maldives"></Picker.Item>
                            <Picker.Item value="133" label="Mali"></Picker.Item>
                            <Picker.Item value="134" label="Malta"></Picker.Item>
                            <Picker.Item value="135" label="Marshall Islands"></Picker.Item>
                            <Picker.Item value="136" label="Martinique"></Picker.Item>
                            <Picker.Item value="137" label="Mauritania"></Picker.Item>
                            <Picker.Item value="138" label="Mauritius"></Picker.Item>
                            <Picker.Item value="139" label="Mayotte"></Picker.Item>
                            <Picker.Item value="140" label="Mexico"></Picker.Item>
                            <Picker.Item value="141" label="Micronesia, Federated States of"></Picker.Item>
                            <Picker.Item value="142" label="Moldova, Republic of"></Picker.Item>
                            <Picker.Item value="143" label="Monaco"></Picker.Item>
                            <Picker.Item value="144" label="Mongolia"></Picker.Item>
                            <Picker.Item value="145" label="Montenegro"></Picker.Item>
                            <Picker.Item value="146" label="Montserrat"></Picker.Item>
                            <Picker.Item value="147" label="Morocco"></Picker.Item>
                            <Picker.Item value="148" label='Mozambique'></Picker.Item>
                            <Picker.Item value="149" label="Myanmar"></Picker.Item>
                            <Picker.Item value="150" label="Namibia"></Picker.Item>
                            <Picker.Item value="151" label="Nauru"></Picker.Item>
                            <Picker.Item value="152" label="Nepal"></Picker.Item>
                            <Picker.Item value="153" label="Netherlands"></Picker.Item>
                            <Picker.Item value="154" label="Netherlands Antilles"></Picker.Item>
                            <Picker.Item value="155" label="New Caledonia"></Picker.Item>
                            <Picker.Item value="156" label="New Zealand"></Picker.Item>
                            <Picker.Item value="157" label="Nicaragua"></Picker.Item>
                            <Picker.Item value="158" label="Niger"></Picker.Item>
                            <Picker.Item value="159" label="Nigeria"></Picker.Item>
                            <Picker.Item value="160" label="Niue"></Picker.Item>
                            <Picker.Item value="161" label="Norfork Island"></Picker.Item>
                            <Picker.Item value="162" label="Northern Mariana Islands"></Picker.Item>
                            <Picker.Item value="163" label="Norway"></Picker.Item>
                            <Picker.Item value="164" label="Oman"></Picker.Item>
                            <Picker.Item value="165" label="Pakistan"></Picker.Item>
                            <Picker.Item value="166" label="Palau"></Picker.Item>
                            <Picker.Item value="167" label="Panama"></Picker.Item>
                            <Picker.Item value="168" label="Papua New Guinea"></Picker.Item>
                            <Picker.Item value="169" label="Paraguay"></Picker.Item>
                            <Picker.Item value="170" label="Peru"></Picker.Item>
                            <Picker.Item value="171" label="Philippines"></Picker.Item>
                            <Picker.Item value="172" label="Pitcairn"></Picker.Item>
                            <Picker.Item value="173" label="Poland"></Picker.Item>
                            <Picker.Item value="174" label="Portugal"></Picker.Item>
                            <Picker.Item value="175" label="Puerto Rico"></Picker.Item>
                            <Picker.Item value="176" label="Qatar"></Picker.Item>
                            <Picker.Item value="177" label="Reunion"></Picker.Item>
                            <Picker.Item value="178" label="Romania"></Picker.Item>
                            <Picker.Item value="179" label="Russian Federation"></Picker.Item>
                            <Picker.Item value="180" label="Rwanda"></Picker.Item>
                            <Picker.Item value="181" label="Saint Kitts and Nevis"></Picker.Item>
                            <Picker.Item value="182" label="Saint Lucia"></Picker.Item>
                            <Picker.Item value="183" label="Saint Vincent and the Grenadines"></Picker.Item>
                            <Picker.Item value="184" label="Samoa"></Picker.Item>
                            <Picker.Item value="185" label="San Marino"></Picker.Item>
                            <Picker.Item value="186" label="Sao Tome and Principe"></Picker.Item>
                            <Picker.Item value="187" label="Saudi Arabia"></Picker.Item>
                            <Picker.Item value="188" label="Senegal"></Picker.Item>
                            <Picker.Item value="189" label="Serbia"></Picker.Item>
                            <Picker.Item value="190" label="Seychelles"></Picker.Item>
                            <Picker.Item value="191" label="Sierra Leone"></Picker.Item>
                            <Picker.Item value="192" label="Singapore"></Picker.Item>
                            <Picker.Item value="193" label="Slovakia"></Picker.Item>
                            <Picker.Item value="194" label="Slovenia"></Picker.Item>
                            <Picker.Item value="195" label="Solomon Islands"></Picker.Item>
                            <Picker.Item value="196" label="Somalia"></Picker.Item>
                            <Picker.Item value="197" label="South Africa"></Picker.Item>
                            <Picker.Item value="198" label="South Georgia South Sandwich Islands"></Picker.Item>
                            <Picker.Item value="199" label="Spain"></Picker.Item>
                            <Picker.Item value="200" label="Sri Lanka"></Picker.Item>
                            <Picker.Item value="201" label="St. Helena"></Picker.Item>
                            <Picker.Item value="202" label="St. Pierre and Miquelon"></Picker.Item>
                            <Picker.Item value="203" label="Sudan"></Picker.Item>
                            <Picker.Item value="204" label="Suriname"></Picker.Item>
                            <Picker.Item value="205" label="Svalbarn and Jan Mayen Islands"></Picker.Item>
                            <Picker.Item value="206" label="Swaziland"></Picker.Item>
                            <Picker.Item value="207" label="Sweden"></Picker.Item>
                            <Picker.Item value="208" label="Switzerland"></Picker.Item>
                            <Picker.Item value="209" label="Syrian Arab Republic"></Picker.Item>
                            <Picker.Item value="210" label="Taiwan"></Picker.Item>
                            <Picker.Item value="211" label="Tajikistan"></Picker.Item>
                            <Picker.Item value="212" label="Tanzania, United Republic of"></Picker.Item>
                            <Picker.Item value="213" label="Thailand"></Picker.Item>
                            <Picker.Item value="214" label="Togo"></Picker.Item>
                            <Picker.Item value="215" label="Tokelau"></Picker.Item>
                            <Picker.Item value="216" label="Tonga"></Picker.Item>
                            <Picker.Item value="217" label="Trinidad and Tobago"></Picker.Item>
                            <Picker.Item value="218" label="Tunisia"></Picker.Item>
                            <Picker.Item value="219" label="Turkey"></Picker.Item>
                            <Picker.Item value="220" label="Turkmenistan"></Picker.Item>
                            <Picker.Item value="221" label="Turks and Caicos Islands"></Picker.Item>
                            <Picker.Item value="222" label="Tuvalu"></Picker.Item>
                            <Picker.Item value="223" label="Uganda"></Picker.Item>
                            <Picker.Item value="224" label="Ukraine"></Picker.Item>
                            <Picker.Item value="225" label="United Arab Emirates"></Picker.Item>
                            <Picker.Item value="226" label="United Kingdom"></Picker.Item>
                            <Picker.Item value="1" label="United States"></Picker.Item>
                            <Picker.Item value="227" label="United States minor outlying islands"></Picker.Item>
                            <Picker.Item value="228" label="Uruguay"></Picker.Item>
                            <Picker.Item value="229" label="Uzbekistan"></Picker.Item>
                            <Picker.Item value="230" label="Vanuatu"></Picker.Item>
                            <Picker.Item value="231" label="Vatican City State"></Picker.Item>
                            <Picker.Item value="232" label="Venezuela"></Picker.Item>
                            <Picker.Item value="233" label="Vietnam"></Picker.Item>
                            <Picker.Item value="234" label="Virgin Islands (British)"></Picker.Item>
                            <Picker.Item value="235" label="Virgin Islands (U.S.)"></Picker.Item>
                            <Picker.Item value="236" label="Wallis and Futuna Islands"></Picker.Item>
                            <Picker.Item value="237" label="Western Sahara"></Picker.Item>
                            <Picker.Item value="238" label="Yemen"></Picker.Item>
                            <Picker.Item value="239" label="Yugoslavia"></Picker.Item>
                            <Picker.Item value="240" label="Zaire"></Picker.Item>
                            <Picker.Item value="241" label="Zambia"></Picker.Item>
                            <Picker.Item value="242" label="Zimbabwe"></Picker.Item>
                        </Picker>
                    </Item>
                    <Item regular style={{backgroundColor: 'transparent', borderColor: 'black', borderRadius: 7,
                    marginBottom: 10,  width: Dimensions.get('screen').width - 30, paddingLeft: 10 }}>
                        <Icon name="lock" size={23} style={{marginRight: 5,color: '#f6b93b'}} />
                        <Input placeholderTextColor={'#f6b93b'} style={{color: 'black'}} 
                        placeholder={"Password"} onChangeText = {(text)=>{ this.setState({ password: text }) }} />
                    </Item>
                    <Item regular style={{backgroundColor: 'transparent', borderColor: 'black', borderRadius: 7,
                    marginBottom: 10,  width: Dimensions.get('screen').width - 30, paddingLeft: 10 }}>
                        <Icon name="lock" size={23} style={{marginRight: 5,color: '#f6b93b'}} />
                        <Input placeholderTextColor={'#f6b93b'} style={{color: 'black'}} 
                        placeholder={"Confirm Password"} onChangeText = {(text)=>{ this.setState({ confirmpassword: text }) }} />
                    </Item>
                </Form>
                <View style={{alignItems: 'center', justifyContent:'center'}}>
                    <ScalingButton label="Give" 
                        onPress={this._handlePress.bind(this)}
                        styles={{button: styles.animated_button, label: styles.button_label}}
                    >
                        <Text style={{color: 'white', fontSize: 20}}>Save changes</Text>
                    </ScalingButton>
                </View>
                </ScrollView>
            </View>
        )
    }

    _handlePress = () => {
        if(this.state.fullname == '')
        {
            alert("Full name is a required field. Please provide your full name");
            return;
        }
        if(this.state.password != '')
        {
            if(this.state.password != this.state.confirmpassword)
            {
                alert("Password and ConfirmPassword dont match. Please try again");
                return;
            }
        }
        else{
            this.props.updateAccount();
            // this.props.navigation.navigate('Payment');
        }
        
    }

    _renderLoader = () =>{
        return(
        <View>
            <Dialog
            visible={this.state.loader}
            dialogStyle={{width: 200, padding: 20}}
            >
            <DialogContent>
                <ActivityIndicator size={"large"} color={"golden"} />
                <Text style={{fontSize: 20}}>Please wait...</Text>
            </DialogContent>
            </Dialog>
        </View>
        )
    }

    render() {
        return (
            <Container>
                <View style={{width: Dimensions.get('window').width, height: Platform.OS == "android" ? 60 : 85, backgroundColor: '#f6b93b', 
                justifyContent:'center', alignItems: 'flex-end', flexDirection:'row', paddingBottom: 15,
                }}>
                    {/* <Image source={require('../assets/img/logo.png')} style={{width: 32, height: 32, paddingBottom: 3, 
                        marginRight: 4, tintColor: '#fff'}} /> */}
                    <Image source={require('../assets/img/complete_logo.png')} style={{width: 144, height: 20, tintColor: '#fff'}} />
                </View>
                <Content contentContainerStyle={{flex: 1}}>
                    {this._renderHeader()}
                    {this._renderLoader()}
                    {this._renderCreationView()}
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    bottom:{
        flex: 1, backgroundColor: 'white', 
        paddingTop: 25,
        paddingBottom: 20,
        paddingLeft: 30,
        paddingRight: 30, 
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30
    },
    title:{
        fontSize: 26,
        fontWeight: 'bold',
        color: 'black'
    },
    animated_button : {
        backgroundColor: '#f6b93b',
        borderRadius: 10,
        width: 300
    },
    button_label: {
        color: '#fff',
        fontSize: 18
    },
    uploadAvatar : {
        width: Dimensions.get('window').width,
        height: 150
    }
});

const mapStateToProps = (state) => {
    return {
        loggedUser: state.loggedUser,
        userInfo: state.userInfo,
        isAccountUpdated: state.isAccountUpdated
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reset: () => {
            dispatch(resetRedirectionToMyAccount())
        },
        getAccount: (x) => {
            dispatch(getAccountInfo(x))
        },
        updateAccount: (x) => {
            dispatch(updateAccountInfo(x))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Account);
