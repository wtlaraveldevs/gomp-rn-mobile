/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, AsyncStorage, Text, View, Dimensions, Image, 
  ActivityIndicator, Linking, ImageBackground} from 'react-native';
import { Container, Content, Header, Button, Left, Body, Right, Title, Icon, Fab } from 'native-base';
import { Grid, Row, Col } from 'react-native-easy-grid';
import Dialog, { DialogFooter, DialogButton, DialogContent } from 'react-native-popup-dialog';
import { connect } from 'react-redux';
import AnimatedBottom from './authencation/AnimatedBottom';
import PaymentGateway from './util/paymentgateway';
import { showLogin, setLoggedUser, getUserInfo, redirectToGoals, setGOM } from '../redux/actions';
import Fee from './fee';
import { config } from '../config';
import { primaryColor } from '../assets/Util/colors';

import Confetti from 'react-native-confetti';
import dynamicLinks from '@react-native-firebase/dynamic-links';
import messaging from '@react-native-firebase/messaging';

class App extends Component {

  state = {
    visible: false,
    loader: false,
    loading: false,
    isLoggedIn: false,
    showFee: false,
    hasActivePot: false
  }

  componentWillMount(){

    // dynamicLinks().getInitialLink().then((link) => {
    //   console.log("Link in the url " + link);
    // })

    this.requestUserPermission();

    dynamicLinks().onLink((link) => {
      alert("Dynamic Link " + link);
    })

    Linking.getInitialURL().then((link) => {
      // alert("Linking URL " + link);
      if(link){
        var objects = link.split("%2F");
        var potId = objects[objects.length - 1];
        if(potId){
          this.setState({loading: true})
          fetch(`${config.api.searchPot}`, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({ 
              text : potId
            })
          })
          .then(response=> { return response.json() })
          .then((response) => {

              if(response.status == 'success')
              {
                this.props.setSearchedGOM(response.data.data[0]);

                setTimeout(()=>{
                  this.setState({loading: false})
                  this.props.navigation.navigate('PotView');
                }, 3000);
              }
              else{
                this.setState({loading: false})
                alert("POT not found.")
              }
              
          })
          .catch((error) => {
            this.setState({loading: false})
            alert("Unable to find the pot " + error);
          });
        }
      }
    })
    
    AsyncStorage.getItem('isLoggedIn').then((item)=>{
      // console.log("IsloggedIn", item);
      if(item == "true")
      {
        this.setState({ isLoggedIn: true })
      }
      else{
        this.setState({ isLoggedIn: false })
      }
    });

    AsyncStorage.getItem('user').then((user)=>{
      // this.props.setUser(user);
      console.log("User Stored", user);
      if(user)
      {
        console.log("Logging the user again");
        var _user = JSON.parse(user);
        var data = {
          email: _user.email
        }
        console.log("GetUserInfo Data", data);
        this.props.getUserDetails(data)
      }
      // if(user != "" || user != null)
      // {
      //   var _user = JSON.parse(user);
      //   // get active pot status
      //   this.getAcitvePot(_user.id);
      // }
    });
  }

   requestUserPermission = async () => {
    const authStatus = await messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;
  
    if (enabled) {
      console.log('Authorization status:', authStatus);
    }
  }

  getAcitvePot(user_id){
    try{
      fetch(`${config.api.hasActivePot}`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          user_id: user_id
        })
      })
      .then((response) => {
        return response.json();
      })
      .then((response) => {
        if(response.status == false){
          console.log("something went wrong on the server. Please check logs.");
        }
        else {
          console.log("Has an active pot? ", response.hasActivePot );
          this.setState({
            hasActivePot: response.hasActivePot
          });
        }
      })
    }
    catch(error)
    {
      alert("Unable to fetch active pot information");
    }
  }


  componentDidMount() {
    // if(this._confettiView) {
    //   this._confettiView.startConfetti();
    // }
    this._confettiView.startConfetti();
  }

  componentWillReceiveProps(nextProps){
    // if(nextProps.isLoggedIn == true)
    // {
      
    // }
    console.log('isLoggedIn', nextProps.isLoggedIn);
    this.setState({isLoggedIn: nextProps.isLoggedIn == true ? true: false})


    if(nextProps.redirectToFavorites)
    {
      this.props.navigation.navigate('Favorites');
    }
    if(nextProps.redirectToGoals)
    {
      this.props.navigation.navigate('Goals');
    }
    else if(nextProps.redirectToMyAccount)
    {
      this.props.navigation.navigate('Account');
    }
  }

  _renderFeeView = () => {
    if(this.state.showFee)
    {
      return <Fee onDismiss={()=>{this.setState({showFee: false})}} />
    }
    else return null;
  }

  _renderLoader = () =>{
    return(
      <View>
        <Dialog
          visible={this.state.loading}
          dialogStyle={{width: 200, padding: 15}}
        >
          <DialogContent>
            <ActivityIndicator size={"large"} color={"golden"} />
            <Text style={{fontSize: 20}}>Please wait...</Text>
          </DialogContent>
        </Dialog>
      </View>
    )
  }

  _renderSearchDialog = () =>{
    return(
      <View style={styles.container}>
        <Dialog
          visible={this.state.visible}
          dialogStyle={{width: '100%'}}
          footer={
            <DialogFooter>
              <DialogButton
                text="CANCEL"
                onPress={() => {
                  this.setState({visible: false})
                }}
              />
              <DialogButton
                text="OK"
                onPress={() => {
                  this.setState({visible: false})
                  // but send a request to api
                }}
              />
            </DialogFooter>
          }
        >
          <DialogContent>
            <Text>welcome</Text>
          </DialogContent>
        </Dialog>
      </View>
    )
  }

  render() {
    const navigation = this.props.navigation;
    return (
      <Container>
        <View style={{width: Dimensions.get('window').width, height: Platform.OS == "android" ? 60 : 85, borderBottomColor: "#fff", 
        borderBottomWidth: 1, justifyContent:'center', alignItems: 'flex-end', 
        flexDirection:'row', paddingBottom: 5}}>
            <View style={{justifyContent:'center', alignItems: 'center', flexDirection:'row',}}>
              {/* <Image source={require('../assets/img/logo.png')} style={{width: 32, height: 32, paddingBottom: 3, 
                  marginRight: 4, tintColor: primaryColor}} /> */}
              <Image source={require('../assets/img/complete_logo.png')} style={{width: 144, height: 20, tintColor: primaryColor}} />
            </View>
        </View>
        <Content>
        {/* <ImageBackground source={require('../assets/img/background.png')} style={{width: '100%', height: '100%'}} > */}
        {this._renderSearchDialog()}
        {
            this.state.loader ?
            <PaymentGateway title={"Buy your GYFTmoni Pot"} height={470}
            description={"We will send you your unique GYFTmoni Pot ID to share with others so that you can start receiving Gift Of Money"} buttonLabel={"Pay £2.00"} navigation={this.props.navigation} purchasePot={true} onHide={()=>{
                this.setState({ loader: false })
            }} /> 
            : null
        }
        {this._renderFeeView()}
        
        <Grid style={{ padding: 15, paddingBottom: 60}}>
          <Col size={Dimensions.get('screen').width / 2}>
            <Row>
              <Col size={Dimensions.get('screen').width / 2} style={styles.colStyle}>
                <ImageBackground source={require('../assets/img/confetti.jpg')} 
                style={styles.rightIB}>
                  <Button transparent onPress={()=>{ navigation.navigate("Give") }} style={styles.buttonResolution}>
                    <Text style={styles.buttonText} >{`Send\nGYFTmoni`}</Text>
                  </Button>
                </ImageBackground>
              </Col>
              <Col size={Dimensions.get('screen').width / 2} style={styles.colStyle}>
                <ImageBackground source={require('../assets/img/confetti.jpg')} 
                  style={styles.leftIB}>
                  <Button transparent onPress={() => {
                    console.log("isLoggedIn", this.state.isLoggedIn);
                    console.log("User ", this.props.loggedUser);
                    
                    if(this.props.loggedUser)
                    {
                      if(this.props.loggedUser.active_purchased_pot == '1'){ // User has an active pot.
                        this.props.navigation.navigate("CreatePot")
                      }
                      else {
                        this.setState({ loader: true })
                      }
                    }
                    else{
                      this.props.showDialog();
                    }
                  }}
                  style={styles.buttonResolution}>
                    <Text style={styles.buttonText} >{`Create\nGYFTmoni Pot`}</Text>
                  </Button>
                </ImageBackground>
              </Col>
            </Row>
            <Row>
              <Col size={Dimensions.get('screen').width / 2} style={styles.colStyle}>
                <ImageBackground source={require('../assets/img/confetti.jpg')} 
                  style={styles.rightIB}>
                  <Button transparent onPress={()=> { 
                    if(this.props.loggedUser)
                    {
                      this.props.navigation.navigate('Goals')
                    }
                    else{
                      if(Platform.OS === 'ios' && !this.props.loggedUser)
                      {
                        this.props.navigation.navigate('Login');
                      }
                      else{
                        this.props.showDialog()
                      }
                    }
                  }} style={styles.buttonResolution}>
                    <Text style={styles.buttonText} >{`My GYFTmoni\nPots`}</Text>
                  </Button>
                </ImageBackground>
              </Col>
              <Col size={Dimensions.get('screen').width / 2} style={styles.colStyle}>
                <ImageBackground source={require('../assets/img/confetti.jpg')} 
                  style={styles.leftIB}>
                  <Button transparent onPress={()=> { 
                    if(this.props.loggedUser)
                    {
                      this.props.navigation.navigate('Wallet')
                    }
                    else{
                      if(Platform.OS === 'ios' && !this.props.loggedUser)
                      {
                        this.props.navigation.navigate('Login');
                      }
                      else{
                        this.props.showDialog()
                      }
                    }
                  }} style={styles.buttonResolution}>
                    <Text style={styles.buttonText} >{`Get My\nMoney`}</Text>
                  </Button>
                </ImageBackground>
              </Col>
            </Row>
            <Row>
              <Col size={Dimensions.get('screen').width / 2} style={styles.colStyle}>
                <ImageBackground source={require('../assets/img/confetti.jpg')} 
                  style={styles.rightIB}>
                  <Button transparent onPress={()=> { 
                    if(this.props.loggedUser)
                    {
                      this.props.navigation.navigate('DashboardContainer', {
                        viewNumber: 2
                      })
                    }
                    else{
                      if(Platform.OS === 'ios' && !this.props.loggedUser)
                      {
                        this.props.navigation.navigate('Login');
                      }
                      else{
                        this.props.showDialog()
                      }
                    }
                  }} style={styles.buttonResolution}>
                    <Text style={styles.buttonText} >{`ThankYous\n& Messages`}</Text>
                  </Button>
                </ImageBackground>
              </Col>
              <Col size={Dimensions.get('screen').width / 2} style={styles.colStyle}>
                <ImageBackground source={require('../assets/img/confetti.jpg')} 
                  style={styles.leftIB}>
                  <Button transparent onPress={()=> { 
                    if(this.props.loggedUser)
                    {
                      this.props.navigation.navigate('DashboardContainer', {
                        viewNumber: 0
                      })
                    }
                    else{
                      if(Platform.OS === 'ios' && !this.props.loggedUser)
                      {
                        this.props.navigation.navigate('Login');
                      }
                      else{
                        this.props.showDialog()
                      }
                    }
                  }} style={styles.buttonResolution}>
                    <Text style={styles.buttonText} >Dashboard</Text>
                  </Button>
                  </ImageBackground>
              </Col>
            </Row>
            <Row>
              <Col size={Dimensions.get('screen').width / 2} style={styles.colStyle}>
                <ImageBackground source={require('../assets/img/confetti.jpg')} 
                  style={styles.rightIB}>
                  <Button transparent onPress={()=>{ navigation.navigate("Search") }} 
                  style={styles.buttonResolution}>
                    <Text style={styles.buttonText} >Search</Text>
                  </Button>
                </ImageBackground>
              </Col>
              <Col size={Dimensions.get('screen').width / 2} style={styles.colStyle}>
                <ImageBackground source={require('../assets/img/confetti.jpg')} 
                  style={styles.leftIB}>
                  <Button transparent onPress={()=> { this.setState({showFee: true}) }} 
                  style={styles.buttonResolution}>
                    <Text style={styles.buttonText} >Fees</Text>
                  </Button>
                </ImageBackground>
              </Col>
            </Row>
            {/* <Row style={{marginBottom: 10}}>
              <Button onPress={()=>{ navigation.navigate("howitworks") }} style={{backgroundColor: '#f6b93b', justifyContent: 'center',
                alignItems: 'center', borderColor: 'yellow', borderWidth: 1, borderRadius: 7,
                 width: Dimensions.get('screen').width/ 3.25, height: Dimensions.get('screen').height/ 10}}>
                <Text style={{fontSize: 20, textAlign: 'center', color: 'white'}} >{`How It\nWorks`}</Text>
              </Button>
            </Row> */}
          </Col>
          <Col>
          </Col>
        </Grid>
        {/* </ImageBackground> */}
          {this._renderLoader()}
        </Content>
        <Confetti 
          ref={(node) => this._confettiView = node}
        />
        <Fab
          active={this.state.active}
          style={{ position: Platform.OS == "ios" ? "absolute" : 'relative', bottom: 30, right: 0, backgroundColor: primaryColor, marginBottom: 15, zIndex: 20 }}
          position="bottomRight"
          onPress={() => this.props.navigation.navigate("howitworks") }>
          <Icon name="help" color={"#fff"} />
        </Fab>
        <AnimatedBottom navigation={this.props.navigation} />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  colStyle: {
    marginBottom: 15,
  },
  leftIB: {
    width: '98%', 
    height: Dimensions.get('screen').height/ 6, 
    marginLeft: 8,
    borderTopLeftRadius: 12,
    borderBottomLeftRadius: 12
  },
  rightIB: {
    width: '98%', 
    height: Dimensions.get('screen').height/ 6, 
    marginRight: 5,
    borderTopLeftRadius: 12,
    borderBottomLeftRadius: 12
  },
  buttonResolution: {
    justifyContent: 'center',
    alignItems: 'center', 
    borderColor: 'black', 
    borderWidth: 4, 
    borderRadius: 7,
    width: '98%', 
    height: Dimensions.get('screen').height/ 6
  },
  buttonText: {
    fontSize: 20, 
    textAlign: 'center', 
    color: 'black',
    fontWeight: '900'
  }
});

const mapStateToProps = (state) => {
  return {
    isLoggedIn: state.isLoggedIn,
    loggedUser: state.loggedUser,
    redirectToFavorites: state.redirectToFavorites,
    redirectToGoals: state.redirectToGoals,
    redirectToMyAccount: state.redirectToMyAccount
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    showDialog: () => {
      dispatch(showLogin())
    },
    setUser: (user) => {
      dispatch(setLoggedUser(user));
    },
    getUserDetails: (data) => {
      dispatch(getUserInfo(data))
    },
    setSearchedGOM : (data) => {
      dispatch(setGOM(data))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
