import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    ImageBackground
} from 'react-native';
import Dialog, { DialogFooter, DialogButton, DialogContent } from 'react-native-popup-dialog';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { Input, Button } from 'native-base';

class Fee extends Component {
    state = { 
        visible: true,
        fee: "0.00",
        amount: 0
    }

    _handleCalculation = () => {
        var val = this.state.amount.toString();
        var strs = '';
        if( val.indexOf(',') != -1 ){
            // strs = val.split(',');
            val = val.replace(',', '');
        }
        // if(strs != '')
        // {
        //     var amt = parseInt(strs[0]+strs[1]);
        //     val = amt;
        // }
        var fee =  (((val * 100) * 0.02) + 130).toFixed(2).toString();
        fee = parseFloat(fee / 100).toFixed(2);
		this.setState({fee})
    }

    render() { 
        return ( 
            <View style={styles.container}>
            <Dialog
            onDismiss={this.props.onDismiss}
            visible={this.state.visible}
            dialogStyle={{width: '80%', height: 320, overflow: 'visible'}}
                >
                {/* <View style={{position: 'absolute', top: -20, left: -20,}}>
                    <Image source={require('../assets/img/gbow.png')} style={{width: 64, height: 64}} />
                </View> */}
                <ImageBackground source={require('../assets/img/template_border_old.jpg')}
                    style={{width: '100%', height: '100%'}} >
                <DialogContent style={{backgroundColor:'transparent', height: '100%', width: '100%'}}>
                    
                    <Grid style={{backgroundColor: 'transparent'}}>
                        <Col style={{padding: 10}}>
                            <Row size={45} style={{ marginBottom: 10, justifyContent: 'center', alignItems: 'center',
                                borderBottomWidth: 1, borderColor: '#ccc'}}>
                                <Text style={{fontWeight: 'bold', fontSize: 44}}>£{this.state.fee}</Text>
                            </Row>
                            <Row size={40} style={{marginBottom: 10, paddingBottom:5, borderBottomWidth: 1, borderColor: '#ccc', flexDirection:'column', justifyContent: 'center', }}>
                                <Text style={{marginBottom: 10}}>Calculate GYFTmoni fee</Text>
                                <Input style={{marginBottom: 5, borderColor: 'black', borderWidth: 1, 
                                padding: 0, borderRadius: 5,  paddingLeft: 15}}
                                placeholder={"Amount"} keyboardType={"number-pad"} onChangeText={(text)=>{this.setState({amount: text})}} />
                                <Text style={{color: '#a9a9a9', fontSize: 12}}>Gifter Fees : 2% + £1.30</Text>
                                <Text style={{color: '#a9a9a9', fontSize: 12}}>Giftee      : No Withdrawal Fees</Text>
                            </Row>
                            <Row size={15} style={{justifyContent: 'center', paddingTop: 15, alignItems: 'center'}}>
                                <Button onPress={this._handleCalculation.bind(this)} 
                                transparent style={{justifyContent:'center', borderRadius: 5, alignItems:'center',
                                 width: 100, backgroundColor: '#f6b93b'}}>
                                    <Text style={{color:'white', fontSize: 16}}>Calculate</Text>
                                </Button>
                                <Button  onPress={()=>{
                                    this.setState({visible: false})
                                }}
                                transparent style={{marginLeft: 5, borderRadius: 5, justifyContent:'center', alignItems:'center',
                                 width: 80, backgroundColor: '#f6b93b'}}>
                                    <Text style={{color:'white', fontSize: 16}}>Close</Text>
                                </Button>
                            </Row>
                        </Col>
                    </Grid>
                </DialogContent>
                </ImageBackground>
                {/* <View style={{position: 'absolute', bottom: -20, right: -20,}}>
                    <Image source={require('../assets/img/gbow.png')} style={{width: 64, height: 64}} />
                </View> */}
                </Dialog>
            </View>
         );
    }
}
 
export default Fee;