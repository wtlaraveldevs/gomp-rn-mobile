import React, {Component} from 'react';
import { 
  TouchableOpacity, StyleSheet, Text, View, ScrollView,
  Dimensions, Image, TouchableWithoutFeedback, ActivityIndicator
} from 'react-native';
import { Container, Content, Header, Button, Form, 
    Input, Title, Item, Label, Textarea } from 'native-base';
import { connect } from 'react-redux';
import { config } from '../config/index';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Grid, Row, Col } from 'react-native-easy-grid';
// import Modal, { ModalContent, ModalTitle } from 'react-native-modals';
import KeyboardSpacer from 'react-native-keyboard-spacer'

import PaymentGateway from './util/paymentgateway';

// Scaling Button
import ScalingButton from './util/ScalingButton';

class Payment extends Component {

    state = {
        img : '',
        visible: true,
        confirm: false,
        loader: false,
        loading: false,

        // params
        amount: 0,
        message: null,
        firstname: '',
        lastname: '',
        phone: '',
        formattedPhone: '',
        email: '',


        potId: 0
    }

    componentWillMount(){
        this.setState({ potId: this.props.selected_gom.id })
    }

    componentWillReceiveProps(nextProps){
        console.log("NextProps",nextProps);
    }

    _renderHeader(){
        return(
            <Header style={{marginLeft: -2, width: '102%', backgroundColor: 'transparent' }}>
            <Grid>
                <Row>
                <Col size={15} style={{justifyContent: 'center', alignItems: 'center'}}>
                    <Button transparent onPress={()=>{
                        if(this.state.confirm){
                            this.setState({confirm : false})
                        }else{
                            this.props.navigation.goBack()
                        }
                    }}>
                    <Icon name="chevron-left" size={30} color={'#FCC438'} />
                    </Button>
                </Col>
                <Col size={65} style={{justifyContent: 'center', alignItems: 'center'}}>
                    <Text style={{fontSize: 20}}>Payment</Text>
                </Col>
                <Col size={15} style={{justifyContent: 'center', alignItems: 'center'}}>
                </Col>
                </Row>
            </Grid>
            </Header>
        )
    }

    _renderPaymentView(){
        return (
            <View style={{flex: 1, backgroundColor: 'transparent'}}>
                <ScrollView style={{padding: 15, }}>
                <Form>
                    <Label style={{paddingLeft: 5, color: '#f6b93b', marginBottom: 5, fontWeight: 'bold'}}>Sender Info</Label>
                    <Item regular style={{backgroundColor: 'transparent', borderColor: 'black', borderRadius: 7,  
                    marginBottom: 10,  width: Dimensions.get('screen').width - 30, paddingLeft: 10 }}>
                        <Icon name="email" size={23} style={{marginRight: 5, color: '#f6b93b'}} />
                        <Input placeholderTextColor={'#f6b93b'} style={{color: 'black'}} 
                        placeholder={"First Name"} 
                        onChangeText = {(text)=>{this.setState({firstname: text})}} />
                    </Item>
                    <Item regular style={{backgroundColor: 'transparent', borderColor: 'black', borderRadius: 7,
                    marginBottom: 10,  width: Dimensions.get('screen').width - 30, paddingLeft: 10 }}>
                        <Icon name="lock" size={23} style={{marginRight: 5,color: '#f6b93b'}} />
                        <Input placeholderTextColor={'#f6b93b'} style={{color: 'black'}} 
                        placeholder={"Last Name"} 
                        onChangeText = {(text)=>{this.setState({lastname: text})}} />
                    </Item>
                    <Item regular style={{backgroundColor: 'transparent', borderColor: 'black', borderRadius: 7,
                    marginBottom: 10,  width: Dimensions.get('screen').width - 30, paddingLeft: 10 }}>
                        <Icon name="lock" size={23} style={{marginRight: 5,color: '#f6b93b'}} />
                        <Input keyboardType={"number-pad"} placeholderTextColor={'#f6b93b'} style={{color: 'black'}} 
                        placeholder={"Mobile #"} 
                        onChangeText = {(phone)=>{
                            // var _phone = this.formatPhoneNumber(phone);
                            var _phone = phone;
                            if(_phone == null || _phone == '')
                            {
                                this.setState({phone: phone })
                            }
                            else{
                                this.setState({phone: phone, formattedPhone: _phone})
                            }
                        }} />
                    </Item>
                    <Item regular style={{backgroundColor: 'transparent', borderColor: 'black', borderRadius: 7,  
                    marginBottom: 10,  width: Dimensions.get('screen').width - 30, paddingLeft: 10 }}>
                        <Icon name="email" size={23} style={{marginRight: 5, color: '#f6b93b'}} />
                        <Input keyboardType={"email-address"} placeholderTextColor={'#f6b93b'} style={{color: 'black'}} 
                        placeholder={"Email"} 
                        onChangeText = {(email)=>{this.setState({email:email})}} />
                    </Item>
                </Form>
                <Form>
                    <Label style={{paddingLeft: 5, color: '#f6b93b', marginBottom: 5, fontWeight: 'bold'}}>The Gift</Label>
                    <Item regular style={{backgroundColor: 'transparent', borderColor: 'black', borderRadius: 7,  
                    marginBottom: 10,  width: Dimensions.get('screen').width - 30, paddingLeft: 10 }}>
                        <Icon name="currency-gbp" size={23} style={{marginRight: 5, color: '#f6b93b'}} />
                        <Input keyboardType={"number-pad"} placeholderTextColor={'#f6b93b'} style={{color: 'black'}} 
                        placeholder={"Amount"} 
                        onChangeText = {(amt)=>{ this.setState({amount: amt}) }} />
                    </Item>
                    <Item regular style={{backgroundColor: 'transparent', borderColor: 'black', borderRadius: 7,
                    marginBottom: 10,  width: Dimensions.get('screen').width - 30, paddingLeft: 10 }}>
                        <Icon name="message-text" size={23} style={{marginRight: 5,color: '#f6b93b'}} />
                        <Textarea rowSpan={4} placeholderTextColor={'#f6b93b'} style={{width: '100%',color: 'black'}} 
                        placeholder={"Message"} 
                        onChangeText = {(message)=>{ this.setState({message: message})}} />
                    </Item>
                </Form>
                <View style={{alignItems: 'center', justifyContent:'center'}}>
                    {/* {
                        this.state.loading == true ?
                        <View style={{backgroundColor: "#f6b93b", borderRadius: 30,
                        width: 300, paddingVertical: 15, marginTop: 20}}
                        >
                            <ActivityIndicator color={"#fff"} size={"large"} />
                        </View> :
                            <ScalingButton label="Give" 
                            onPress={() => {
                                this.setState({loading: true});
                                setTimeout(()=>{
                                    this._handlePress.bind(this)
                                }, 1000);
                            }}
                            styles={{button: styles.animated_button, label: styles.button_label}}
                        >
                            <Text style={{color: 'white', fontSize: 20}}>Confirm</Text>
                        </ScalingButton>
                    } */}
                    <ScalingButton label="Give" 
                        onPress={this._handlePress.bind(this)}
                        styles={{button: styles.animated_button, label: styles.button_label}}
                    >
                        <Text style={{color: 'white', fontSize: 20}}>Confirm</Text>
                    </ScalingButton>
                </View>
                </ScrollView>
            </View>
        )
    }

    _renderPhoneNumber = () => {
        var phone = this.state.phone;
        if(phone[0] == "0")
        {
            phone = phone.slice(1);
        }
        this.setState({ phone: phone  })
        // return <Label>+44 {this.state.formattedPhone}</Label>
        return <Label>+44{phone}</Label>
    }

    formatPhoneNumber(phoneNumberString) {
        var cleaned = ('' + phoneNumberString).replace(/\D/g, '')
        var match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/)
        if (match) {
          return match[1] + '' + match[2] + '' + match[3]
        }
        return null
    }

    _renderConfirmationView(){
        return (
            <View style={{flex: 1, padding: 15, backgroundColor: 'transparent'}}>
                <ScrollView keyboardShouldPersistTaps="always" style={{flex: 1}}>
                <Grid style={{paddingBottom: 100}}>
                    <Col>
                        <Row>
                            <Item style={{backgroundColor: 'transparent',
                                width: Dimensions.get('screen').width - 35, 
                                padding: 10 }}>
                            <Col>
                                <Row style={{marginBottom: 10, }}>
                                    <Label style={{color: '#f6b93b', fontWeight: 'bold'}}>GYFTmoni Info</Label>
                                </Row>
                                <Row>
                                <Col size={30}>
                                    <Text>Title</Text>
                                </Col>
                                <Col size={70} style={{flexDirection: 'row', justifyContent: 'flex-end'}}>
                                    <Label>{this.props.selected_gom.title}</Label>
                                </Col>
                                </Row>
                            </Col>
                            </Item>
                        </Row>
                        <Row>
                            <Item style={{backgroundColor: 'transparent',
                                width: Dimensions.get('screen').width - 35, 
                                padding: 10 }}>
                            <Col>
                                <Row style={{marginBottom: 10, }}>
                                    <Label style={{color: '#f6b93b', fontWeight: 'bold',}}>Gift</Label>
                                </Row>
                                <Row>
                                    <Col size={30}>
                                        <Text>Amount</Text>
                                    </Col>
                                    <Col size={70} style={{flexDirection: 'row', justifyContent: 'flex-end'}}>
                                        <Icon name="currency-gbp" size={23} style={{marginRight: 5,color: 'black'}} />
                                        <Label>{this.state.amount}</Label>
                                    </Col>
                                </Row>
                            </Col>
                            </Item>
                        </Row>
                        <Row>
                            <Item style={{backgroundColor: 'transparent',
                                width: Dimensions.get('screen').width - 35, 
                                padding: 10 }}>
                            <Col size={30}>
                                <Text>Message</Text>
                            </Col>
                            <Col size={70} style={{flexDirection: 'row', justifyContent: 'flex-end'}}>
                                <Text style={{textAlign: 'justify', }}>
                                    {this.state.message}
                                </Text>
                            </Col>
                            </Item>
                        </Row>
                        <Row>
                            <Item style={{backgroundColor: 'transparent',
                                width: Dimensions.get('screen').width - 35, 
                                padding: 10 }}>
                            <Col>
                                <Row style={{marginBottom: 10, }}>
                                    <Label style={{color: '#f6b93b', fontWeight: 'bold',}}>Gifter</Label>
                                </Row>
                                <Row>
                                    <Col size={30}>
                                        <Text>Name</Text>
                                    </Col>
                                    <Col size={70} style={{flexDirection: 'row', justifyContent: 'flex-end'}}>
                                        <Label>{this.state.firstname} {this.state.lastname}</Label>
                                    </Col>
                                </Row>
                            </Col>
                            </Item>
                        </Row>
                        <Row>
                            <Item style={{backgroundColor: 'transparent',
                                marginBottom: 10, width: Dimensions.get('screen').width - 35, 
                                padding: 10 }}>
                            <Col size={30}>
                                <Text>Mobile</Text>
                            </Col>
                            <Col size={70} style={{flexDirection: 'row', justifyContent: 'flex-end'}}>
                                {/* {this._renderPhoneNumber()} */}
                                <Label>{this.state.phone}</Label>
                            </Col>
                            </Item>
                        </Row>
                        <Row>
                            <Item style={{backgroundColor: 'transparent', borderBottomWidth: 0,
                                marginBottom: 10, width: Dimensions.get('screen').width - 35, 
                                padding: 10 }}>
                            <Col>
                                <Text>Email</Text>
                            </Col>
                            <Col style={{flexDirection: 'row', justifyContent: 'flex-end'}}>
                                <Label>{this.state.email}</Label>
                            </Col>
                            </Item>
                        </Row>
                    </Col>
                </Grid>
                </ScrollView>
                <View style={{alignItems: 'center', justifyContent:'center', position: 'absolute',
                    bottom: 5, width: Dimensions.get('window').width}}>
                    <ScalingButton label="Give" 
                        onPress={this._handlePress.bind(this)}
                        styles={{button: styles.animated_button, label: styles.button_label}}
                    >
                        <Text style={{color: 'white', fontSize: 20}}>Checkout with MangoPay</Text>
                    </ScalingButton>
                </View>
            </View>
        )
    }

    _handlePress = () => {
        console.log("Handle Press");

        if(this.state.confirm){
            // show the payment view
            this.setState({ loader : true })
        }else{
            console.log("Amount ", this.state.amount);
            if(this.state.firstname == "" || this.state.firstname == null){
                alert("Please provide a valid first name.")
            }
            else if(this.state.lastname == "" || this.state.lastname == null){
                alert("Please provide a valid last name.")
            }
            else if(this.state.phone == "" || this.state.phone == null){
                alert("Please provide a valid mobile number.")
            }
            else if(this.state.email == "" || this.state.email == null){
                alert("Please provide a valid email.")
            }
            else if(this.state.amount == 0 || this.state.amount == "" || this.state.amount == null)
            {
                alert("Please enter amount greater than zero.")
            }
            else{

                console.log("Showing confirmation view");

                var phone = this.state.phone;
                if(phone[0] == "0")
                {
                    phone = phone.slice(1);
                }

                // show the confirmation view
                this.setState({confirm : true, phone: phone})
            }
        }
        // this.props.navigation.navigate('Payment');
    }

  
    render() {
        return (
            <Container>
                <Content contentContainerStyle={{flex: 1}} >
                    <View style={{width: Dimensions.get('window').width, height: 60, backgroundColor: '#f6b93b', 
                    justifyContent:'center', alignItems: 'flex-end', flexDirection:'row', paddingBottom: 15}}>
                        <Image source={require('../assets/img/logo.png')} style={{width: 32, height: 32, paddingBottom: 3, 
                            marginRight: 4, tintColor: '#fff'}} />
                        <Image source={require('../assets/img/complete_logo.png')} style={{width: 144, height: 20, tintColor: '#fff'}} />
                    </View>
                    {this._renderHeader()}
                    {
                        this.state.loader ?
                        <PaymentGateway title={"Provide payment details"}
                        description={""} height={320}
                        id={this.props.selected_gom.id} email={this.state.email} message={this.state.message}
                        amount={this.state.amount} phone={this.state.phone} firstname={this.state.firstname}
                        lastname={this.state.lastname}
                        navigation={this.props.navigation} buttonLabel={"Pay With MangoPay"} purchasePot={false} onHide={()=>{
                            this.setState({ loader: false })
                        }} /> 
                        : null
                    }
                    {
                        this.state.confirm == true ?
                        this._renderConfirmationView() 
                        :
                        this._renderPaymentView()
                    }
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
  bottom:{
    flex: 1, backgroundColor: 'white', 
    paddingTop: 25,
    paddingBottom: 20,
    paddingLeft: 30,
    paddingRight: 30, 
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30
  },
  title:{
    fontSize: 26,
    fontWeight: 'bold',
    color: 'black'
  },
  animated_button : {
    backgroundColor: '#f6b93b',
    borderRadius: 10,
    width: 300,
  },
  button_label: {
    color: '#fff',
    fontSize: 18
  }
});

const mapStateToProps = (state) => {
  return {
    selected_gom: state.selected_gom
  }
}

export default connect(mapStateToProps,null)(Payment);
