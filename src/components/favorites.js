import React, {Component} from 'react';
import { 
  TouchableOpacity, StyleSheet, Text, View, Image,
  Dimensions, FlatList, TouchableWithoutFeedback, SafeAreaView
} from 'react-native';
import { Container, Content, Header, Button, Form, 
    Input, Title, Item, Label, Textarea } from 'native-base';
import { connect } from 'react-redux';
import { config } from '../config/index';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { fetchFavorites, resetRedirectionToFavorites, setGOM } from '../redux/actions';
import { primaryColor } from '../assets/Util/colors';


class Favorites extends Component {

    state = {
        img : '',
        visible: true,
        loader: false,
        favArray: [],
        hasData: false,
        _initial: true,
    }

    componentWillMount(){
        // get favorites from the server.
        this.props.getFavorites({user_id: this.props.loggedUser.id});
    }

    componentWillReceiveProps(nextProps){
        this.setState({_initial: false})
        if(nextProps.favorites.length > 0)
        {
            this.setState({ favArray: nextProps.favorites, hasData: true })
        }
        else 
        {
            this.setState({ favArray: [], hasData: false })
        }
    }

    _renderHeader(){
        return(
            <Header style={{marginLeft: -2, width: '102%', backgroundColor: 'transparent' }}>
            <Grid>
                <Row>
                <Col size={15} style={{justifyContent: 'center', alignItems: 'center'}}>
                    <Button transparent onPress={()=>{
                        setTimeout(() => {
                            this.props.navigation.goBack()
                        }, 50);
                        this.props.reset();
                    }}>
                    <Icon name="chevron-left" size={30} color={'#FCC438'} />
                    </Button>
                </Col>
                <Col size={65} style={{justifyContent: 'center', alignItems: 'center'}}>
                    <Text style={{fontSize: 20}}>Favourites</Text>
                </Col>
                <Col size={15} style={{justifyContent: 'center', alignItems: 'center'}}>
                </Col>
                </Row>
            </Grid>
            </Header>
        )
    }

    render() {
        return (
            <Container>
                <View style={{width: Dimensions.get('window').width, height: 60, backgroundColor: '#f6b93b', 
                justifyContent:'center', alignItems: 'flex-end', flexDirection:'row', paddingBottom: 15,
                marginTop: Platform.OS == "ios" ? 35 : 0}}>
                    <Image source={require('../assets/img/logo.png')} style={{width: 32, height: 32, paddingBottom: 3, 
                        marginRight: 4, tintColor: '#fff'}} />
                    <Image source={require('../assets/img/complete_logo.png')} style={{width: 144, height: 20, tintColor: '#fff'}} />
                </View>
                <Content contentContainerStyle={{flex: 1}} >
                    {/* <SafeAreaView> */}
                        {this._renderHeader()}
                        {
                            this.state._initial == true ?
                            null :
                            this.state.hasData ? 
                            <FlatList
                                data={this.state.favArray}
                                extraData={this.state}
                                keyExtractor={item => item.id}
                                style={{padding: 10}}
                                renderItem={(item)=>{
                                    let url = `${config.smallImg}/${item.item.small_image}`;
                                    return(
                                        <TouchableWithoutFeedback onPress={()=>{
                                            this.props.setGM(item.item);
                                            setTimeout(()=>{
                                                this.props.navigation.navigate('PotView');
                                            }, 500);
                                        }}>
                                        <Row style={{borderBottomColor: '#ddd', borderBottomWidth: 1, paddingBottom:10}}>
                                            <Col size={40} style={{marginRight: 7}}>
                                                <Image source={{uri: url}}
                                                    style = {{height: 130, resizeMode : 'stretch', margin: 5 }}/>
                                            </Col>
                                            <Col size={60} style={{padding:5}}>
                                                <Row size={15}>
                                                    <Icon name={'account'} size={20} color={'gray'} />
                                                    <Text>
                                                        {item.item.name}
                                                    </Text>
                                                </Row>
                                                <Row size={85}>
                                                <Label style={{fontSize: 24, color: 'black'}}>
                                                    {item.item.title}
                                                </Label>
                                                </Row>
                                            </Col>
                                        </Row>
                                        </TouchableWithoutFeedback>
                                    )
                                }}
                            />
                            :
                            <View style={{width: Dimensions.get('screen').width, height: Dimensions.get('screen').height/1.5,
                            justifyContent: 'center', alignItems: 'center'}}>
                                <View style={{borderWidth: 2, borderColor:primaryColor, padding: 25, borderRadius: 50, marginBottom: 10  }}>
                                    <Icon name="heart-broken" size={50} color={primaryColor} />
                                </View>
                                <Text style={{fontSize: 20}}>No favourites have been found.</Text>
                            </View>
                        }
                    {/* </SafeAreaView> */}
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
  bottom:{
    flex: 1, backgroundColor: 'white', 
    paddingTop: 25,
    paddingBottom: 20,
    paddingLeft: 30,
    paddingRight: 30, 
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30
  },
  title:{
    fontSize: 26,
    fontWeight: 'bold',
    color: 'black'
  },
  animated_button : {
    backgroundColor: '#f45302',
    borderRadius: 30,
    width: 350,
  },
  button_label: {
    color: '#fff',
    fontSize: 18
  }
});

const mapStateToProps = (state) => {
  return {
    favorites: state.favorites,
    loggedUser: state.loggedUser,
  }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getFavorites: (data) =>{
            dispatch(fetchFavorites(data))
        },
        reset: () => {
            dispatch(resetRedirectionToFavorites())
        },
        setGM: (data) =>{
            dispatch(setGOM(data))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Favorites);
