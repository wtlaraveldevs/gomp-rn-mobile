import React, {Component} from 'react';
import { 
  ActivityIndicator, StyleSheet, Text, View, ScrollView,
  Dimensions, ImageBackground, TouchableWithoutFeedback, Platform
} from 'react-native';
import { Container, Content, Header, Button, Card, CardItem, Left, Body, Right, Input, Title, Item, Label } from 'native-base';
import HeaderImageScrollView, { TriggeringView } from 'react-native-image-header-scroll-view';
import { connect } from 'react-redux';
import { config } from '../config/index';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Grid, Row, Col } from 'react-native-easy-grid';
import Dialog, { DialogContent } from 'react-native-popup-dialog';

// Scaling Button
import ScalingButton from './util/ScalingButton';
import { setFavorites, resetApiResponse } from '../redux/actions';

class PotView extends Component {

  state = {
    img : '',
    visible: true,
    liked: false,
    loader: false
  }

  componentWillMount(){
    this.setState({img: this.props.selected_gom.small_image});
    this.props.reset();
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.selected_gom != this.state.img){
      this.setState({img: this.props.selected_gom.small_image})
    }
    this.setState({ liked: nextProps.enableFavorite == 0 ? false : true })
  }

  _renderHeader(){
    return(
      <ImageBackground source={{ uri: `${config.smallImg}/${this.state.img}`}}
        style={{width: Dimensions.get('window').width, height: 300, marginTop: Platform.OS == "ios" ? 35 : 0}}>
        <Content>
          {/* <Header style={{marginLeft: -2, width: '102%', backgroundColor: 'transparent', borderColor: 'transparent' }}> */}
            <Grid>
              <Row>
                <Col size={15} style={{justifyContent: 'center', alignItems: 'center', padding: 5}}>
                  <Button transparent style={{borderColor: '#FCC438', borderWidth: 1, padding: 7}}
                  onPress={()=>{this.props.navigation.goBack()}}>
                    <Icon name="chevron-left" size={30} color={'#FCC438'} />
                  </Button>
                </Col>
                <Col size={65} style={{justifyContent: 'center', alignItems: 'center'}}>
                  
                </Col>
                <Col size={15} style={{justifyContent: 'center', alignItems: 'center'}}>
                </Col>
              </Row>
            </Grid>
          {/* </Header> */}
        </Content>
      </ImageBackground>
    )
  }

  _handlePress = () => {
    // alert("welcome");
    this.props.navigation.navigate('Payment');
  }

  _renderLoader = () =>{
    return(
      <View>
        <Dialog
          visible={this.state.loader}
          dialogStyle={{width: 200, padding: 20}}
        >
          <DialogContent>
            <ActivityIndicator size={"large"} color={"golden"} />
            <Text style={{fontSize: 20}}>Please wait...</Text>
          </DialogContent>
        </Dialog>
      </View>
    )
  }

  _handleLikes = (_id) => {
    
    if(this.props.loggedUser)
    {
      var obj = {
        user_id: this.props.loggedUser.id,
        id: _id
      }
      this.props.activateFavorite(obj);
    }
    else{
      this.props.navigation.navigate('Login');
    }
  }
  
  render() {
    return (
      <View>
        {this._renderHeader()}
        {this._renderLoader()}
        <View style={{backgroundColor: 'transparent', top: -40, height: '59%',}}>
          <View style={styles.bottom}>
              <View>
                {
                  this.state.liked ?
                  <Button onPress={this._handleLikes.bind(this, this.props.selected_gom.id)} transparent style={{position: 'absolute', top: -60, right: 0,
                    padding: 20, height: 70, borderColor: '#f6b93b', borderWidth: 2, backgroundColor: 'white', 
                    borderRadius: 40}}>
                    <Icon name={"heart"} size={30} color={'red'}/>
                  </Button> :
                  <Button onPress={this._handleLikes.bind(this, this.props.selected_gom.id)} transparent style={{position: 'absolute', top: -60, right: 0,
                  padding: 20, height: 70, borderColor: '#f6b93b', borderWidth: 2, backgroundColor: 'white', 
                  borderRadius: 40}}>
                    <Icon name={"heart-outline"} size={30} color={'red'}/>
                  </Button>
                }
              </View>
              <Text style={[styles.title, { marginBottom: 5 }]}>{this.props.selected_gom.title}</Text>
              <Text style={[styles.title, {fontSize: 16, color: 'black'}]}>By {this.props.selected_gom.name + " " + this.props.selected_gom.surname}</Text>
              <View style={{flexDirection: 'row', marginTop: 5}}>
                <Text style={[styles.title, {fontSize: 20, color: '#f6b93b', width: '50%'}]}>GYFTmoni Pot</Text>
                <Text style={[styles.title, {fontSize: 20, color: '#f6b93b', width: '50%', textAlign:'right'}]}>{this.props.selected_gom.token_id}</Text>
              </View>
              <View style={{backgroundColor: '#f6b93b', height: 2, marginTop: 10, marginBottom: 10}}></View>
              
              <View style={{flexDirection: 'row'}}>
                <Text style={[styles.title, {fontSize: 20, color: 'black', width: '50%'}]}>Total</Text>
                <Text style={[styles.title, {fontSize: 20, color: 'black', width: '50%', textAlign:'right'}]}>£{this.props.selected_gom.total}</Text>
              </View>
              <ScrollView style={{flex: 1}}>
                <Text style={{textAlign: 'justify'}}>{this.props.selected_gom.description}</Text>
              {/* <View style={{bottom: 20, right: 20, position: 'absolute'}}>
                <TouchableOpacity onPress={()=>{

                }} style={{width: 200, backgroundColor: '#f6b93b', height: 60, justifyContent: 'center',
                alignItems: 'center', borderRadius: 30}}>
                  <Text style={{color: 'white', fontSize: 18}}>Give</Text>
                </TouchableOpacity>
              </View> */}
              </ScrollView>
              
          </View>
          {
            this.props.newPot == true ? null :
            <View style={{width: Dimensions.get('screen').width}}>
              <View style={{alignItems: 'center',  width: Dimensions.get('screen').width, position: 'absolute', bottom: 0}}>
                <ScalingButton label="Give" 
                  onPress={this._handlePress.bind(this)}
                  styles={{button: styles.animated_button, label: styles.button_label}}
                >
                  <Text style={{color: 'white', fontSize: 20}}>Send Now</Text>
                </ScalingButton>
              </View>
            </View>
          }
          {/* <Image source={{ uri: `${config.largeImg}/${this.props.selected_gom.large_image}`}} style={{width: '100%', height: 300}} /> */}
        </View>
        {/* <HeaderImageScrollView
        maxHeight={300}
        minHeight={60}
        headerImage={{ uri: `${config.largeImg}/${this.props.selected_gom.large_image}`}}
        renderFixedForeground={() => (
            <Content>
              <Header style={{marginLeft: -2, width: '102%', backgroundColor: 'transparent', borderColor: 'transparent', borderWidth: 0}}>
                <Grid>
                  <Row>
                    <Col size={15} style={{justifyContent: 'center', alignItems: 'center'}}>
                      <Button transparent onPress={()=>{this.props.navigation.goBack()}}>
                        <Icon name="chevron-left" size={30} color={'#FCC438'} />
                      </Button>
                    </Col>
                    <Col size={65} style={{justifyContent: 'center', alignItems: 'center'}}>
                      
                    </Col>
                    <Col size={15} style={{justifyContent: 'center', alignItems: 'center'}}>
                    </Col>
                  </Row>
                </Grid>
              </Header>
            </Content>
        )}>
        <Content padder style={{height: 1200}}>
          <TriggeringView>
            <View style={{flex: 1, backgroundColor: 'yellow'}}>
            <Card style={{ height: 300, borderRadius: 10, marginTop: 0, marginBottom: 0, marginLeft: 0, marginRight: 0 }}>
              <CardItem header bordered style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Text style={{textAlign: 'center', fontSize: 20, color: 'blue'}}>Unique GOM ID</Text>
              </CardItem>
              <CardItem style={{height: 150,}}>
                  <Body>
                      <Text style={{fontSize: 20, textAlign: 'justify', marginBottom: '5%'}}>
                          ENTER THE UNIQUE GOMP NUMBER OF THE PERSON YOU WANT TO GIFT
                      </Text>
                      <Item regular>
                        <Input placeholder='GOM ID' placeholderTextColor={'gray'} />
                      </Item>
                  </Body>
              </CardItem>
              <CardItem footer style={{justifyContent: 'center', alignItems: 'center'}}>
                <Button style={{width: 200, borderRadius: 8, justifyContent: 'center', alignItems: 'center'}}>
                  <Text style={{color: 'white', fontSize: 20}}>SEARCH</Text>
                </Button>
              </CardItem>
            </Card>
            </View>
          </TriggeringView>
        </Content>
        </HeaderImageScrollView> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  bottom:{
    flex: 1, backgroundColor: 'white', 
    paddingTop: 25,
    paddingBottom: 20,
    paddingLeft: 30,
    paddingRight: 30, 
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30
  },
  title:{
    fontSize: 26,
    fontWeight: 'bold',
    color: 'black'
  },
  animated_button : {
    backgroundColor: '#f6b93b',
    borderRadius: 10,
    width: 300
  },
  button_label: {
    color: '#fff',
    fontSize: 18
  }
});

const mapStateToProps = (state) => {
  return {
    loggedUser: state.loggedUser,
    selected_gom: state.selected_gom,
    enableFavorite: state.enableFavorite,
    newPot: state.newPot
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    activateFavorite: (x) => {
      dispatch(setFavorites(x))
    },
    reset: () => {
      dispatch(resetApiResponse())
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PotView);
