import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Dimensions,
    Alert,
    ScrollView,
    ActivityIndicator,
    Image,
    TouchableOpacity,
    Platform,
} from 'react-native';
import { Container  } from 'native-base';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux'
import {Collapse,CollapseHeader, CollapseBody} from 'accordion-collapse-react-native';
import { primaryColor } from '../../assets/Util/colors';

class HowItWorks extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            view: 0
         }
    }
    render() { 
        return ( 
            <Container>
                <View style={{width: Dimensions.get('window').width, height: Platform.OS == "android" ? 60 : 85, backgroundColor: '#f6b93b', 
                justifyContent:'center', alignItems: 'flex-end',
                flexDirection:'row', paddingBottom: 15}}>
                    <View style={{position:'absolute', left: 0,
                    bottom: 0, justifyContent:'center', alignItems: 'flex-end'}}>
                        <TouchableOpacity style={{borderColor: '#fff', borderWidth: 1, padding: 10}} onPress={() => { this.props.navigation.pop(); }}>
                            <Icon name="chevron-left" size={30} color={'white'} />
                        </TouchableOpacity>
                    </View>
                    <View style={{justifyContent:'center', alignItems: 'center', flexDirection:'row',}}>
                        <Image source={require('../../assets/img/logo.png')} style={{width: 32, height: 32, paddingBottom: 3, 
                            marginRight: 4, tintColor: '#fff'}} />
                        <Image source={require('../../assets/img/complete_logo.png')} style={{width: 144, height: 20, tintColor: '#fff'}} />
                    </View>
                </View>
                <ScrollView style={{paddingHorizontal: 15, paddingVertical: 10}}>
                    <Collapse isCollapsed={this.state.view == 0 ? true : false} onToggle={()=>{this.setState({view: 0})}}>
                        <CollapseHeader style={{flexDirection: 'row', alignItems:'center', borderWidth: 1, 
                        borderColor: primaryColor, padding: 8, backgroundColor: primaryColor}}>
                            <View style={{flex: 0.9, marginLeft: 5}}>
                                <Text style={{fontSize: 20, color: "#fff"}}>Send GYFTmoni</Text>
                            </View>
                            <View style={{flex: 0.1}}>
                                <Icon name="chevron-down" size={30} color={"#fff"} />
                            </View>
                        </CollapseHeader>
                        <CollapseBody style={{padding: 8, borderColor: primaryColor, borderLeftWidth: 1,
                        borderRightWidth: 1, borderBottomWidth: 1, paddingBottom: 15}}>
                            <Text>To Send a Gift Of Money follow these easy steps:</Text>
                            <Text style={{marginTop: 5, marginLeft: 12}}>1. Click ‘Send GYFTmoni’</Text>
                            <Text style={{marginTop: 5, marginLeft: 12}}>2. Enter the UNIQUE GYFTmoni Pot number of the person you want to gift money to.</Text>
                            <Text style={{marginTop: 5, marginLeft: 12}}>3. Fill in some of your details and choose the AMOUNT you want to gift.</Text>
                            <Text style={{marginTop: 5, marginLeft: 12}}>4. Write a message (optional).</Text>
                            <Text style={{marginTop: 5, marginLeft: 12}}>5. Confirm and Checkout/Send.</Text>
                            <Text style={{marginTop: 5, marginLeft: 12}}>(The details we ask for allow a ‘Message’ and a ‘Thankyou’ to be sent ….and are also needed to comply with anti money laundering legislation.)</Text>
                        </CollapseBody>
                    </Collapse>
                    <Collapse isCollapsed={this.state.view == 1 ? true : false} onToggle={()=>{this.setState({view: 1})}}>
                        <CollapseHeader style={{flexDirection: 'row', alignItems:'center', borderWidth: 1, 
                        borderColor: primaryColor, padding: 8, backgroundColor: primaryColor}}>
                            <View style={{flex: 0.9, marginLeft: 5}}>
                                <Text style={{fontSize: 20, color: "#fff"}}>Create GYFTmoni Pot</Text>
                            </View>
                            <View style={{flex: 0.1}}>
                                <Icon name="chevron-down" size={30} color={"#fff"} />
                            </View>
                        </CollapseHeader>
                        <CollapseBody style={{padding: 8, borderColor: primaryColor, borderLeftWidth: 1,
                        borderRightWidth: 1, borderBottomWidth: 1, paddingBottom: 15}}>
                            <Text>To Receive Gifts Of Money follow these easy steps:</Text>
                            <Text style={{marginTop: 5, marginLeft: 12}}>1. Register with GYFTmoni</Text>
                            <Text style={{marginTop: 5, marginLeft: 12}}>2. Click on ‘Create GYFTmoni Pot’ and buy your £2 GYFTmoni Pot.</Text>
                            <Text style={{marginTop: 5, marginLeft: 12}}>3. We will send you your Unique GYFTmoni Pot Number.</Text>
                            <Text style={{marginTop: 5, marginLeft: 12}}>4. Share your Unique GYFTmoni Pot Number with your friends and Family / Facebook.</Text>
                            <Text style={{marginTop: 5, marginLeft: 12}}>(The details we ask for allow a ‘Message’ and a ‘Thankyou’ to be sent ….and are also needed to comply with anti money laundering legislation.) </Text>
                        </CollapseBody>
                    </Collapse>
                    <Collapse isCollapsed={this.state.view == 2 ? true : false} onToggle={()=>{this.setState({view: 2})}}>
                        <CollapseHeader style={{flexDirection: 'row', alignItems:'center', borderWidth: 1, 
                        borderColor: primaryColor, padding: 8, backgroundColor: primaryColor}}>
                            <View style={{flex: 0.9, marginLeft: 5}}>
                                <Text style={{fontSize: 20, color: "#fff"}}>Withdrawals</Text>
                            </View>
                            <View style={{flex: 0.1}}>
                                <Icon name="chevron-down" size={30} color={"#fff"} />
                            </View>
                        </CollapseHeader>
                        <CollapseBody style={{padding: 8, borderColor: primaryColor, borderLeftWidth: 1,
                        borderRightWidth: 1, borderBottomWidth: 1, paddingBottom: 15}}>
                            <Text>To withdraw Gifts Of Money follow these easy steps:</Text>
                            <Text style={{marginTop: 5, marginLeft: 12}}>1.	Click on ‘Get My Money’</Text>
                            <Text style={{marginTop: 5, marginLeft: 12}}>2.	Upload your Passport or Driver’s License ( only needed one time )</Text>
                            <Text style={{marginTop: 5, marginLeft: 12}}>3.	Upload your Bank Details ( only needed one time )</Text>
                            <Text style={{marginTop: 5, marginLeft: 12}}>4.	Click on ‘Withdraw’ when you want to send some of your GYFTmoni to your bank account.</Text>
                        </CollapseBody>
                    </Collapse>
                    <Collapse isCollapsed={this.state.view == 3 ? true : false} onToggle={()=>{this.setState({view: 3})}}>
                        <CollapseHeader style={{flexDirection: 'row', alignItems:'center', borderWidth: 1, 
                        borderColor: primaryColor, padding: 8, backgroundColor: primaryColor}}>
                            <View style={{flex: 0.9, marginLeft: 5}}>
                                <Text style={{fontSize: 20, color: "#fff"}}>Messages and ThankYous</Text>
                            </View>
                            <View style={{flex: 0.1}}>
                                <Icon name="chevron-down" size={30} color={"#fff"} />
                            </View>
                        </CollapseHeader>
                        <CollapseBody style={{padding: 8, borderColor: primaryColor, borderLeftWidth: 1,
                        borderRightWidth: 1, borderBottomWidth: 1, paddingBottom: 15}}>
                            <Text style={{fontSize: 16, fontWeight: 'bold', marginTop: 10}}>Messages</Text>
                            <Text style={{marginTop: 5, marginLeft: 12}}>To view messages from Gifters click on ‘Messages & ThankYous’.</Text>
                            
                            <Text style={{fontSize: 16, fontWeight: 'bold', marginTop: 10}}>Thank Yous</Text>
                            <Text style={{marginTop: 5, marginLeft: 12}}>1.	Update your Thankyou template if desired</Text>
                            <Text style={{marginTop: 5, marginLeft: 12}}>2.	Click on ‘Say Thank You’ to send your Thankyou message</Text>
                        </CollapseBody>
                    </Collapse>
                </ScrollView>
            </Container>
         );
    }
}
 
export default HowItWorks;