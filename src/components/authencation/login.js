/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  View,
  ActivityIndicator,
  Dimensions,
  Image,
  AsyncStorage,
  ImageBackground
} from 'react-native';
import { connect } from 'react-redux';
import { Container, Label, Content, Form, 
  Footer, Item, Card, Header, Input, Button,
  Text, } from 'native-base';
import Icon from 'react-native-vector-icons/Entypo';
import { login, hideLogin } from '../../redux/actions/index';
import { Grid, Row, Col } from 'react-native-easy-grid';
import messaging from '@react-native-firebase/messaging';
import { primaryColor, secondaryTextColor, primaryTextColor } from '../../assets/Util/colors';
import { TouchableOpacity } from 'react-native-gesture-handler';

class Login extends Component {

  state = {
    username: "",
    password: "",
    active: false,
    loader: false,
    isLoggedIn: false,
    showPassword: false,
    token: null
  };

  constructor(props)
  {
    super(props);
    this.handleLogin = this.handleLogin.bind(this);
  }

  handleLogin()
  {
    if(this.state.username == '')
    {
      alert("Please enter a valid username")
    }
    else if(this.state.password == '')
    {
      alert("Please enter password to proceed")
    }
    else {
      var data = {
        email: this.state.username,
        password: this.state.password
      }
      this.setState({loader: true})
      this.props.signIn(JSON.stringify(data))
    }
  }

  componentWillMount(){
    messaging().getToken().then((token) => {
      console.log("Ftoken", token);
      this.setState({token});
    })
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.loggedUser != null)
    {
      console.log("Logged User is not empty")
      this.props.navigation.pop();
      // this.setState({ isLoggedIn: true, user: nextProps.loggedUser.name, loader: false }) //isLoggedIn: nextProps.isLoggedIn,
    }
    else {
      console.log("Logged User is empty")
      this.setState({ loader: false, isLoggedIn: false })
      if(nextProps.apiStatus == undefined || nextProps.apiStatus == "failed" && nextProps.apiCount != this.state.apiCount && nextProps.apiCount != 0){
        alert("Invalid email or password.")
      }
    }

    console.log("NextProps", nextProps.loggedUser);

    // if(nextProps.loggedUser != null && nextProps.loggedUser != {})
    // {
    //   this.setState({ isLoggedIn: true, loader: false }) //isLoggedIn: nextProps.isLoggedIn,
    //   this.props.hideLoginView();
    //   this.props.navigation.goBack();
    // }
  }   

  _renderHeader(){
    return(
        // <Header style={{marginLeft: -2, width: '102%', backgroundColor: 'transparent' }}>
        <Grid style={{flex: 0.1, width: '100%'}}>
            <Row>
              <Col size={15} style={{justifyContent: 'center', alignItems: 'center'}}>
                  {/* <Button transparent onPress={()=>{
                    this.props.hideLoginView();
                    this.props.navigation.goBack();
                  }}>
                  <Icon name="chevron-left" size={30} color={'#FCC438'} />
                  </Button> */}
              </Col>
              <Col size={65} style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Text style={{fontSize: 20}}>Sign In</Text>
              </Col>
              <Col size={15} style={{justifyContent: 'center', alignItems: 'center'}}>
              </Col>
            </Row>
        </Grid>
        // </Header>
    )
  }

  render() {
    let {height, width} = Dimensions.get('window');
    const {navigate} = this.props.navigation;
    return (
      <Container>
        <Content keyboardShouldPersistTaps={'handled'} contentContainerStyle={{ flex: 1, alignItems:'center'}}>
        <ImageBackground style={{width: Dimensions.get('screen').width, flex: 1, alignItems:'center'}}
          source={require('../../assets/img/confettibg.png')}>
        <View style={{width: Dimensions.get('window').width, height: Platform.OS == "android" ? 60 : 95, backgroundColor: 'transparent', 
         flexDirection: 'row', zIndex: 4}}>
            <View style={{position:'absolute', left: 10, top: 10, bottom: 0, zIndex: 5, justifyContent:'center', alignItems: 'flex-end'}}>
              <TouchableOpacity style={{borderColor: '#000', borderWidth: 2, padding: 10, zIndex: 6}} 
              onPress={() => {
                this.props.navigation.pop(); 
              }}>
                <Icon name="chevron-left" size={30} color={'black'} />
              </TouchableOpacity>
            </View>
            <View style={{flex: 1, marginBottom: 15, justifyContent:'center', alignItems: 'flex-end', flexDirection:'row',}}>
              {/* <Image source={require('../../assets/img/logo.png')} style={{width: 32, height: 32, paddingBottom: 3, 
                  marginRight: 4, tintColor: primaryColor}} /> */}
              <Image source={require('../../assets/img/complete_logo.png')} style={{width: 144, height: 20, tintColor: primaryColor}} />
              {/* <Text style={{fontSize: 20}}>Sign In</Text> */}
            </View>
        </View>
        
        {/* {this._renderHeader()} */}
        <View style={{flex: 1, alignItems:'center', }}>
          <Image source={require('../../assets/img/logo.png')} style={{width: 112, height: 112, tintColor: primaryColor, marginVertical: 40}} />
          <Form style={{marginTop: 20,}} >
              <Item regular style={{...styles.input, marginBottom: 20}}>
                <Icon name="mail" size={23} style={{marginRight: 5, marginLeft: 5, color: primaryTextColor}} />
                <Input placeholderTextColor={primaryTextColor} style={{color: 'black'}} placeholder={"Username"} 
                onChangeText={(text)=>{this.setState({username: text})}} />
              </Item>
              <Item regular style={styles.input}>
                <Icon name="lock" size={23} style={{marginRight: 5, marginLeft: 5, color: primaryTextColor}} />
                <Input placeholderTextColor={primaryTextColor} style={{color: 'black'}} 
                secureTextEntry={!this.state.showPassword} placeholder={"Password"} 
                onChangeText={(text)=>{this.setState({password: text})}} />
                <TouchableOpacity onPress={() => { this.setState({showPassword: !this.state.showPassword}) }}>
                  {
                    this.state.showPassword ?
                    <Icon name="eye" type={"Entypo"} size={23} style={{marginRight: 5,color: primaryTextColor}} /> :
                    <Icon name="eye-with-line" type={"Entypo"} size={23} style={{marginRight: 5,color: primaryTextColor}} />
                  }
                </TouchableOpacity>
              </Item>
              {
                this.state.loader == true ?
                <View style={{backgroundColor: '#f6b93b', marginTop: 40, width: 300, borderRadius: 5, 
                padding: 7,
                justifyContent: 'center', alignContent: 'center'}}>
                    <ActivityIndicator size={"large"} color={"white"} />
                </View> :
                <Button onPress={()=>{
                  if(this.state.username == "" || this.state.username == null){
                    alert("Please provide a valid username.")
                  }else if(this.state.password == "" || this.state.password == null) {
                    alert("Please provide a valid password.")
                  }else{
                    var data = {
                      email: this.state.username,
                      password: this.state.password,
                      ftoken: this.state.token
                    }
                    this.setState({loader: true})
                    this.props.signIn(JSON.stringify(data))
                  }
                }} style={{backgroundColor: '#f6b93b', width: 300, marginTop: 40, borderRadius: 5, 
                justifyContent: 'center', alignContent: 'center'}}>
                    <Text style={{color: secondaryTextColor, fontSize: 18}}>Sign In</Text>
                </Button>
              }
              <Button transparent onPress={()=>{this.props.navigation.navigate('forgotpassword')}} 
              style={{ width: 300, justifyContent: 'center', alignContent: 'center', marginVertical: 10,}}>
                  <Text style={{color: primaryTextColor, fontSize: 16, textTransform: "none"}}>Forgot Password? Tap Here!</Text>
              </Button>

              <Button transparent onPress={()=>{this.props.navigation.navigate('Register')}} 
              style={{ width: 300, justifyContent: 'center', alignContent: 'center'}}>
                  <Text style={{color: primaryTextColor, fontSize: 16, textTransform: "none"}}>Register! Tap Here!</Text>
              </Button>

          </Form>
          </View>
          </ImageBackground>
        </Content>
      </Container>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F5FCFF'
  },
  input: {
    backgroundColor: 'white', 
    borderColor: 'black',  
    borderWidth: 4,
    borderRadius: 5,
    marginBottom: 10, 
    width: 300
  },
  backgroundImage: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  titleText:{
    color: '#0A60FF',
    fontSize: 18,
  },
  item:{
    marginBottom: 10,
    padding: 0
  },
  bottomView:{
    position: 'absolute',
    bottom: 0
  },
  
});

const mapStateToProps = (state) => {
  return {
    loggedUser: state.loggedUser,
    // isLoggedIn: state.isLoggedIn,
    showDialog: state.openLoginView,
    apiStatus: state.apiStatus,
    apiCount: state.apiCount
  }
}

const mapDispatchToProps = (dispatch) => {
    return{ 
      signIn : (data) => {
        dispatch(login(data))
      },
      hideLoginView: () => {
        dispatch(hideLogin())
      }
    }
}

export default connect (mapStateToProps, mapDispatchToProps)(Login);
