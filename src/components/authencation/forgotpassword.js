/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Alert,
  StyleSheet,
  View,
  ActivityIndicator,
  Dimensions,
  Image,
  AsyncStorage,
  Platform
} from 'react-native';
import { connect } from 'react-redux';
import { Container, Label, Content, Form, 
  Footer, Item, Card, Header, Input, Button,
  Text, } from 'native-base';
import Icon from 'react-native-vector-icons/Entypo';
import { login, hideLogin } from '../../redux/actions/index';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { config } from '../../config';

class ForgotPassword extends Component {

  state = {
    active: false,
    loader: false,
    isLoggedIn: false,
    email: ''
  };

  constructor(props)
  {
    super(props);
    this.handleLogin = this.handleLogin.bind(this);
  }

  handleLogin()
  {
    if(this.state.email == '')
    {
      alert("Please provide a valid email address.")
    }
    else {
      var data = {
        email: this.state.email
      }
      this.setState({loader: true})
      
      fetch(`${config.api.forgotpassword}`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(data)
      })
      .then((response) => { return response.json() })
      .then((response) => {

        if(response.status)
        {
          Alert.alert(
            "Success",
            "You will shortly receive a 6 digit code by email. Please use it to reset your password after you press ok.",
            [
              { text: "OK", onPress: () => { this.props.navigation.replace("verificationcode") } }
            ],
            { cancelable: false }
          );
        }
        else{
          alert(response.message);
        }
      })
    }
  }

  componentWillReceiveProps(nextProps) {
    console.log("NextProps", nextProps.loggedUser);
  }  

  _renderHeader(){
    return(
        <Header style={{marginLeft: -2, width: '102%', backgroundColor: 'transparent' }}>
        <Grid>
            <Row>
            <Col size={15} style={{justifyContent: 'center', alignItems: 'center'}}>
                <Button transparent onPress={()=>{
                  this.props.navigation.goBack();
                }}>
                <Icon name="chevron-left" size={30} color={'#FCC438'} />
                </Button>
            </Col>
            <Col size={65} style={{justifyContent: 'center', alignItems: 'center'}}>
                <Text style={{fontSize: 20}}>Forgot Password</Text>
            </Col>
            <Col size={15} style={{justifyContent: 'center', alignItems: 'center'}}>
            </Col>
            </Row>
        </Grid>
        </Header>
    )
  }

  render() {
    let {height, width} = Dimensions.get('window');
    const {navigate} = this.props.navigation;
    return (
      <Container>
        <View style={{width: Dimensions.get('window').width, height: Platform.OS == "android" ? 60 : 85, backgroundColor: '#f6b93b', 
        justifyContent:'center', alignItems: 'flex-end',
        flexDirection:'row', paddingBottom: 15}}>
            <View style={{justifyContent:'center', alignItems: 'center', flexDirection:'row',}}>
              <Image source={require('../../assets/img/logo.png')} style={{width: 32, height: 32, paddingBottom: 3, 
                  marginRight: 4, tintColor: '#fff'}} />
              <Image source={require('../../assets/img/complete_logo.png')} style={{width: 144, height: 20, tintColor: '#fff'}} />
            </View>
        </View>
        {this._renderHeader()}
        <Content contentContainerStyle={{alignItems:'center', paddingTop: 30}}>
          <Image 
            source={require('../../assets/img/logo.png')} style={{width: 196, height: 196, marginBottom: 40}}
          />
          <Text style={{paddingHorizontal: 55, marginBottom: 10, fontSize: 18}}>Please provide your email address. We will send you a 6 digit code by email.</Text>
          <Form>
              <Item regular style={{backgroundColor: 'transparent', borderColor: 'black',  marginBottom: 20, width: 300 }}>
                <Icon name="mail" size={23} style={{marginRight: 5, marginLeft: 5, color: '#f6b93b'}} />
                <Input placeholderTextColor={'#f6b93b'} style={{color: 'black'}} placeholder={"Email Address"} 
                onChangeText={(text)=>{this.setState({email: text})}} />
              </Item>
              {
                this.state.loader == true ?
                <View style={{backgroundColor: '#f6b93b', width: 300, borderRadius: 5, 
                padding: 7,
                justifyContent: 'center', alignContent: 'center'}}>
                    <ActivityIndicator size={"large"} color={"white"} />
                </View> :
                <Button onPress={this.handleLogin} style={{backgroundColor: '#f6b93b', width: 300, borderRadius: 5, 
                justifyContent: 'center', alignContent: 'center'}}>
                    <Text style={{color: 'white', fontSize: 18}}>Send</Text>
                </Button>
              }
          </Form>
        </Content>
      </Container>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F5FCFF'
  },
  backgroundImage: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  titleText:{
    color: '#0A60FF',
    fontSize: 18,
  },
  item:{
    marginBottom: 10,
    padding: 0
  },
  bottomView:{
    position: 'absolute',
    bottom: 0
  },
  
});

const mapStateToProps = (state) => {
  return {
    loggedUser: state.loggedUser,
    // isLoggedIn: state.isLoggedIn,
  }
}

const mapDispatchToProps = (dispatch) => {
    return{ 
      signIn : (data) => {
        dispatch(login(data))
      },
      hideLoginView: () => {
        dispatch(hideLogin())
      }
    }
}

export default connect (mapStateToProps, mapDispatchToProps)(ForgotPassword);
