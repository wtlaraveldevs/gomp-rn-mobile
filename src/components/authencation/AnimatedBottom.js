import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  Animated,
  ActivityIndicator,
  AsyncStorage,
  Platform,
  TouchableOpacity,
  ImageBackground
} from 'react-native';
import { Container, Label, Content, Form, Thumbnail, 
    Footer, Item, Card, Header, CardItem, Input, Button, Left, Right, Body, Icon,
    CheckBox, ListItem } from 'native-base';
import { connect } from 'react-redux';
import { login, hideLogin, logout, redirectToMyAccount, redirectToFavorites, redirectToGoals, resetApiResponse } from '../../redux/actions/index';
import messaging from '@react-native-firebase/messaging';
import { config } from '../../config/index';

class AnimatedBottom extends Component {
 
    constructor(props) {
      super(props);
      this.y_translate = new Animated.Value(0);
      this.state = {
        menu_expanded: false,
        username: '',
        password: '',
        isLoggedIn: false,
        user: '',
        loader: false,
        showPassword: false,
        apiCount: this.props.apiCount
      };
      this._redirectToRegister = this._redirectToRegister.bind(this);
    }

    componentWillMount(){
      AsyncStorage.getItem('isLoggedIn').then((item)=>{
        if(item == "true")
        {
          this.setState({ isLoggedIn: true })
          this.sendTokenToServer();
        }
        else{
          this.setState({ isLoggedIn: false })
        }
      })
      AsyncStorage.getItem('user').then((item)=>{
        if(item != null)
        {
          var obj = JSON.parse(item);
          this.setState({user: obj.name})
        }
      })

      AsyncStorage.multiGet(["isLoggedIn", "users"]).then((payload) => {
        if(payload[0][1] == "true" && payload[1][1] != null)
        {
          var obj = JSON.parse(payload[1][1]);
          this.setState({user: obj.name, isLoggedIn: true })
          // this.sendTokenToServer(obj.id);
        }
        else{
          this.setState({ isLoggedIn: false })
          messaging().getToken().then((token) => {
            console.log("Ftoken", token);
            this.setState({token});
          })
        }
      })
    }

    // sendTokenToServer = (user_id) => {
    //   try{
    //     console.log("Logged In User ID", user_id);
    //     messaging().requestPermission()
    //     .then((authStatus) => {
    //       if(authStatus){
    //         // get token from messaging.
    //         messaging().getToken().then((token) => {
    //           console.log("Ftoken before sending to server ", token);
    //           this.setState({token});
  
    //           var data = {
    //             ftoken: token,
    //             user_id: user_id
    //           }
  
    //           fetch(`${config.api.updateToken}`, {
    //             method: 'POST',
    //             headers: {
    //                 'Accept': 'application/json',
    //                 'Content-Type': 'application/json',
    //             },
    //             body: JSON.stringify(data)
    //           })
    //           .then(response=> { return response.json(); })
    //           .then((response) => {
    //               console.log(response);
    //           })
    //           .catch((error) => {
    //               console.error(error);
    //           });
    //         })
    //       }
    //       // const enabled =
    //       //   authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
    //       //   authStatus === messaging.AuthorizationStatus.PROVISIONAL;
        
    //       // if (enabled) {
    //       //   console.log('Authorization status:', authStatus);
    //       // }
    //     })
        
    //   }
    //   catch(err){
    //     console.log("Error in sending ftoken to the server", err);
    //   }
    // }

    openMenu() {
      if(Platform.OS === 'ios' && !this.props.loggedUser)
      {
        this.props.navigation.navigate('Login');
        return;
      }
      this.setState({
        menu_expanded: true
      }, () => {
        this.y_translate.setValue(0);
        Animated.spring(
          this.y_translate,
          {
            toValue: 1,
          //   friction: 3
          }
        ).start();
      });
    }
    
    hideMenu() {
        this.setState({
          menu_expanded: false
        }, () => {
          this.y_translate.setValue(1);
          Animated.spring(
            this.y_translate,
            {
              toValue: 0,
            //   friction: 4
            }
          ).start();
        });
        this.props.hideLoginDialog();
    }

    _redirectToRegister = () =>{
      this.hideMenu();
      setTimeout(() => {
        this.props.navigation.navigate('Register')
      }, 300);
    }

    handleUsername = (text) => {
      this.setState({ username: text })
    }

    handlePassword = (text) => {
      this.setState({ password: text })
    }

    componentWillReceiveProps(nextProps) {
      if(nextProps.loggedUser != null)
      {
        console.log("Logged User is not empty")
        this.setState({ isLoggedIn: true, user: nextProps.loggedUser.name, loader: false }) //isLoggedIn: nextProps.isLoggedIn,
      }
      else {
        console.log("Logged User is empty")
        this.setState({ loader: false, isLoggedIn: false })
      }

      console.log("NextProps", nextProps.loggedUser);

      if(nextProps.apiStatus == undefined || nextProps.apiStatus == "failed" && nextProps.apiCount != this.state.apiCount && nextProps.apiCount != 0){
        alert("Invalid email or password.")
      }

      if(nextProps.showDialog == true)
      {
        this.openMenu();
      }
    }  

    _renderLoginView(){
      return (
        <View style={{width: Dimensions.get('window').width , flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Form>
              <Item regular style={{backgroundColor: 'transparent', borderColor: 'black',  marginBottom: 10, width: 300 }}>
              <Icon name="mail" size={23} style={{marginRight: 5, color: '#f6b93b'}} />
              <Input placeholderTextColor={'#f6b93b'} style={{color: 'black'}} placeholder={"Username"} onChangeText = {this.handleUsername.bind(this)} />
              </Item>
              <Item regular style={{backgroundColor: 'transparent', borderColor: 'black', marginBottom: 20,  width: 300 }}>
              <Icon name="lock" size={23} style={{marginRight: 5,color: '#f6b93b'}} />
              <Input placeholderTextColor={'#f6b93b'} style={{color: 'black'}} 
              secureTextEntry={!this.state.showPassword} 
              placeholder={"Password"} onChangeText = {this.handlePassword.bind(this)} />
              <TouchableOpacity onPress={() => { this.setState({showPassword: !this.state.showPassword}) }}>
                {
                  this.state.showPassword ?
                  <Icon name="eye" type={"Entypo"} size={23} style={{marginRight: 5,color: '#f6b93b'}} /> :
                  <Icon name="eye-with-line" type={"Entypo"} size={23} style={{marginRight: 5,color: '#f6b93b'}} />
                }
              </TouchableOpacity>
              </Item>
              {
                this.state.loader == true ?
                <View style={{backgroundColor: '#f6b93b', width: 300, borderRadius: 5, 
                padding: 7,
                justifyContent: 'center', alignContent: 'center'}}>
                    <ActivityIndicator size={"large"} color={"white"} />
                </View> :
                <Button onPress={()=>{
                  if(this.state.username == "" || this.state.username == null){
                    alert("Please provide a valid username");
                  }
                  else if(this.state.password == "" || this.state.password == null){
                    alert("Please provide a password");
                  }
                  else {
                    var data = {
                      email: this.state.username,
                      password: this.state.password,
                      ftoken: this.state.token
                    }
                    this.setState({loader: true})
                    this.props.signIn(JSON.stringify(data))
                  }
                }} style={{backgroundColor: '#f6b93b', width: 300, borderRadius: 5, 
                justifyContent: 'center', alignContent: 'center'}}>
                    <Text style={{color: 'white', fontSize: 18}}>Sign In</Text>
                </Button>
              }
              <Button transparent onPress={()=>{this.props.navigation.navigate('forgotpassword')}} 
              style={{ width: 300, justifyContent: 'center', alignContent: 'center'}}>
                  <Text style={{color: '#f6b93b', fontSize: 18}}>Forgot Password? Tap Here!</Text>
              </Button>
              <Button transparent onPress={this._redirectToRegister} 
              style={{ width: 300, justifyContent: 'center', alignContent: 'center'}}>
                  <Text style={{color: '#f6b93b', fontSize: 18}}>Register, Tap Here!</Text>
              </Button>
          </Form>
        </View>
      )
    }

    _renderDashboardView(){
      return (
        <View style={{flex: 1, backgroundColor: 'transparent', marginTop: 10}}>
          {/* <Button onPress={()=>{ this.props.redirectToGoalView() }} 
          style={{backgroundColor: '#f6b93b', width: 300, borderRadius: 5, marginBottom: 10,
          justifyContent: 'center', alignContent: 'center', borderColor: '#f6b93b', borderWidth: 1}}>
              <Text style={{color: 'white', fontSize: 18}}>My GYFTmoni Pots</Text>
          </Button> */}
          <Button onPress={()=>{ this.props.redirectToFav() }} 
          style={{backgroundColor: '#f6b93b', width: 300, borderRadius: 5, marginBottom: 10,
          justifyContent: 'center', alignContent: 'center'}}>
              <Text style={{color: 'white', fontSize: 18}}>Favourites</Text>
          </Button>
          <Button onPress={()=> { this.props.redirectToAccountView() }} 
          style={{backgroundColor: '#f6b93b', width: 300, borderRadius: 5, marginBottom: 10,
          justifyContent: 'center', alignContent: 'center'}}>
              <Text style={{color: 'white', fontSize: 18}}>Account Settings</Text>
          </Button>
          <Button onPress={()=>{
            AsyncStorage.clear();
            this.hideMenu();
            this.setState({ isLoggedIn: false })
            this.props.signout();
          }} 
          style={{backgroundColor: '#f6b93b', width: 300, borderRadius: 5, marginBottom: 10,
          justifyContent: 'center', alignContent: 'center'}}>
              <Text style={{color: 'white', fontSize: 18}}>Log out</Text>
          </Button>
        </View>
      )
    }
  
    render() {
        const menu_moveY = this.y_translate.interpolate({
            inputRange: [0, 1],
            outputRange: [0, -300]
        });
        return (
            <Animated.View 
            style={[ 
                styles.footer_menu,
                {
                transform: [
                    {
                    translateY: menu_moveY
                    }
                ]
                }
            ]}
            >
              <ImageBackground style={{width:'100%', height: '100%', alignItems: 'center',}}
              source={require('../../assets/img/template_border_old.jpg')}>
                {
                    this.state.menu_expanded ? 
                    <Button transparent onPress={this.hideMenu.bind(this)} style={{marginTop: 5,
                      width: 50, justifyContent: 'flex-end', alignSelf: 'flex-end' }}>
                        <Icon name={'close'} size={30} style={{color: '#f6b93b', fontSize: 20}} />
                    </Button>
                    :
                    <Button transparent onPress={this.openMenu.bind(this)} style={styles.avatar}>
                        <Thumbnail small style={{marginRight: 10}} source={require('../../assets/img/default_avatar.jpg')} />
                        {
                          this.state.isLoggedIn ?
                            <View style={{flexDirection: 'row'}}>
                              <Text style={{fontSize: 18, color: '#f6b93b'}}>{this.state.user}</Text>
                              <Icon size={40} style={{color: '#fea500'}} name={"menu"} />
                            </View> :
                            <Text style={{fontSize: 18, color: 'black'}}>Sign in / Register</Text>
                        }
                    </Button>
                }
                {
                    this.state.menu_expanded && this.state.isLoggedIn == false ? 
                    <View>
                        <Text style={{color: '#f6b93b', fontSize: 22}}>SIGN IN</Text>
                    </View>
                    : null
                }
                {
                  this.state.isLoggedIn ? 
                  this._renderDashboardView()
                  :
                  this._renderLoginView()
                }
              </ImageBackground>
            </Animated.View>
        )
    }

}

	
const styles = StyleSheet.create({
    container: {
      flex: 10,
      flexDirection: 'column'
    },
    body: {
      flex: 10,
      backgroundColor: '#ccc'
    },
    footer_menu: {
      position: 'absolute',
      width: Dimensions.get('window').width,
      height: 360, 
      bottom: -300,
      backgroundColor: '#fff',
      alignItems: 'center',
      borderColor: '#c97f00', //'#f6b93b',
      borderWidth: 2,
      zIndex: 1,
    },
    tip_menu: {
      flexDirection: 'row'
    },
    button: {
      backgroundColor: '#fff'
    },
    button_label: {
      fontSize: 20,
      fontWeight: 'bold'
    },
    avatar : {
        justifyContent: 'center',
        alignItems: 'center',
        width: Dimensions.get('screen').width,
        marginBottom: 10,
        backgroundColor: 'white', //'#ff7f50', //'#f6b93b',
        height: 60
    }
});

const mapStateToProps = (state) => {
  return {
    loggedUser: state.loggedUser,
    // isLoggedIn: state.isLoggedIn,
    showDialog: state.openLoginView,
    apiStatus: state.apiStatus,
    apiCount: state.apiCount
  }
}

const mapDispatchToProps = (dispatch) => {
    return{ 
      signIn : (data) => {
        dispatch(login(data))
      },
      signout: () =>{
        dispatch(logout())
      },
      hideLoginDialog: () =>{
        dispatch(hideLogin())
      },
      redirectToFav: () => {
        dispatch(redirectToFavorites())
      },
      redirectToGoalView: () => {
        dispatch(redirectToGoals())
      },
      redirectToAccountView: () => {
        dispatch(redirectToMyAccount())
      },
      resetResponse: () =>{
        dispatch(resetApiResponse())
      }
    }
}

export default connect (mapStateToProps, mapDispatchToProps)(AnimatedBottom);