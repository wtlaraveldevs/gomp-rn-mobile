/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Alert,
  StyleSheet,
  View,
  ActivityIndicator,
  Dimensions,
  Image,
  AsyncStorage
} from 'react-native';
import { connect } from 'react-redux';
import { Container, Label, Content, Form, 
  Footer, Item, Card, Header, Input, Button,
  Text, } from 'native-base';
import Icon from 'react-native-vector-icons/Entypo';
import { login, hideLogin } from '../../redux/actions/index';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { config } from '../../config';
import thunk from 'redux-thunk';

class CodeVerification extends Component {

  state = {
    active: false,
    loader: false,
    isLoggedIn: false,
    code: ''
  };

  constructor(props)
  {
    super(props);
    this.handleVerification = this.handleVerification.bind(this);
  }

  handleVerification()
  {
    if(this.state.code == '' || this.state.code == null)
    {
      alert("Invalid verification code.")
    }
    else {
      var data = {
        token: this.state.code
      }
      this.setState({loader: true})
      
      fetch(`${config.api.tokenverification}`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(data)
      })
      .then((response) => { return response.json() })
      .then((response) => {
        if(response.status)
        {
          // Alert.alert(
          //   "Success",
          //   "You will an email with verification code in few minutes. Please use that to complete the reset password.",
          //   [
          //     { text: "OK", onPress: () => { this.props.navigation.navigate("verificationcode") } }
          //   ],
          //   { cancelable: false }
          // );
          this.props.navigation.replace("resetpassword", {
            token: this.state.code
          });
        }
        else{
          alert(response.message);
        }
      })
    }
  }

  componentWillReceiveProps(nextProps) {
    console.log("NextProps", nextProps.loggedUser);
  }  

  _renderHeader(){
    return(
        <Header style={{marginLeft: -2, width: '102%', backgroundColor: 'transparent' }}>
        <Grid>
            <Row>
            <Col size={15} style={{justifyContent: 'center', alignItems: 'center'}}>
                <Button transparent onPress={()=>{
                  this.props.navigation.goBack();
                }}>
                <Icon name="chevron-left" size={30} color={'#FCC438'} />
                </Button>
            </Col>
            <Col size={65} style={{justifyContent: 'center', alignItems: 'center'}}>
                <Text style={{fontSize: 20}}>To Change Password</Text>
            </Col>
            <Col size={15} style={{justifyContent: 'center', alignItems: 'center'}}>
            </Col>
            </Row>
        </Grid>
        </Header>
    )
  }

  render() {
    let {height, width} = Dimensions.get('window');
    const {navigate} = this.props.navigation;
    return (
      <Container>
        <View style={{width: Dimensions.get('window').width, height: 60, backgroundColor: '#f6b93b', 
        justifyContent:'center', alignItems: 'flex-end', marginTop: Platform.OS == "ios" ? 35 : 0,
        flexDirection:'row', paddingBottom: 15}}>
            <View style={{justifyContent:'center', alignItems: 'center', flexDirection:'row',}}>
              <Image source={require('../../assets/img/logo.png')} style={{width: 32, height: 32, paddingBottom: 3, 
                  marginRight: 4, tintColor: '#fff'}} />
              <Image source={require('../../assets/img/complete_logo.png')} style={{width: 144, height: 20, tintColor: '#fff'}} />
            </View>
        </View>
        {this._renderHeader()}
        <Content contentContainerStyle={{alignItems:'center', paddingTop: 30}}>
          <Image 
            source={require('../../assets/img/logo.png')} style={{width: 196, height: 196, marginBottom: 40}}
          />
          <Text style={{paddingHorizontal: 55, marginBottom: 10, fontSize: 18}}>Please enter the 6 digit code from the email we sent to you in the box below.</Text>
          <Form>
              <Item regular style={{backgroundColor: 'transparent', borderColor: 'black',  marginBottom: 20, width: 300 }}>
                <Input placeholderTextColor={'#f45302'} style={{color: 'black'}} placeholder={"Enter 6-digit Code"} 
                onChangeText={(text)=>{this.setState({code: text})}} />
              </Item>
              {
                this.state.loader == true ?
                <View style={{backgroundColor: '#f45302', width: 300, borderRadius: 5, 
                padding: 7,
                justifyContent: 'center', alignContent: 'center'}}>
                    <ActivityIndicator size={"large"} color={"white"} />
                </View> :
                <Button onPress={this.handleVerification} style={{backgroundColor: '#f6b93b', width: 300, borderRadius: 5, 
                justifyContent: 'center', alignContent: 'center'}}>
                    <Text style={{color: 'white', fontSize: 18}}>Verify</Text>
                </Button>
              }
          </Form>
        </Content>
      </Container>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F5FCFF'
  },
  backgroundImage: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  titleText:{
    color: '#0A60FF',
    fontSize: 18,
  },
  item:{
    marginBottom: 10,
    padding: 0
  },
  bottomView:{
    position: 'absolute',
    bottom: 0
  },
  
});

const mapStateToProps = (state) => {
  return {
    loggedUser: state.loggedUser,
    // isLoggedIn: state.isLoggedIn,
  }
}

const mapDispatchToProps = (dispatch) => {
    return{ 
      signIn : (data) => {
        dispatch(login(data))
      },
      hideLoginView: () => {
        dispatch(hideLogin())
      }
    }
}

export default connect (mapStateToProps, mapDispatchToProps)(CodeVerification);
