/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */   

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  Alert,
  ScrollView,
  ActivityIndicator,
  Image,
  ImageBackground,
  Platform,
} from 'react-native';
import { Item, Form, Content, Input, DatePicker, Picker, 
  Button, ListItem, CheckBox, Body  } from 'native-base';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux'
import { clearRegistration, getAllCountries, register } from '../../redux/actions/index';
import { Grid, Row, Col } from 'react-native-easy-grid';
import moment from 'moment';
import { primaryColor, primaryTextColor } from '../../assets/Util/colors';
import { TouchableOpacity } from 'react-native-gesture-handler';

class Register extends Component {

  constructor(props) {
    super(props);
  }

  state = {
    firstname: null,
    surname: null,
    nickname: null,
    address: null,
    mobile: null,
    email   : null,
    confirmEmail: null,
    password: null,
    confirmPassword: null,
    country: null,
    firstview: true,
    showPassword: false,
    regAttempt: 0,
    loading: false,
    showConfirmPassword: false,
    condition: false,
    year: 2020,
    month: "01", day: "01",
    city: '', address1: '', address2: '', postcode: '',
    countries: []
  }

  componentWillMount(){
    this.props.getCountries();
    this.createYearPickerValues();
  }

  createYearPickerValues(){
    try{
      var year = new Date().getFullYear();

      var _years = [];
      for (let index = year; index > 1940; index--) {
        _years.push(String(index));
      }

      console.log("Years", _years);

      this.setState({
        years: _years.map((value, i) => {
            return (
                <Picker.Item key={i} label={value} value={value}/>
            )
          }
        ) 
      })
    }
    catch(error){
      console.log("Unable to create year picker " + error);
    }
  }

  componentWillReceiveProps(nextProps)
  {
    // if(nextProps.regAttempt != this.state.regAttempt)
    // {
    //   if(nextProps.regComplete == true){
    //     this.props.navigation.replace('DrawerStack')
    //   }
    //   else{
    //     alert("Something went wrong, Please try again. thank you.");
    //   }
    // }
    if(nextProps.regResponse != '' && nextProps.regResponse != null)
    {
      this.setState({loading: false})
      if(nextProps.regResponse.status == true)
      {
        Alert.alert(
          "Congratulations",
          "You have successfully registered",
          [
            { text: "OK", onPress: () => { 
              this.props.navigation.navigate('DrawerStack')
              
              // clear response from redux store.
              this.props.clearRegistrationObject();
            } }
          ],
          { cancelable: false }
        );
      }
      else{
        Alert.alert(
          "Failed",
          nextProps.regResponse.message,
          [
            { text: "OK", onPress: () => { 
              // cleared the response from redux store.
              this.props.clearRegistrationObject();
            } }
          ],
          { cancelable: false }
        );
      }
    }

    if(nextProps.countriesResponse){
      if(nextProps.countriesResponse.status){
        // get countries from response
        var countries = nextProps.countriesResponse.countries;
        this.setState({countries});
      }
      else {
        // notify the user with an alert.
        alert(nextProps.countriesResponse.message);
      }
    }

  }

  onClickListener = () => {
    if(this.state.mobile == null  || this.state.country == null || 
      this.state.postcode == null || this.state.address1 == null || this.state.city == null)
      {
        alert("Please enter valid information in all the fields. Thank you");
      }
      else {
        // if(this.state.email != this.state.confirmEmail)
        // {
        //   alert("Email and Confirm Email does not match.")
        // }
        // else if(this.state.password != this.state.confirmPassword)
        // {
        //   alert("Password and Confirm Password does not match")
        // }
        // else{

          this.setState({loading: true})
          // call registration api.
          var _dob = this.state.year + "-" + this.state.month + "-" + this.state.day;
          var _address = this.state.address1 + " " + this.state.address2 + " " + this.state.city + " " + this.state.postcode;

          var obj={
            forename: this.state.firstname,
            surname: this.state.surname,
            nick: this.state.nickname,
            email: this.state.email,
            mobile: this.state.mobile,
            password: this.state.password,
            address: _address,
            dob: _dob,
            country: this.state.country
          };

          this.props.regUser(obj);
        // }
      }
  }

  componentDidMount(){
    this.setState({ regAttempt: this.props.regAttempt })
  }

  render() {

    const countryItems = this.state.countries.map((value, index)=>{
        return <Picker.Item label={value.country_name} key={index} value={value.id} />
    })

    return (
      <Content contentContainerStyle={styles.container} keyboardShouldPersistTaps={true}>
        <ScrollView contentContainerStyle={{justifyContent:'center', alignItems: 'center'}}>
        <ImageBackground style={{width: Dimensions.get('screen').width,  alignItems:'center'}}
          source={require('../../assets/img/confettibg.png')}>
        <View style={{width: Dimensions.get('window').width, height: Platform.OS == "android" ? 60 : 90, backgroundColor: 'transparent', 
         flexDirection: 'row', zIndex: 4}}>
            <View style={{position:'absolute',zIndex: 5, left: 10, top: 10, bottom: 0, justifyContent:'center', alignItems: 'flex-end'}}>
              <TouchableOpacity style={{borderColor: '#000', borderWidth: 2, padding: 10, zIndex: 6}} 
              onPress={() => {
                this.props.clearRegistrationObject();
                this.props.navigation.pop(); 
              }}>
                <Icon name="chevron-left" size={30} color={'black'} />
              </TouchableOpacity>
            </View>
            <View style={{flex: 1, marginBottom: 15, justifyContent:'center', alignItems: 'flex-end', flexDirection:'row',}}>
              {/* <Image source={require('../../assets/img/logo.png')} style={{width: 32, height: 32, paddingBottom: 3, 
                  marginRight: 4, tintColor: primaryColor}} /> */}
              <Image source={require('../../assets/img/complete_logo.png')} style={{width: 144, height: 20, tintColor: primaryColor}} />
            </View>
        </View>
        <Grid style={{opacity: 1}} >
        <Row size={25} style={{marginLeft: 15, marginTop: 5, marginBottom: 30, 
          marginRight: 15, justifyContent: 'center', alignItems: 'flex-end'}}> 
          <Image source={require('../../assets/img/logo.png')} style={{width: 96, height: 96, tintColor: primaryColor}} />
        </Row>
        <Row size={55}>
        <Form> 
        {
          (this.state.firstview == true) ?
          <Col>
            <Item regular style={styles.itemContainer}>
              <Icon name="account" size={23} style={{marginRight: 5, marginLeft: 5, color: primaryTextColor}} />
              <Input placeholderTextColor={primaryTextColor} style={{color: 'black'}} placeholder={"First Name"} 
              onChangeText = {(text)=>{ this.setState({firstname: text})}} />
            </Item>
            <Item regular style={styles.itemContainer}>
              <Icon name="account" size={23} style={{marginRight: 5, marginLeft: 5, color: primaryTextColor}} />
              <Input placeholderTextColor={primaryTextColor} style={{color: 'black'}} placeholder={"Surname"} 
              onChangeText = {(text)=>{ this.setState({surname: text})}} />
            </Item>
            <Item regular style={styles.itemContainer}>
              <Icon name="account" size={23} style={{marginRight: 5, marginLeft: 5, color: primaryTextColor}} />
              <Input placeholderTextColor={primaryTextColor} style={{color: 'black'}} placeholder={"Name you like to be known by"} 
              onChangeText = {(text)=>{ this.setState({nickname: text})}} />
            </Item>
            {/* <Item regular style={styles.itemContainer}>
              <Icon name="calendar" size={23} style={{marginRight: 5, marginLeft: 5, color: '#f6b93b'}} />
              <DatePicker
                defaultDate={new Date()}
                locale={"en"}
                timeZoneOffsetInMinutes={undefined}
                modalTransparent={false}
                animationType={"fade"}
                androidMode={"default"}
                placeHolderText="Select Date of Birth"
                formatChosenDate={date => {return moment(date).format('L');}}
                textStyle={{ color: "#f6b93b" }}
                placeHolderTextStyle={{ color: "#d3d3d3" }}
                onDateChange={(date)=>{this.setState({ dob: date });}}
                disabled={false}
              />
            </Item> */}
            <Text>Date of Birth</Text>
            <View style={{flexDirection: 'row', width: 300}}>
              <Item regular style={{...styles.itemContainer, width: '30%'}}>
                <Picker
                    note
                    mode="dialog"
                    itemTextStyle={{color: 'black'}}
                    selectedValue={this.state.day}
                    onValueChange={(item)=>{this.setState({ day: item });}}
                >
                    {/* <Picker.Item value="-1" label="Day"/> */}
                    <Picker.Item value="01" label="01"></Picker.Item>
                    <Picker.Item value="22" label="02"></Picker.Item>
                    <Picker.Item value="03" label="03"/>
                    <Picker.Item value="04" label="04" />
                    <Picker.Item value="05" label="05"></Picker.Item>
                    <Picker.Item value="06" label="06"></Picker.Item>
                    <Picker.Item value="07" label="07"></Picker.Item>
                    <Picker.Item value="08" label="08"></Picker.Item>
                    <Picker.Item value="09" label="09"></Picker.Item>
                    <Picker.Item value="10" label="10"></Picker.Item>
                    <Picker.Item value="11" label="11"></Picker.Item>
                    <Picker.Item value="12" label="12"></Picker.Item>
                    <Picker.Item value="13" label="13"></Picker.Item>
                    <Picker.Item value="14" label="14"></Picker.Item>
                    <Picker.Item value="15" label="15"></Picker.Item>
                    <Picker.Item value="16" label="16"></Picker.Item>
                    <Picker.Item value="17" label="17"></Picker.Item>
                    <Picker.Item value="18" label="18"></Picker.Item>
                    <Picker.Item value="19" label="19"></Picker.Item>
                    <Picker.Item value="20" label="20"></Picker.Item>
                    <Picker.Item value="21" label="21"></Picker.Item>
                    <Picker.Item value="22" label="22"></Picker.Item>
                    <Picker.Item value="23" label="23"></Picker.Item>
                    <Picker.Item value="24" label="24"></Picker.Item>
                    <Picker.Item value="25" label="25"></Picker.Item>
                    <Picker.Item value="26" label="26"></Picker.Item>
                    <Picker.Item value="27" label="27"></Picker.Item>
                    <Picker.Item value="28" label="28"></Picker.Item>
                    <Picker.Item value="29" label="29"></Picker.Item>
                    <Picker.Item value="30" label="30"></Picker.Item>
                    <Picker.Item value="31" label="31"></Picker.Item>
                </Picker>
              </Item>
              <Item regular style={{...styles.itemContainer, width: '30%'}}>
                <Picker
                    note
                    mode="dialog"
                    itemTextStyle={{color: 'black'}}
                    style={{ width: undefined }}
                    selectedValue={this.state.month}
                    onValueChange={(item)=>{this.setState({ month: item });}}
                >
                    {/* <Picker.Item label="Select One" value="-1" /> */}
                    {/* <Picker.Item value="-1" label="Month"/> */}
                    <Picker.Item value="01" label="Jan"></Picker.Item>
                    <Picker.Item value="22" label="Feb"></Picker.Item>
                    <Picker.Item value="03" label="Mar"/>
                    <Picker.Item value="04" label="Apr" />
                    <Picker.Item value="05" label="May"></Picker.Item>
                    <Picker.Item value="06" label="Jun"></Picker.Item>
                    <Picker.Item value="07" label="Jul"></Picker.Item>
                    <Picker.Item value="08" label="Aug"></Picker.Item>
                    <Picker.Item value="09" label="Sep"></Picker.Item>
                    <Picker.Item value="10" label="Oct"></Picker.Item>
                    <Picker.Item value="11" label="Nov"></Picker.Item>
                    <Picker.Item value="12" label="Dec"></Picker.Item>
                </Picker>
              </Item>
              <Item regular style={{...styles.itemContainer, width: '35%'}}>
                <Picker
                    note
                    mode="dialog"
                    itemTextStyle={{color: 'black'}}
                    style={{ width: undefined }}
                    selectedValue={this.state.year}
                    onValueChange={(item)=>{this.setState({ year: item });}}
                >
                    {this.state.years}
                </Picker>
              </Item>
            </View>
            <Item regular style={styles.itemContainer}>
              <Icon name="email" size={23} style={{marginRight: 5, marginLeft: 5, color: primaryTextColor}} />
              <Input keyboardType={"email-address"} placeholderTextColor={primaryTextColor} style={{color: 'black'}} placeholder={"Email"} 
              onChangeText = {(text)=>{ this.setState({email: text})}} />
            </Item>
            <Item regular style={styles.itemContainer}>
              <Icon name="email" size={23} style={{marginRight: 5, marginLeft: 5, color: primaryTextColor}} />
              <Input keyboardType={"email-address"} placeholderTextColor={primaryTextColor} style={{color: 'black'}} placeholder={"Confirm Email"} 
              onChangeText = {(text)=>{ this.setState({confirmEmail: text})}} />
            </Item>
            <Item regular style={styles.itemContainer}>
              <Icon name="lock" size={23} style={{marginRight: 5, marginLeft: 5,color: primaryTextColor}} />
              <Input placeholderTextColor={primaryTextColor} style={{color: 'black'}} secureTextEntry={!this.state.showPassword} placeholder={"Password"} 
              onChangeText = {(_text)=>{this.setState({password: _text})}} />
              <TouchableOpacity onPress={() => { this.setState({showPassword: !this.state.showPassword}) }}>
                {
                  this.state.showPassword ?
                  <Icon name="eye" type={"Entypo"} size={23} style={{marginRight: 5,color: primaryTextColor}} /> :
                  <Icon name="eye-off" type={"Entypo"} size={23} style={{marginRight: 5,color: primaryTextColor}} />
                }
              </TouchableOpacity>
            </Item>
            <Item regular style={styles.itemContainer}>
              <Icon name="lock" size={23} style={{marginRight: 5, marginLeft: 5,color: primaryTextColor}} />
              <Input placeholderTextColor={primaryTextColor} style={{color: 'black'}} secureTextEntry={!this.state.showConfirmPassword} placeholder={"Confirm Password"} 
              onChangeText = {(password)=>{this.setState({ confirmPassword: password})}} />
              <TouchableOpacity onPress={() => { this.setState({showConfirmPassword: !this.state.showConfirmPassword}) }}>
                {
                  this.state.showConfirmPassword ?
                  <Icon name="eye" type={"Entypo"} size={23} style={{marginRight: 5,color: primaryTextColor}} /> :
                  <Icon name="eye-off" type={"Entypo"} size={23} style={{marginRight: 5,color: primaryTextColor}} />
                }
              </TouchableOpacity>
            </Item>
          </Col>
          :
          <View>
            <Item regular style={styles.itemContainer}>
              <Icon name="phone" size={23} style={{marginRight: 5, marginLeft: 5, color: primaryTextColor}} />
              <Input keyboardType={"phone-pad"} placeholderTextColor={primaryTextColor} style={{color: 'black'}} placeholder={"Mobile"} 
              onChangeText = {(text)=>{ this.setState({mobile: text})}} />
            </Item>
            <Item regular style={styles.itemContainer}>
              <Icon name="home" size={23} style={{marginRight: 5, marginLeft: 5, color: primaryTextColor}} />
              <Input placeholderTextColor={primaryTextColor} style={{color: 'black'}} placeholder={"Address 1"} 
              onChangeText = {(text)=>{ this.setState({address1: text})}} />
            </Item>
            <Item regular style={styles.itemContainer}>
              <Icon name="home" size={23} style={{marginRight: 5, marginLeft: 5, color: primaryTextColor}} />
              <Input placeholderTextColor={primaryTextColor} style={{color: 'black'}} placeholder={"Address 2 (Optional)"} 
              onChangeText = {(text)=>{ this.setState({address2: text})}} />
            </Item>
            <Item regular style={styles.itemContainer}>
              <Icon name="home" size={23} style={{marginRight: 5, marginLeft: 5, color: primaryTextColor}} />
              <Input placeholderTextColor={primaryTextColor} style={{color: 'black'}} placeholder={"City"} 
              onChangeText = {(text)=>{ this.setState({city: text})}} />
            </Item>
            <Item regular style={styles.itemContainer}>
              <Icon name="home" size={23} style={{marginRight: 5, marginLeft: 5, color: primaryTextColor}} />
              <Input placeholderTextColor={primaryTextColor} style={{color: 'black'}} placeholder={"Postcode"} 
              onChangeText = {(text)=>{ this.setState({postcode: text})}} />
            </Item>
            <Item regular style={styles.itemContainer}>
              <Icon name="alpha-c" size={23} style={{marginRight: 5, marginLeft: 5,color: primaryTextColor}} />
              <Picker
                  note
                  mode="dialog"
                  itemTextStyle={{color: 'black'}}
                  style={{ width: undefined }}
                  selectedValue={this.state.country}
                  onValueChange={(item)=>{this.setState({ country: item });}}
              >
                  <Picker.Item label="Select One" value="-1" />
                  {countryItems}
                  {/* <Picker.Item value="-1" label="Select your country"/>
                  <Picker.Item value="3" label="Afghanistan"/>
                  <Picker.Item value="4" label="Albania" />
                  <Picker.Item value="5" label="Algeria"></Picker.Item>
                  <Picker.Item value="6" label="American Samoa"></Picker.Item>
                  <Picker.Item value="7" label="Andorra"></Picker.Item>
                  <Picker.Item value="8" label="Angola"></Picker.Item>
                  <Picker.Item value="9" label="Anguilla"></Picker.Item>
                  <Picker.Item value="10" label="Antarctica"></Picker.Item>
                  <Picker.Item value="11" label="Antigua and/or Barbuda"></Picker.Item>
                  <Picker.Item value="12" label="Argentina"></Picker.Item>
                  <Picker.Item value="13" label="Armenia"></Picker.Item>
                  <Picker.Item value="14" label="Aruba"></Picker.Item>
                  <Picker.Item value="15" label="Australia"></Picker.Item>
                  <Picker.Item value="16" label="Austria"></Picker.Item>
                  <Picker.Item value="17" label="Azerbaijan"></Picker.Item>
                  <Picker.Item value="18" label="Bahamas"></Picker.Item>
                  <Picker.Item value="19" label="Bahrain"></Picker.Item>
                  <Picker.Item value="20" label="Bangladesh"></Picker.Item>
                  <Picker.Item value="21" label="Barbados"></Picker.Item>
                  <Picker.Item value="22" label="Belarus"></Picker.Item>
                  <Picker.Item value="23" label="Belgium"></Picker.Item>
                  <Picker.Item value="24" label="Belize" ></Picker.Item>
                  <Picker.Item value="25" label="Benin"></Picker.Item>
                  <Picker.Item value="26" label="Bermuda"></Picker.Item>
                  <Picker.Item value="27" label="Bhutan"></Picker.Item>
                  <Picker.Item value="28" label="Bolivia"></Picker.Item>
                  <Picker.Item value="29" label="Bosnia and Herzegovina"></Picker.Item>
                  <Picker.Item value="30" label="Botswana"></Picker.Item>
                  <Picker.Item value="31" label="Bouvet Island"></Picker.Item>
                  <Picker.Item value="32" label="Brazil"></Picker.Item>
                  <Picker.Item value="33" label="British lndian Ocean Territory"></Picker.Item>
                  <Picker.Item value="34" label="Brunei Darussalam"></Picker.Item>
                  <Picker.Item value="35" label="Bulgaria"></Picker.Item>
                  <Picker.Item value="36" label="Burkina Faso"></Picker.Item>
                  <Picker.Item value="37" label="Burundi"></Picker.Item>
                  <Picker.Item value="38" label="Cambodia"></Picker.Item>
                  <Picker.Item value="39" label="Cameroon"></Picker.Item>
                  <Picker.Item value="2" label="Canada"></Picker.Item>
                  <Picker.Item value="40" label="Cape Verde"></Picker.Item>
                  <Picker.Item value="41" label="Cayman Islands"></Picker.Item>
                  <Picker.Item value="42" label="Central African Republic"></Picker.Item>
                  <Picker.Item value="43" label="Chad"></Picker.Item>
                  <Picker.Item value="44" label="Chile"></Picker.Item>
                  <Picker.Item value="45" label="China"></Picker.Item>
                  <Picker.Item value="46" label="Christmas Island"></Picker.Item>
                  <Picker.Item value="47" label="Cocos (Keeling) Islands"></Picker.Item>
                  <Picker.Item value="48" label="Colombia"></Picker.Item>
                  <Picker.Item value="49" label="Comoros"></Picker.Item>
                  <Picker.Item value="50" label="Congo"></Picker.Item>
                  <Picker.Item value="51" label="Cook Islands"></Picker.Item>
                  <Picker.Item value="52" label="Costa Rica"></Picker.Item>
                  <Picker.Item value="53" label="Croatia (Hrvatska)"></Picker.Item>
                  <Picker.Item value="54" label="Cuba"></Picker.Item>
                  <Picker.Item value="55" label="Cyprus"></Picker.Item>
                  <Picker.Item value="56" label="Czech Republic"></Picker.Item>
                  <Picker.Item value="57" label="Denmark"></Picker.Item>
                  <Picker.Item value="58" label="Djibouti"></Picker.Item>
                  <Picker.Item value="59" label="Dominica"></Picker.Item>
                  <Picker.Item value="60" label="Dominican Republic"></Picker.Item>
                  <Picker.Item value="61" label="East Timor"></Picker.Item>
                  <Picker.Item value="62" label="Ecuador"></Picker.Item>
                  <Picker.Item value="63" label="Egypt"></Picker.Item>
                  <Picker.Item value="64" label="Egypt">x</Picker.Item>
                  <Picker.Item value="65" label="Equatorial Guinea"></Picker.Item>
                  <Picker.Item value="66" label="Eritrea"></Picker.Item>
                  <Picker.Item value="67" label="Estonia"></Picker.Item>
                  <Picker.Item value="68" label="Ethiopia"></Picker.Item>
                  <Picker.Item value="69" label="Falkland Islands (Malvinas)"></Picker.Item>
                  <Picker.Item value="70" label="Faroe Islands"></Picker.Item>
                  <Picker.Item value="71" label="Fiji"></Picker.Item>
                  <Picker.Item value="72" label="Finland"></Picker.Item>
                  <Picker.Item value="73" label="France"></Picker.Item>
                  <Picker.Item value="74" label="France, Metropolitan"></Picker.Item>
                  <Picker.Item value="75" label="French Guiana"></Picker.Item>
                  <Picker.Item value="76" label="French Polynesia" />
                  <Picker.Item value="77" label="French Southern Territories"></Picker.Item>
                  <Picker.Item value="78" label="Gabon"></Picker.Item>
                  <Picker.Item value="79" label="Gambia"></Picker.Item>
                  <Picker.Item value="80" label="Georgia"></Picker.Item>
                  <Picker.Item value="81" label="Germany"></Picker.Item>
                  <Picker.Item value="82" label="Ghana"></Picker.Item>
                  <Picker.Item value="83" label="Gibraltar"></Picker.Item>
                  <Picker.Item value="84" label="Greece"></Picker.Item>
                  <Picker.Item value="85" label="Greenland"></Picker.Item>
                  <Picker.Item value="86" label="Grenada"></Picker.Item>
                  <Picker.Item value="87" label="Guadeloupe"></Picker.Item>
                  <Picker.Item value="88" label="Guam"></Picker.Item>
                  <Picker.Item value="89" label="Guatemala"></Picker.Item>
                  <Picker.Item value="90" label="Guinea"></Picker.Item>
                  <Picker.Item value="91" label="Guinea-Bissau"></Picker.Item>
                  <Picker.Item value="92" label="Guyana"></Picker.Item>
                  <Picker.Item value="93" label="Haiti"></Picker.Item>
                  <Picker.Item value="94" label="Heard and Mc Donald Islands"></Picker.Item>
                  <Picker.Item value="95" label="Honduras"></Picker.Item>
                  <Picker.Item value="96" label="Hong Kong"></Picker.Item>
                  <Picker.Item value="97" label="Hungary"></Picker.Item>
                  <Picker.Item value="98" label="Iceland"></Picker.Item>
                  <Picker.Item value="99" label="India"></Picker.Item>
                  <Picker.Item value="100" label="Indonesia"></Picker.Item>
                  <Picker.Item value="101" label="Iran (Islamic Republic of)"></Picker.Item>
                  <Picker.Item value="102" label="Iraq"></Picker.Item>
                  <Picker.Item value="103" label="Ireland"></Picker.Item>
                  <Picker.Item value="104" label="Israel"></Picker.Item>
                  <Picker.Item value="105" label="Italy"></Picker.Item>
                  <Picker.Item value="106" label="Ivory Coast"></Picker.Item>
                  <Picker.Item value="107" label="Jamaica"></Picker.Item>
                  <Picker.Item value="108" label="Japan"></Picker.Item>
                  <Picker.Item value="109" label="Jordan"></Picker.Item>
                  <Picker.Item value="110" label="Kazakhstan"></Picker.Item>
                  <Picker.Item value="111" label="Kenya"></Picker.Item>
                  <Picker.Item value="112" label="Kiribati"></Picker.Item>
                  <Picker.Item value="113" label="Korea, Democratic People's Republic of"></Picker.Item>
                  <Picker.Item value="114" label="Korea, Republic of"></Picker.Item>
                  <Picker.Item value="115" label="Kosovo"></Picker.Item>
                  <Picker.Item value="116" label="Kuwait"></Picker.Item>
                  <Picker.Item value="117" label="Kyrgyzstan"></Picker.Item>
                  <Picker.Item value="118" label="Lao People's Democratic Republic"></Picker.Item>
                  <Picker.Item value="119" label="Latvia"></Picker.Item>
                  <Picker.Item value="120" label="Lebanon"></Picker.Item>
                  <Picker.Item value="121" label="Lesotho"></Picker.Item>
                  <Picker.Item value="122" label="Liberia"></Picker.Item>
                  <Picker.Item value="123" label="Libyan Arab Jamahiriya"></Picker.Item>
                  <Picker.Item value="124" label="Liechtenstein"></Picker.Item>
                  <Picker.Item value="125" label="Lithuania"></Picker.Item>
                  <Picker.Item value="126" label="Luxembourg"></Picker.Item>
                  <Picker.Item value="127" label="Macau"></Picker.Item>
                  <Picker.Item value="128" label="Macedonia"></Picker.Item>
                  <Picker.Item value="129" label="Madagascar"></Picker.Item>
                  <Picker.Item value="130" label="Malawi"></Picker.Item>
                  <Picker.Item value="131" label="Malaysia"></Picker.Item>
                  <Picker.Item value="132" label="Maldives"></Picker.Item>
                  <Picker.Item value="133" label="Mali"></Picker.Item>
                  <Picker.Item value="134" label="Malta"></Picker.Item>
                  <Picker.Item value="135" label="Marshall Islands"></Picker.Item>
                  <Picker.Item value="136" label="Martinique"></Picker.Item>
                  <Picker.Item value="137" label="Mauritania"></Picker.Item>
                  <Picker.Item value="138" label="Mauritius"></Picker.Item>
                  <Picker.Item value="139" label="Mayotte"></Picker.Item>
                  <Picker.Item value="140" label="Mexico"></Picker.Item>
                  <Picker.Item value="141" label="Micronesia, Federated States of"></Picker.Item>
                  <Picker.Item value="142" label="Moldova, Republic of"></Picker.Item>
                  <Picker.Item value="143" label="Monaco"></Picker.Item>
                  <Picker.Item value="144" label="Mongolia"></Picker.Item>
                  <Picker.Item value="145" label="Montenegro"></Picker.Item>
                  <Picker.Item value="146" label="Montserrat"></Picker.Item>
                  <Picker.Item value="147" label="Morocco"></Picker.Item>
                  <Picker.Item value="148" label='Mozambique'></Picker.Item>
                  <Picker.Item value="149" label="Myanmar"></Picker.Item>
                  <Picker.Item value="150" label="Namibia"></Picker.Item>
                  <Picker.Item value="151" label="Nauru"></Picker.Item>
                  <Picker.Item value="152" label="Nepal"></Picker.Item>
                  <Picker.Item value="153" label="Netherlands"></Picker.Item>
                  <Picker.Item value="154" label="Netherlands Antilles"></Picker.Item>
                  <Picker.Item value="155" label="New Caledonia"></Picker.Item>
                  <Picker.Item value="156" label="New Zealand"></Picker.Item>
                  <Picker.Item value="157" label="Nicaragua"></Picker.Item>
                  <Picker.Item value="158" label="Niger"></Picker.Item>
                  <Picker.Item value="159" label="Nigeria"></Picker.Item>
                  <Picker.Item value="160" label="Niue"></Picker.Item>
                  <Picker.Item value="161" label="Norfork Island"></Picker.Item>
                  <Picker.Item value="162" label="Northern Mariana Islands"></Picker.Item>
                  <Picker.Item value="163" label="Norway"></Picker.Item>
                  <Picker.Item value="164" label="Oman"></Picker.Item>
                  <Picker.Item value="165" label="Pakistan"></Picker.Item>
                  <Picker.Item value="166" label="Palau"></Picker.Item>
                  <Picker.Item value="167" label="Panama"></Picker.Item>
                  <Picker.Item value="168" label="Papua New Guinea"></Picker.Item>
                  <Picker.Item value="169" label="Paraguay"></Picker.Item>
                  <Picker.Item value="170" label="Peru"></Picker.Item>
                  <Picker.Item value="171" label="Philippines"></Picker.Item>
                  <Picker.Item value="172" label="Pitcairn"></Picker.Item>
                  <Picker.Item value="173" label="Poland"></Picker.Item>
                  <Picker.Item value="174" label="Portugal"></Picker.Item>
                  <Picker.Item value="175" label="Puerto Rico"></Picker.Item>
                  <Picker.Item value="176" label="Qatar"></Picker.Item>
                  <Picker.Item value="177" label="Reunion"></Picker.Item>
                  <Picker.Item value="178" label="Romania"></Picker.Item>
                  <Picker.Item value="179" label="Russian Federation"></Picker.Item>
                  <Picker.Item value="180" label="Rwanda"></Picker.Item>
                  <Picker.Item value="181" label="Saint Kitts and Nevis"></Picker.Item>
                  <Picker.Item value="182" label="Saint Lucia"></Picker.Item>
                  <Picker.Item value="183" label="Saint Vincent and the Grenadines"></Picker.Item>
                  <Picker.Item value="184" label="Samoa"></Picker.Item>
                  <Picker.Item value="185" label="San Marino"></Picker.Item>
                  <Picker.Item value="186" label="Sao Tome and Principe"></Picker.Item>
                  <Picker.Item value="187" label="Saudi Arabia"></Picker.Item>
                  <Picker.Item value="188" label="Senegal"></Picker.Item>
                  <Picker.Item value="189" label="Serbia"></Picker.Item>
                  <Picker.Item value="190" label="Seychelles"></Picker.Item>
                  <Picker.Item value="191" label="Sierra Leone"></Picker.Item>
                  <Picker.Item value="192" label="Singapore"></Picker.Item>
                  <Picker.Item value="193" label="Slovakia"></Picker.Item>
                  <Picker.Item value="194" label="Slovenia"></Picker.Item>
                  <Picker.Item value="195" label="Solomon Islands"></Picker.Item>
                  <Picker.Item value="196" label="Somalia"></Picker.Item>
                  <Picker.Item value="197" label="South Africa"></Picker.Item>
                  <Picker.Item value="198" label="South Georgia South Sandwich Islands"></Picker.Item>
                  <Picker.Item value="199" label="Spain"></Picker.Item>
                  <Picker.Item value="200" label="Sri Lanka"></Picker.Item>
                  <Picker.Item value="201" label="St. Helena"></Picker.Item>
                  <Picker.Item value="202" label="St. Pierre and Miquelon"></Picker.Item>
                  <Picker.Item value="203" label="Sudan"></Picker.Item>
                  <Picker.Item value="204" label="Suriname"></Picker.Item>
                  <Picker.Item value="205" label="Svalbarn and Jan Mayen Islands"></Picker.Item>
                  <Picker.Item value="206" label="Swaziland"></Picker.Item>
                  <Picker.Item value="207" label="Sweden"></Picker.Item>
                  <Picker.Item value="208" label="Switzerland"></Picker.Item>
                  <Picker.Item value="209" label="Syrian Arab Republic"></Picker.Item>
                  <Picker.Item value="210" label="Taiwan"></Picker.Item>
                  <Picker.Item value="211" label="Tajikistan"></Picker.Item>
                  <Picker.Item value="212" label="Tanzania, United Republic of"></Picker.Item>
                  <Picker.Item value="213" label="Thailand"></Picker.Item>
                  <Picker.Item value="214" label="Togo"></Picker.Item>
                  <Picker.Item value="215" label="Tokelau"></Picker.Item>
                  <Picker.Item value="216" label="Tonga"></Picker.Item>
                  <Picker.Item value="217" label="Trinidad and Tobago"></Picker.Item>
                  <Picker.Item value="218" label="Tunisia"></Picker.Item>
                  <Picker.Item value="219" label="Turkey"></Picker.Item>
                  <Picker.Item value="220" label="Turkmenistan"></Picker.Item>
                  <Picker.Item value="221" label="Turks and Caicos Islands"></Picker.Item>
                  <Picker.Item value="222" label="Tuvalu"></Picker.Item>
                  <Picker.Item value="223" label="Uganda"></Picker.Item>
                  <Picker.Item value="224" label="Ukraine"></Picker.Item>
                  <Picker.Item value="225" label="United Arab Emirates"></Picker.Item>
                  <Picker.Item value="226" label="United Kingdom"></Picker.Item>
                  <Picker.Item value="1" label="United States"></Picker.Item>
                  <Picker.Item value="227" label="United States minor outlying islands"></Picker.Item>
                  <Picker.Item value="228" label="Uruguay"></Picker.Item>
                  <Picker.Item value="229" label="Uzbekistan"></Picker.Item>
                  <Picker.Item value="230" label="Vanuatu"></Picker.Item>
                  <Picker.Item value="231" label="Vatican City State"></Picker.Item>
                  <Picker.Item value="232" label="Venezuela"></Picker.Item>
                  <Picker.Item value="233" label="Vietnam"></Picker.Item>
                  <Picker.Item value="234" label="Virgin Islands (British)"></Picker.Item>
                  <Picker.Item value="235" label="Virgin Islands (U.S.)"></Picker.Item>
                  <Picker.Item value="236" label="Wallis and Futuna Islands"></Picker.Item>
                  <Picker.Item value="237" label="Western Sahara"></Picker.Item>
                  <Picker.Item value="238" label="Yemen"></Picker.Item>
                  <Picker.Item value="239" label="Yugoslavia"></Picker.Item>
                  <Picker.Item value="240" label="Zaire"></Picker.Item>
                  <Picker.Item value="241" label="Zambia"></Picker.Item>
                  <Picker.Item value="242" label="Zimbabwe"></Picker.Item> */}
              </Picker>
            </Item>
          </View>
        }
        </Form>
        </Row>
        <Row size={20}>
        <View>
          {this.state.firstview == false ?
          <View>
            <ListItem style={{marginLeft: 0}}>
              <CheckBox checked={this.state.condition}
              onPress={() => { this.setState({ condition: !this.state.condition }) }}
              style={{marginRight: 20}} />
              <Body>
                <Text style={{fontWeight:'bold', fontSize: 14}}>I have a valid Passport or Driving License.</Text>
              </Body>
            </ListItem>
          </View> : null
          }
          {
            this.state.loading ?
            <View style={{backgroundColor: '#f6b93b', width: 300, borderRadius: 5, 
                padding: 7,
                justifyContent: 'center', alignContent: 'center'}}>
                <ActivityIndicator size={"large"} color={"white"} />
            </View> : 
            <Button onPress={()=>{
              if(this.state.firstview == true){
                if(this.state.firstname == null || this.state.surname == null || this.state.nickname == null ||
                  this.state.email == null || this.state.confirmEmail == null || this.state.password == null ||
                  this.state.confirmPassword == null )
                  {
                    alert("Please enter valid information in all the fields. Thank you")
                  }
                  else{
                    if(this.state.email != this.state.confirmEmail){
                      alert("Email does not match.")
                    }
                    else if(String(this.state.password).length < 8){
                      alert("Password needs to be atleast 8 characters long.")
                    }
                    else if(this.state.password != this.state.confirmPassword)
                    {
                      alert("Password does not match.")
                    }
                    else{
                      this.setState({firstview: false})
                    }
                  }
              }
              else{
                if(this.state.condition){
                  this.onClickListener();
                }
                else{
                  alert("To receive GYFTmoni you need a valid passport or driving licence.")
                }
              }
              this.setState({loader: true})
            }} style={[styles.buttonContainer, {backgroundColor: '#f6b93b', width: 300, borderRadius: 5, 
            justifyContent: 'center', alignContent: 'center'}]}>
              {
                this.state.firstview == true ?
                <Text style={{color: 'white', fontSize: 18}}>Next</Text>
                :
                <Text style={{color: 'white', fontSize: 18}}>Register</Text>
              }
            </Button>
          }
          <Button transparent onPress={()=>{this.props.navigation.navigate('Login')}} 
            style={{ width: 300, marginBottom: 40, justifyContent: 'center', alignContent: 'center'}}>
              <Text style={{color: 'black', fontSize: 18, fontWeight: 'bold'}}>Already have an account?</Text>
          </Button>
        </View>
        </Row>
        </Grid>
        </ImageBackground>
        </ScrollView>
      </Content>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff', //f45302
  },
  inputContainer: {
      borderBottomColor: '#F5FCFF',
      backgroundColor: '#FFFFFF',
      borderRadius:30,
      borderBottomWidth: 1,
      width:250,
      height:45,
      marginBottom:20,
      flexDirection: 'row',
      alignItems:'center'
  },
  inputs:{
      height:45,
      marginLeft:16,
      borderBottomColor: '#FFFFFF',
      flex:5,
  },
  inputIcon:{
    width:30,
    height:30,
    marginLeft:15,
    justifyContent: 'center'
  },
  buttonContainer: {
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:20,
    width:250,
    borderRadius:30,
  },
  signupButton: {
    backgroundColor: "#f6b93b",
  },
  signUpText: {
    color: 'white',
  },
  itemContainer: {
    backgroundColor: 'white', 
    borderColor: 'black',
    borderWidth: 5,
    marginBottom: 20,  
    width: 300, 
    borderRadius: 5 
  }
});


export const mapStateToProps = (state) =>{
  return{
    regAttempt: state.registerAttempt,
    regComplete: state.regComplete,
    regResponse: state.regResponse,
    countriesResponse: state.countriesResponse
  }
}

export const mapDispatchToProps = (dispatch) => {
  return {
    regUser: (data) =>{
      dispatch(register(data))
    },
    getCountries: () => {
      dispatch(getAllCountries())
    },
    clearRegistrationObject: () => {
      dispatch(clearRegistration())
    }
  }

}

export default connect(mapStateToProps, mapDispatchToProps)(Register);
 