import React, {Component} from 'react';
import { 
  ActivityIndicator, StyleSheet, Text, View, ScrollView,
  Dimensions, Image, Platform
} from 'react-native';
import { 
    Container, Content, Header, Button, Card, CardItem, 
    Textarea, Input, Form, Item, Picker 
} from 'native-base';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Grid, Row, Col } from 'react-native-easy-grid';
import Dialog, { DialogContent } from 'react-native-popup-dialog';

// Scaling Button
import ScalingButton from './util/ScalingButton';
import { createPot, getAccountInfo, submitBankDetails } from '../redux/actions';
import ImagePicker from 'react-native-image-crop-picker';

const options = {
    title: 'Select Avatar',
    customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };

class SubmitBankInfo extends Component {

    state = {
        img : '',
        visible: true,
        liked: false,
        loader: false,
        selected: '',

        firstname: '',
        lastname: '',
        account: '',
        iban: '',
        sortcode: '',
        address1: '',
        address2: '',
        city: '',
        region: '',
        postcode: ''
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.bankResponse != '')
        {
            this.setState({loader: false})
            if(nextProps.bankResponse.status == true)
            {
                this.props.navigation.pop();
            }
            else{
                alert(nextProps.bankResponse.message);
            }
        }
    }

    _renderHeader(){
        return(
            <Header style={{marginLeft: -2, width: '102%', backgroundColor: 'transparent' }}>
            <Grid>
                <Row>
                <Col size={15} style={{justifyContent: 'center', alignItems: 'center'}}>
                    <Button transparent onPress={()=>{
                        this.props.navigation.goBack()
                    }}>
                    <Icon name="chevron-left" size={30} color={'#FCC438'} />
                    </Button>
                </Col>
                <Col size={65} style={{justifyContent: 'center', alignItems: 'center'}}>
                    <Text style={{fontSize: 20}}>Bank Info</Text>
                </Col>
                <Col size={15} style={{justifyContent: 'center', alignItems: 'center'}}>
                </Col>
                </Row>
            </Grid>
            </Header>
        )
    }

    _renderCreationView(){
        return (
            <View style={{flex: 1, backgroundColor: 'transparent'}}>
                <ScrollView style={{padding: 15, }}>
                <Form>
                    <View style={{borderBottomColor: '#ccc', borderBottomWidth: 1, paddingBottom: 6,
                    marginBottom: 15 }}>
                        <Text style={{fontSize: 16, fontStyle: 'italic',  color: 'black'}}>Account Information</Text>
                    </View>
                    <View style={{flexDirection: 'row', marginBottom: 10}}>
                        <Item regular style={{backgroundColor: 'transparent', borderColor: 'black', borderRadius: 7,  
                        flex: 0.5, paddingLeft: 5, marginRight: 5,}}>
                            <Icon name="account" size={23} style={{marginRight: 5, color: '#f6b93b'}} />
                            <Input placeholderTextColor={'#ddd'} style={{color: 'black'}} placeholder={"First Name"} 
                            onChangeText = {(text)=>{this.setState({firstname: text})}} />
                        </Item>
                        <Item regular style={{backgroundColor: 'transparent', borderColor: 'black', borderRadius: 7,  
                        flex: 0.5, paddingLeft: 5}}>
                            <Icon name="account" size={23} style={{marginRight: 5, color: '#f6b93b'}} />
                            <Input placeholderTextColor={'#ddd'} style={{color: 'black'}} placeholder={"Last Name"} 
                            onChangeText = {(text)=>{this.setState({lastname: text})}} />
                        </Item>
                    </View>
                    
                    <Item regular style={{backgroundColor: 'transparent', borderColor: 'black', borderRadius: 7,
                    marginBottom: 10,  width: Dimensions.get('screen').width - 30, paddingLeft: 10 }}>
                        <Icon name="message-text" size={23} style={{marginRight: 5,color: '#f6b93b'}} />
                        <Input placeholderTextColor={'#ddd'} style={{color: 'black'}} placeholder={"Account Number"} 
                            onChangeText = {(text)=>{this.setState({account: text})}} />
                    </Item>
                    <Item regular style={{backgroundColor: 'transparent', borderColor: 'black', borderRadius: 7,
                    marginBottom: 10,  width: Dimensions.get('screen').width - 30, paddingLeft: 10 }}>
                        <Icon name="message-text" size={23} style={{marginRight: 5,color: '#f6b93b'}} />
                        <Input placeholderTextColor={'#ddd'} style={{color: 'black'}} placeholder={"IBAN"} 
                            onChangeText = {(text)=>{this.setState({iban: text})}} />
                    </Item>
                    <Item regular style={{backgroundColor: 'transparent', borderColor: 'black', borderRadius: 7,
                    marginBottom: 10,  width: Dimensions.get('screen').width - 30, paddingLeft: 10 }}>
                        <Icon name="message-text" size={23} style={{marginRight: 5,color: '#f6b93b'}} />
                        <Input placeholderTextColor={'#ddd'} style={{color: 'black'}} placeholder={"Sort Code"} 
                            onChangeText = {(text)=>{this.setState({sortcode: text})}} />
                    </Item>
                    
                    <View style={{marginTop: 10, borderBottomColor: '#ccc', borderBottomWidth: 1, paddingBottom: 6,
                    marginBottom: 15 }}>
                        <Text style={{fontSize: 16, fontStyle: 'italic',  color: 'black'}}>Address Information</Text>
                    </View>
                    
                    <Item regular style={{backgroundColor: 'transparent', borderColor: 'black', borderRadius: 7,
                    marginBottom: 10,  width: Dimensions.get('screen').width - 30, paddingLeft: 10 }}>
                        <Icon name="message-text" size={23} style={{marginRight: 5,color: '#f6b93b'}} />
                        <Input placeholderTextColor={'#ddd'} style={{color: 'black'}} placeholder={"Address 1"} 
                            onChangeText = {(text)=>{this.setState({address1: text})}} />
                    </Item>
                    <Item regular style={{backgroundColor: 'transparent', borderColor: 'black', borderRadius: 7,
                    marginBottom: 10,  width: Dimensions.get('screen').width - 30, paddingLeft: 10 }}>
                        <Icon name="message-text" size={23} style={{marginRight: 5,color: '#f6b93b'}} />
                        <Input placeholderTextColor={'#ddd'} style={{color: 'black'}} placeholder={"Address 2"} 
                            onChangeText = {(text)=>{this.setState({address2: text})}} />
                    </Item>
                    <Item regular style={{backgroundColor: 'transparent', borderColor: 'black', borderRadius: 7,
                    marginBottom: 10,  width: Dimensions.get('screen').width - 30, paddingLeft: 10 }}>
                        <Icon name="message-text" size={23} style={{marginRight: 5,color: '#f6b93b'}} />
                        <Input placeholderTextColor={'#ddd'} style={{color: 'black'}} placeholder={"City"} 
                            onChangeText = {(text)=>{this.setState({city: text})}} />
                    </Item>
                    <Item regular style={{backgroundColor: 'transparent', borderColor: 'black', borderRadius: 7,
                    marginBottom: 10,  width: Dimensions.get('screen').width - 30, paddingLeft: 10 }}>
                        <Icon name="message-text" size={23} style={{marginRight: 5,color: '#f6b93b'}} />
                        <Input placeholderTextColor={'#ddd'} style={{color: 'black'}} placeholder={"Region"} 
                            onChangeText = {(text)=>{this.setState({region: text})}} />
                    </Item>
                    <Item regular style={{backgroundColor: 'transparent', borderColor: 'black', borderRadius: 7,
                    marginBottom: 10,  width: Dimensions.get('screen').width - 30, paddingLeft: 10 }}>
                        <Icon name="message-text" size={23} style={{marginRight: 5,color: '#f6b93b'}} />
                        <Input placeholderTextColor={'#ddd'} style={{color: 'black'}} placeholder={"Postal Code"} 
                            onChangeText = {(text)=>{this.setState({postcode: text})}} />
                    </Item>
                    <Item regular style={{backgroundColor: 'transparent', borderColor: 'black', borderRadius: 7,
                    marginBottom: 10,  width: Dimensions.get('screen').width - 30, paddingLeft: 10 }}>
                        <Icon name="lock" size={23} style={{marginRight: 5,color: '#f6b93b'}} />
                        <Picker
                        note
                        mode="dropdown"
                        itemTextStyle={{color: 'black'}}
                        style={{ width: undefined }}
                        iosIcon={<Icon name="ios-arrow-down" size={30} />}
                        selectedValue={this.state.country}
                        onValueChange={(item)=>{this.setState({country: item})}}
                        >
                            <Picker.Item value="" label="Select your country"/>
                            <Picker.Item value="AD" label="Andorra" />
                            <Picker.Item value="AF" label="Afghanistan" />
                            <Picker.Item value="AG" label="Antigua and Barbuda" />
                            <Picker.Item value="AI" label="Anguilla" />
                            <Picker.Item value="AL" label="Albania" />
                            <Picker.Item value="DZ" label="Algeria" />
                            <Picker.Item value="AM" label="Armenia" />
                            <Picker.Item value="AO" label="Angola" />
                            <Picker.Item value="AQ" label="Antarctica" />
                            <Picker.Item value="AR" label="Argentina" />
                            <Picker.Item value="AS" label="American Samoa" />
                            <Picker.Item value="AT" label="Austria" />
                            <Picker.Item value="AU" label="Australia" />
                            <Picker.Item value="AW" label="Aruba" />
                            <Picker.Item value="AX" label="Åland" />
                            <Picker.Item value="AZ" label="Azerbaijan" />
                            <Picker.Item value="BA" label="Bosnia And Herzegovina" />
                            <Picker.Item value="BB" label="Barbados" />
                            <Picker.Item value="BD" label="Bangladesh" />
                            <Picker.Item value="BE" label="Belgium" />
                            <Picker.Item value="BF" label="Burkina Faso" />
                            <Picker.Item value="BG" label="Bulgaria" />
                            <Picker.Item value="BH" label="Bahrain" />
                            <Picker.Item value="BI" label="Burundi" />
                            <Picker.Item value="BJ" label="Benin" />
                            <Picker.Item value="BM" label="Bermuda" />
                            <Picker.Item value="BN" label="Brunei Darussalam" />
                            <Picker.Item value="BO" label="Bolivia" />
                            <Picker.Item value="BQ" label="Bonaire" />
                            <Picker.Item value="BR" label="Brazil" />
                            <Picker.Item value="IO" label="British Indian Ocean Territory" />
                            <Picker.Item value="BS" label="Bahamas" />
                            <Picker.Item value="BT" label="Bhutan" />
                            <Picker.Item value="BV" label="Bouvet Island" />
                            <Picker.Item value="BW" label="Botswana" />
                            <Picker.Item value="BY" label="Belarus" />
                            <Picker.Item value="BZ" label="Belize" />
                            <Picker.item value="KH" label="Cambodia" />
                            <Picker.Item value="CM" label="Cameroon" />
                            <Picker.Item value="CA" label="Canada" />
                            <Picker.Item value="CV" label="Cape Verde" />
                            <Picker.Item value="KY" label="Cayman Islands" />
                            <Picker.Item value="CF" label="Central African Republic" />
                            <Picker.Item value="TD" label="Chad" />
                            <Picker.Item value="CL" label="Chile" />
                            <Picker.Item value="CN" label="China" />
                            <Picker.Item value="CX" label="Christmas Island" />
                            <Picker.Item value="CC" label="Cocos (Keeling) Islands" />
                            <Picker.Item value="CO" label="Colombia" />
                            <Picker.Item value="KM" label="Comoros" />
                            <Picker.Item value="CG" label="Congo (Republic)" />
                            <Picker.Item value="CD" label="Congo (Democratic Republic)" />
                            <Picker.Item value="CK" label="Cook Islands" />
                            <Picker.Item value="CI" label="Cote D’Ivoire" />
                            <Picker.Item value="CR" label="Costa Rica" />
                            <Picker.Item value="HR" label="Croatia" />
                            <Picker.Item value="CU" label="Cuba" />
                            <Picker.Item value="CW" label="Curaçao" />
                            <Picker.Item value="CY" label="Cyprus" />
                            <Picker.Item value="CZ" label="Czech Republic" />
                            
                            <Picker.Item value="DJ" label="Djibouti" />
                            <Picker.Item value="DK" label="Denmark" />
                            <Picker.Item value="DM" label="Denmark" />
                            <Picker.Item value="DO" label="Dominican Republic" />
                            <Picker.Item value="TL" label="East Timor" />
                            <Picker.Item value="EC" label="Ecuador" />
                            <Picker.Item value="EG" label="Egypt" />
                            <Picker.Item value="GQ" label="Equatorial Guinea" />
                            <Picker.Item value="EE" label="Estonia" />
                            <Picker.Item value="SV" label="El Salvador" />
                            <Picker.Item value="ER" label="Eritrea" />
                            <Picker.Item value="ET" label="Ethiopia" />
                            <Picker.Item value="FR" label="France" />
                            <Picker.Item value="GF" label="French Guiana" />
                            <Picker.Item value="PF" label="French Polynesia" />
                            <Picker.Item value="TF" label="French Southern Territories" />
                            <Picker.Item value="FI" label="Finland" />
                            <Picker.Item value="FJ" label="Fiji" />
                            <Picker.Item value="FK" label="Falkland Islands (Malvinas)" />
                            <Picker.Item value="FO" label="Faroe Islands" />
                            
                            <Picker.Item value="GA" label="Gabon" />
                            <Picker.Item value="GM" label="Gambia" />
                            <Picker.Item value="GH" label="Ghana" />
                            <Picker.Item value="DE" label="Germany" />
                            <Picker.Item value="GE" label="Georgia" />
                            
                            <Picker.Item value="GD" label="Grenada" />
                            <Picker.Item value="GG" label="Guernsey" />
                            
                            <Picker.Item value="GI" label="Gibraltar" />
                            <Picker.Item value="GL" label="Greenland" />
                            <Picker.Item value="GR" label="Greece" />
                            <Picker.Item value="GN" label="Guinea" />
                            <Picker.Item value="GP" label="Guadeloupe" />
                            <Picker.Item value="GT" label="Guatemala" />
                            <Picker.Item value="GU" label="Guam" />
                            <Picker.Item value="GW" label="Guinea-Bissau" />
                            <Picker.Item value="GY" label="Guyana" />
                            <Picker.Item value="HT" label="Haiti" />
                            <Picker.Item value="HM" label="Heard And Mc Donald Islands" />
                            <Picker.Item value="HN" label="Honduras" />
                            <Picker.Item value="HK" label="Hong Kong" />
                            <Picker.Item value="HU" label="Hungary" />
                            <Picker.Item value="ID" label="Indonesia" />
                            <Picker.Item value="IE" label="Ireland" />
                            <Picker.Item value="IL" label="Israel" />
                            <Picker.Item value="IM" label="Isle of Man" />
                            <Picker.Item value="IN" label="India" />
                            
                            <Picker.Item value="IQ" label="Iraq" />
                            <Picker.Item value="IR" label="Iran " />
                            <Picker.Item value="IS" label="Iceland" />
                            <Picker.Item value="IT" label="Italy" />
                            <Picker.Item value="JE" label="Jersey" />
                            <Picker.Item value="JM" label="Jamaica" />
                            <Picker.Item value="JO" label="Jordan" />
                            <Picker.Item value="JP" label="Japan" />
                            <Picker.Item value="KE" label="Kenya" />
                            <Picker.Item value="KG" label="Kyrgyzstan" />
                            <Picker.Item value="KI" label="Kiribati" />
                            <Picker.Item value="KR" label="Republic Of Korea" />
                            <Picker.Item value="KW" label="Kuwait" />
                            <Picker.Item value="KZ" label="Kazakhstan" />
                            <Picker.Item value="LA" label="Lao People’s Democratic Republic" />
                            <Picker.Item value="LB" label="Lebanon" />
                            <Picker.Item value="LI" label="Liechtenstein" />
                            <Picker.Item value="LR" label="Liberia" />
                            <Picker.Item value="LS" label="Lesotho" />
                            <Picker.Item value="LT" label="Lithuania" />
                            <Picker.Item value="LU" label="Luxembourg" />
                            <Picker.Item value="LV" label="Latvia" />
                            <Picker.Item value="LY" label="Libyan Arab Jamahiriya" />
                            <Picker.Item value="MA" label="Morocco" />
                            <Picker.Item value="MC" label="Monaco" />
                            <Picker.Item value="MD" label="Moldova, Republic Of" />
                            <Picker.Item value="ME" label="Montenegro" />
                            <Picker.Item value="MG" label="Madagascar" />
                            <Picker.Item value="MH" label="Marshall Islands" />
                            <Picker.Item value="ML" label="Mali" />
                            <Picker.Item value="MN" label="Mongolia" />
                            <Picker.Item value="MO" label="Macau" />
                            <Picker.Item value="MQ" label="Martinique" />
                            <Picker.Item value="MR" label="Mauritania" />
                            <Picker.Item value="MT" label="Malta" />
                            <Picker.Item value="MU" label="Mauritius" />
                            <Picker.Item value="MV" label="Maldives" />
                            <Picker.Item value="MW" label="Malawi" />
                            <Picker.Item value="YT" label="Mayotte" />
                            <Picker.Item value="MY" label="Malaysia" />
                            <Picker.Item value="FM" label="Micronesia, Federated States Of" />
                            <Picker.Item value="MX" label="Mexico" />
                            <Picker.Item value="MS" label="Montserrat" />
                            <Picker.Item value="MZ" label="Mozambique" />
                            <Picker.Item value="MM" label="Myanmar" />
                            <Picker.Item value="NA" label="Namibia" />
                            <Picker.Item value="NR" label="Nauru" />
                            <Picker.Item value="NP" label="Nepal" />
                            <Picker.Item value="NL" label="Netherlands" />
                            <Picker.Item value="AN" label="Netherlands Antilles" />
                            <Picker.Item value="NC" label="New Caledonia" />
                            <Picker.Item value="NZ" label="New Zealand" />
                            <Picker.Item value="NE" label="Niger" />
                            <Picker.Item value="NU" label="Niue" />
                            <Picker.Item value="NG" label="Nigeria" />
                            <Picker.Item value="NI" label="Nicaragua" />
                            <Picker.Item value="NF" label="Norfolk Island" />
                            <Picker.Item value="MK" label="North Macedonia" />
                            <Picker.Item value="MP" label="Northern Mariana Islands" />
                            <Picker.Item value="NO" label="Norway" />
                            <Picker.Item value="OM" label="Oman" />
                            <Picker.Item value="PK" label="Pakistan" />
                            <Picker.Item value="PW" label="Palau" />
                            <Picker.Item value="PY" label="Paraguay" />
                            <Picker.Item value="PA" label="Panama" />
                            <Picker.Item value="PG" label="Papua New Guinea" />
                            <Picker.Item value="PS" label="Palestine" />
                            <Picker.Item value="PE" label="Peru" />
                            <Picker.Item value="PH" label="Philippines, Republic of the" />
                            <Picker.Item value="PN" label="Pitcairn" />
                            <Picker.Item value="PL" label="Poland" />
                            <Picker.Item value="PT" label="Portugal" />
                            <Picker.Item value="PR" label="Puerto Rico" />
                            <Picker.Item value="QA" label="Qatar" />
                            <Picker.Item value="RE" label="Reunion" />
                            <Picker.Item value="RO" label="Romania" />
                            <Picker.Item value="RU" label="Russian Federation" />
                            <Picker.Item value="RW" label="Rwanda" />
                            <Picker.Item value="BL" label="Saint Barthélemy" />
                            <Picker.Item value="KN" label="Saint Kitts And Nevis" />
                            <Picker.Item value="LC" label="Saint Lucia" />
                            <Picker.Item value="SM" label="San Marino" />
                            <Picker.Item value="WS" label="Samoa" />
                            <Picker.Item value="ST" label="Sao Tome And Principe" />
                            <Picker.Item value="SA" label="Saudi Arabia" />
                            <Picker.Item value="SN" label="Senegal" />
                            <Picker.Item value="RS" label="Serbia" />
                            <Picker.Item value="SC" label="Seychelles" />
                            <Picker.Item value="SL" label="Sierra Leone" />
                            <Picker.Item value="SG" label="Singapore" />
                            <Picker.Item value="SX" label="Sint Maarten" />
                            <Picker.Item value="SI" label="Slovenia" />
                            <Picker.Item value="SK" label="Slovakia" />
                            <Picker.Item value="SB" label="Solomon Islands" />
                            <Picker.Item value="SO" label="Somalia" />
                            <Picker.Item value="ZA" label="South Africa" />
                            <Picker.Item value="GS" label="South Georgia" />
                            <Picker.Item value="SS" label="South Sudan" />
                            <Picker.Item value="ES" label="Spain" />
                            <Picker.Item value="LK" label="Sri Lanka" />
                            <Picker.Item value="SH" label="St. Helena" />
                            <Picker.Item value="PM" label="St. Pierre And Miquelon" />
                            <Picker.Item value="SD" label="Sudan" />
                            <Picker.Item value="SR" label="Suriname" />
                            <Picker.Item value="SJ" label="Svalbard And Jan Mayen Islands" />
                            <Picker.Item value="SE" label="Sweden" />
                            <Picker.Item value="SZ" label="Swaziland" />
                            <Picker.Item value="CH" label="Switzerland" />
                            <Picker.Item value="SY" label="Syrian Arab Republic" />
                            <Picker.Item value="TC" label="Turks And Caicos Islands" />
                            <Picker.Item value="TH" label="Thailand" />
                            <Picker.Item value="TJ" label="Tajikistan" />
                            <Picker.Item value="TK" label="Tokelau" />
                            <Picker.Item value="TM" label="Turkmenistan" />
                            <Picker.Item value="TN" label="Tunisia" />
                            <Picker.Item value="TO" label="Tonga" />
                            <Picker.Item value="TR" label="Turkey" />
                            <Picker.Item value="TT" label="Trinidad And Tobago" />
                            <Picker.Item value="TV" label="Tuvalu" />
                            <Picker.Item value="TW" label="Taiwan" />
                            <Picker.Item value="TZ" label="Tanzania, United Republic Of" />
                            <Picker.Item value="UA" label="Ukraine" />
                            <Picker.Item value="UG" label="Uganda" />
                            <Picker.Item value="AE" label="United Arab Emirates" />
                            <Picker.Item value="GB" label="United Kingdom" />
                            <Picker.Item value="UM" label="United States Minor Outlying Islands" />
                            <Picker.Item value="US" label="United States" />
                            <Picker.Item value="UY" label="Uruguay" />
                            <Picker.Item value="UZ" label="Uzbekistan" />
                            <Picker.Item value="VA" label="Vatican City State" />
                            <Picker.Item value="VE" label="Venezuela" />
                            <Picker.Item value="VG" label="Virgin Islands (British)" />
                            <Picker.Item value="VI" label="Virgin Islands (U.S.)" />
                            <Picker.Item value="VN" label="Vietnam" />
                            <Picker.Item value="VU" label="Vanuatu" />
                            <Picker.Item value="WF" label="Wallis And Futuna Islands" />
                            <Picker.Item value="EH" label="Western Sahara" />
                            <Picker.Item value="YE" label="Yemen" />
                            <Picker.Item value="ZM" label="Zambia" />
                            <Picker.Item value="ZW" label="Zimbabwe" />
                        </Picker>
                    </Item>
                </Form>
                <View style={{alignItems: 'center', justifyContent:'center'}}>
                    {
                        this.state.loader == false ?
                        <ScalingButton label="Give" 
                            onPress={this._handlePress.bind(this)}
                            styles={{button: styles.animated_button, label: styles.button_label}}
                        >
                            <Text style={{color: 'white', fontSize: 20}}>Submit</Text>
                        </ScalingButton>
                        :
                        <View styles={{button: styles.animated_button, label: styles.button_label}} >
                            <ActivityIndicator color={"white"} size={"large"} />
                        </View>
                    }
                </View>
                </ScrollView>
            </View>
        )
    }

    _handlePress = () => {
        // alert("welcome");
        if(this.state.firstname == '') {
            alert("Please provide a `.")
        } 
        else if(this.state.lastname == '') {
            alert("Please provide a valid lastname")
        } 
        else if(this.state.account == '') {
            alert("Please provide a valid account number.")
        }
        else if(this.state.iban == '') {
            alert("Please provide a valid iban.")
        }
        else if(this.state.sortcode == '') {
            alert("Please provide a valid sort code.")
        }
        else if(this.state.address1 == '') {
            alert("Please provide a valid Address 1")
        } 
        else if(this.state.address2 == '') {
            alert("Please provide a valid Address 2.")
        }
        else if(this.state.city == '') {
            alert("Please provide a valid city name.")
        }
        else if(this.state.region == '') {
            alert("Please provide a valid region.")
        }
        else if(this.state.postcode == '') {
            alert("Please provide a valid Postal code.")
        }
        else {

            var obj = {
                "firstname": this.state.firstname,
                "lastname": this.state.lastname,
                "account": this.state.account,
                "iban": this.state.iban,
                "sortcode": this.state.sortcode,
                "address1": this.state.address1,
                "address2": this.state.address2,
                "city": this.state.city,
                "region": this.state.region,
                "postcode": this.state.postcode,
                "country": this.state.country,
                "user_id": this.props.loggedUser.id
            };

            this.setState({loader: true});

            this.props.submitDetails(obj);
        }
    }

    _renderLoader = () =>{
        return(
            <View>
                <Dialog
                visible={this.state.loader}
                dialogStyle={{width: 200, padding: 20}}
                >
                <DialogContent>
                    <ActivityIndicator size={"large"} color={"golden"} />
                    <Text style={{fontSize: 20}}>Please wait...</Text>
                </DialogContent>
                </Dialog>
            </View>
        )
    }

    render() {
        return (
            <Container>
                <Content contentContainerStyle={{flex: 1}}>
                    <View style={{width: Dimensions.get('window').width, height: 60, backgroundColor: '#f6b93b', 
                    justifyContent:'center', alignItems: 'flex-end', flexDirection:'row', 
                    paddingBottom: 15, marginTop: Platform.OS == "ios" ? 35 : 0}}>
                        <Image source={require('../assets/img/logo.png')} style={{width: 32, height: 32, paddingBottom: 3, 
                            marginRight: 4, tintColor: '#fff'}} />
                        <Image source={require('../assets/img/complete_logo.png')} style={{width: 144, height: 20, tintColor: '#fff'}} />
                    </View>
                    {this._renderHeader()}
                    {this._renderLoader()}
                    {this._renderCreationView()}
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    bottom:{
        flex: 1, backgroundColor: 'white', 
        paddingTop: 25,
        paddingBottom: 20,
        paddingLeft: 30,
        paddingRight: 30, 
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30
    },
    title:{
        fontSize: 26,
        fontWeight: 'bold',
        color: 'black'
    },
    animated_button : {
        backgroundColor: '#f45302',
        borderRadius: 30,
        width: 350
    },
    button_label: {
        color: '#fff',
        fontSize: 18
    },
    uploadAvatar : {
        width: Dimensions.get('window').width,
        height: 150
    }
});

const mapStateToProps = (state) => {
    return {
        bankResponse: state.bankResponse,
        loggedUser: state.loggedUser,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        submitDetails: (x) => {
            dispatch(submitBankDetails(x))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SubmitBankInfo);
