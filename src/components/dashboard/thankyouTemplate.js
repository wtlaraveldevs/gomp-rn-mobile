import React, { Component } from 'react';
import { Container, Header, Content, Label, Item, Button, Text, Textarea } from 'native-base';
import { Grid, Col, Row } from 'react-native-easy-grid'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';
import { updateThankyouTemplate } from '../../redux/actions';
import { Dimensions, StyleSheet,  View, Image, Platform, ScrollView, Alert } from 'react-native';
import ScalingButton from '../util/ScalingButton';
import { config } from '../../config';

class ThankyouTemplate extends Component {

    constructor(props){
        super(props);
        this.state={
            message: this.props.loggedUser.thankyou_template
        }
    }

    _renderHeader(){
        return(
            <View style={{height: 60}}>
            <Grid style={{alignItems: 'center', paddingHorizontal: 10}}>
                <Row>
                <Col size={15} style={{justifyContent: 'center', alignItems: 'center'}}>
                    <Button transparent onPress={()=>{
                        this.props.navigation.goBack()
                    }}>
                    <Icon name="chevron-left" size={30} color={'#FCC438'} />
                    </Button>
                </Col>
                <Col size={65} style={{justifyContent: 'center', alignItems: 'center'}}>
                    <Text style={{fontSize: 20}}>Thankyou Message</Text>
                </Col>
                <Col size={15} style={{justifyContent: 'center', alignItems: 'center'}}>
                </Col>
                </Row>
            </Grid>
            </View>
        )
    }

    updateTemplate(){
        try{
            
            this.setState({ loader: true })

            var data = {
                "user_id": this.props.loggedUser.id,
                "template": this.state.message
            }

            fetch(`${config.api.updateTemplate}`, {
                method: "POST",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(data)
            })
            .then((response) => { 
                return response.json() 
            })
            .then((response) => {
                console.log("Response", response);
                this.setState({ loader: false })
                if(response.status)
                {
                    this.props.updatethankyou(this.state.message);
                    setTimeout(() => {
                        Alert.alert(
                            "Congratulations",
                            "Thankyou Message has been updated successfully.",
                            [
                                { text: "OK", onPress: () => {
                                    this.props.navigation.pop()
                                } }
                            ],
                            { cancelable: false }
                        );
                    }, 100);
                }
                else {
                    Alert.alert(
                        "Error",
                        "Unfortunately we are unable to update the template at the moment. Please try again or contact the GYFTmoni administration.",
                        [
                            { text: "OK", onPress: () => console.log("OK Pressed") }
                        ],
                        { cancelable: false }
                    );
                }
            })
            .catch(err => {
                this.setState({ loader: false })
                alert(err);
                console.log("ThankYou Error",err);
            })
        }
        catch(err)
        {
            alert(err);
        }
    }

    render() {
        return (
            <Container>
                <View style={{width: Dimensions.get('window').width, height: Platform.OS == "android" ? 60 : 85, backgroundColor: '#f6b93b', 
                justifyContent:'center', alignItems: 'flex-end', 
                flexDirection:'row', paddingBottom: 15}}>
                    {/* <Image source={require('../../assets/img/logo.png')} style={{width: 32, height: 32, paddingBottom: 3, 
                        marginRight: 4, tintColor: '#fff'}} /> */}
                    <Image source={require('../../assets/img/complete_logo.png')} style={{width: 144, height: 20, tintColor: '#fff'}} />
                </View>
                {this._renderHeader()}
                <Content>
                    <View style={{flex: 1, padding: 15, backgroundColor: 'transparent'}}>
                        <ScrollView keyboardShouldPersistTaps="always" style={{flex: 1}}>
                            <Grid style={{paddingBottom: 100}}>
                                <Col>
                                    <Text style={{marginBottom:8, fontWeight: 'bold'}}>You can send Thank you messages to your friends and loved ones. You can update the SMS text here that they receive. </Text>
                                    <Textarea
                                        style={{borderRadius: 5, borderColor: '#ddd', borderWidth: 1}}
                                        rowSpan={12}
                                        value={this.state.message}
                                        onChangeText={(text) => {
                                            this.setState({message: text})
                                        }}
                                    />
                                </Col>
                            </Grid>
                        </ScrollView>
                        <View style={{alignItems: 'center', justifyContent:'center', position: 'absolute',
                            bottom: 5, width: Dimensions.get('window').width}}>
                            <ScalingButton label="Give" 
                                onPress={this.updateTemplate.bind(this)}
                                styles={{button: styles.animated_button, label: styles.button_label}}
                            >
                                <Text style={{color: 'white', fontSize: 20}}>Update Message</Text>
                            </ScalingButton>
                        </View>
                    </View>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    bottom:{
      flex: 1, backgroundColor: 'white', 
      paddingTop: 25,
      paddingBottom: 20,
      paddingLeft: 30,
      paddingRight: 30, 
      borderTopLeftRadius: 30,
      borderTopRightRadius: 30
    },
    title:{
      fontSize: 26,
      fontWeight: 'bold',
      color: 'black'
    },
    animated_button : {
      backgroundColor: '#f6b93b',
      borderRadius: 10,
      width: 300,
    },
    button_label: {
      color: '#fff',
      fontSize: 18
    }
});

const mapStateToProps = (state) => {
    return {
        loggedUser: state.loggedUser
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updatethankyou: (text) => {
            dispatch(updateThankyouTemplate(text))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ThankyouTemplate);