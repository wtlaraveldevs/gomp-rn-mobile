import React, { Component } from 'react';
import { connect } from 'react-redux'
import { Content, Card, CardItem, Text, Label, Button } from 'native-base'
import { FlatList, Image } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { getAllGoals, setGOM } from '../../redux/actions';
import { config } from '../../config/index';
import { Row, Col } from 'react-native-easy-grid'; 

class GoalList extends Component {
    state = { 
        goals: [],
        hasData: false
     }

    componentWillMount(){
        // fetch the favorite List from the server
        this.props.fetchGoals({user_id: this.props.loggedUser.id});

    }

    componentWillReceiveProps(nextProps)
    {
        if(nextProps.allGoals != this.state.goals)
        {
            this.setState({goals: nextProps.allGoals, hasData: true})
        }
    }

    render() { 
        return ( 
            <Content padder>
                {
                    this.state.hasData == false ?
                    null :
                    <FlatList
                        data={this.state.goals}
                        key={item=>item.id}
                        extraData={this.state}
                        renderItem={(item)=>{
                            console.log(item.item);
                            let url = `${config.smallImg}/${item.item.small_image}`;
                            return(
                                <Card>
                                    <Row>
                                        <Col size={40} style={{marginRight: 7}}>
                                            <Image source={{uri: url}}
                                                style = {{height: 110,  resizeMode : 'stretch', margin: 5 }}/>
                                        </Col>
                                        <Col size={60} style={{padding: 7}}>
                                            <Row size={10}>
                                            </Row>
                                            <Row size={70}>
                                                <Label style={{fontSize: 22, color: 'black'}}>
                                                    {item.item.title}
                                                </Label>
                                            </Row>
                                            <Row size={20}>
                                                <Col size={70}>
                                                    <Text style={{fontSize: 22, fontWeight: 'bold', color: '#f6b93b'}}>£{item.item.donation}</Text>
                                                </Col>
                                                <Col size={30}>
                                                    <Button transparent onPress={()=>{
                                                        this.props.setGM(item.item);
                                                        setTimeout(()=>{
                                                            this.props.navigation.navigate('EditPot');
                                                        }, 600);
                                                    }}
                                                    style={{alignSelf: "flex-end", borderWidth: 2, borderColor: '#f6b93b', borderRadius: 20, height: 30}}>
                                                        <Text>Edit</Text>
                                                    </Button>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                </Card>
                            )
                        }}
                    />

                }
            </Content>
        );
    }
}

const mapStateToProps = (state) => {
    return{
        loggedUser: state.loggedUser,
        allGoals: state.getAllGoals
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchGoals: (user) => {
            dispatch(getAllGoals(user))
        },
        setGM: (data) =>{
            dispatch(setGOM(data))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(GoalList);