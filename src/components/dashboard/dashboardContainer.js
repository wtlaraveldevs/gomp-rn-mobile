import React, { Component } from 'react';
import { Container, Header, Content, Footer, FooterTab, Button, Text } from 'native-base';
import { Grid, Col, Row } from 'react-native-easy-grid'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import GoalList from './goalList';
import FavoriteList from './favoriteList';
import Dashboard from './dashboard';
import { View, Image, Dimensions, Platform } from 'react-native';

export default class dashboardContainer extends Component {

    state = {
        _view: 0
    }

    componentDidMount(){
        this.setState({
            _view: this.props.navigation.getParam('viewNumber')
        });
    }

    _renderHeader(){
        return(
            <View style={{height: 50}}>
            <Grid style={{alignItems: 'center', paddingHorizontal: 10}}>
                <Row>
                <Col size={15} style={{justifyContent: 'center', alignItems: 'center'}}>
                    <Button transparent onPress={()=>{
                        if(this.state.confirm){
                            this.setState({confirm : false})
                        }else{
                            this.props.navigation.goBack()
                        }
                    }}>
                    <Icon name="chevron-left" size={30} color={'#FCC438'} />
                    </Button>
                </Col>
                <Col size={65} style={{justifyContent: 'center', alignItems: 'center'}}>
                    {
                        this.state._view == 0 ?
                        <Text style={{fontSize: 20}}>Dashboard</Text>
                        : this.state._view == 1 ?
                        <Text style={{fontSize: 20}}>Edit My Pots</Text>
                        : 
                        <Text style={{fontSize: 20}}>Gifts of Money</Text>
                    }
                    
                </Col>
                <Col size={15} style={{justifyContent: 'center', alignItems: 'center'}}>
                    {/* {
                        this.state._view == 2 ?
                        <Button transparent onPress={()=>{
                            this.props.navigation.navigate('thankyouTemplate');
                        }}>
                            <Icon name="pencil-box-outline" size={30} color={'#FCC438'} />
                        </Button> : null
                    } */}
                </Col>
                </Row>
            </Grid>
            </View>
        )
    }

    render() {
        return (
            <Container>
                <View style={{width: Dimensions.get('window').width, height: Platform.OS == "android" ? 60 : 85, backgroundColor: '#f6b93b', 
                justifyContent:'center', alignItems: 'flex-end', flexDirection:'row', 
                paddingBottom: 15, }}>
                    {/* <Image source={require('../../assets/img/logo.png')} style={{width: 32, height: 32, paddingBottom: 3, 
                        marginRight: 4, tintColor: '#fff'}} /> */}
                    <Image source={require('../../assets/img/complete_logo.png')} style={{width: 144, height: 20, tintColor: '#fff'}} />
                </View>
                {this._renderHeader()}
                <Content>
                    {
                        this.state._view == 0 ?
                        <Dashboard />
                        : this.state._view == 1 ?
                        <GoalList navigation={this.props.navigation} /> 
                        : 
                        <FavoriteList navigation={this.props.navigation} />
                    }
                </Content>
                <Footer>
                    <FooterTab>
                    <Button onPress={()=>{this.setState({_view: 0})}} vertical active={this.state._view == 0 ? true : false}>
                        <Icon color={this.state._view == 0 ? "#fff" : "#000"} name="finance" size={25} />
                        <Text style={{color: this.state._view == 0 ? "#fff" : "#000"}}>Dashboard</Text>
                    </Button>
                    <Button onPress={()=>{this.setState({_view: 1})}} vertical active={this.state._view == 1 ? true : false}>
                        <Icon color={this.state._view == 1 ? "#fff" : "#000"} name="bullseye" size={25} />
                        <Text style={{color: this.state._view == 1 ? "#fff" : "#000"}}>Edit My Pots</Text>
                    </Button>
                    <Button onPress={()=>{this.setState({_view: 2})}} vertical active={this.state._view == 2 ? true : false}>
                        <Icon color={this.state._view == 2 ? "#fff" : "#000"} name="cash" size={25} />
                        <Text style={{color: this.state._view == 2 ? "#fff" : "#000"}}>Gifts of Money</Text>
                    </Button>
                    </FooterTab>
                </Footer>
            </Container>
        );
    }
}