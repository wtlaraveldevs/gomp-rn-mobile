import React, { Component } from 'react';
import { connect } from 'react-redux'
import { Content, Card, CardItem, Text, Label, Button, View } from 'native-base'
import { FlatList, Alert, ActivityIndicator, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { getAllGifts } from '../../redux/actions';
import { config } from '../../config/index';
import { Row, Col } from 'react-native-easy-grid'; 
import Dialog, { DialogContent } from 'react-native-popup-dialog';
import { primaryColor } from '../../assets/Util/colors';

class FavoriteList extends Component {
    state = { 
        gifts: [],
        hasData: false,
        loader: false
    }

    componentWillMount(){
        // fetch the favorite List from the server
        this.props.fetchGifts({user_id: this.props.loggedUser.id});
    }

    componentWillReceiveProps(nextProps)
    {
        if(nextProps.getAllGifts != this.state.gifts)
        {
            this.setState({ gifts: nextProps.getAllGifts, hasData: true})
        }
    }

    _renderLoader = () =>{
        return(
            <View>
                <Dialog
                visible={this.state.loader}
                dialogStyle={{width: 200, padding: 20}}
                >
                <DialogContent>
                    <ActivityIndicator size={"large"} color={"golden"} />
                    <Text style={{fontSize: 20}}>Please wait...</Text>
                </DialogContent>
                </Dialog>
            </View>
        )
    }

    sayThankyou(donation_id, contact){
        try{

            console.log('thankyouMessage ', this.props.loggedUser.thankyou_template)

            this.setState({ loader: true })

            // var data = {
            //     user_id: user_id // this is actually ID in DONATIONS table.
            // }

            var data = {
                "user_id": donation_id
            }

            fetch(`${config.api.thankyou}`, {
                method: "POST",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(data)
            })
            .then((response) => { 
                console.log("Response in JSON", response);
                return response.json() 
            })
            .then((response) => {
                console.log("Response", response);
                this.setState({ loader: false })
                if(response.status)
                {
                    this.updateThankyouFlag(donation_id);
                }
                else {
                    Alert.alert(
                        "",
                        "Unable to send Thank you message. Please try again or contact the GYFTmoni administration.",
                        [
                            { text: "OK", onPress: () => console.log("OK Pressed") }
                        ],
                        { cancelable: false }
                    );
                }
            })
            .catch(err => {
                this.setState({ loader: false })
                alert(err);
                console.log("ThankYou Error",err);
            })
        }
        catch(err)
        {
            alert(err);
        }
    }

    updateThankyouFlag(donation_id){
        try{
            this.setState({ loader: true })

            var data = {
                user_id: donation_id // this is actually ID in DONATIONS table.
            }

            fetch(`${config.api.updateThankyouFlag}`, {
                method: "POST",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(data)
            })
            .then((response) => {
                return response.json() 
            })
            .then((response) => {
                this.setState({ loader: false })
                if(response.status)
                {
                    Alert.alert(
                        "Congratulations",
                        "Successfully sent message to the recipient.",
                        [
                            { text: "OK", onPress: () => {
                                this.props.fetchGifts({user_id: this.props.loggedUser.id});
                            } }
                        ],
                        { cancelable: false }
                    );
                }
                else {
                    Alert.alert(
                        "Error",
                        "Unable to send Thank you message. Please try again or contact the GYFTmoni administration.",
                        [
                            { text: "OK", onPress: () => console.log("OK Pressed") }
                        ],
                        { cancelable: false }
                    );
                }
            })
            .catch(err => {
                this.setState({ loader: false })
                alert(err);
                console.log("ThankYou Error",err);
            })
        }
        catch(err)
        {
            alert(err);
        }
    }

    render() { 
        return ( 
            <Content padder>
                <View style={{marginBottom: 5}}>
                    <TouchableOpacity onPress={()=>{
                        this.props.navigation.navigate('thankyouTemplate');
                    }} style={{backgroundColor: '#f6b93b', padding: 15, borderRadius: 5,
                    justifyContent:'center', alignItems:'center' }}>
                        <Text style={{color: 'white'}}>Edit Thankyou Template</Text>
                    </TouchableOpacity>
                </View>
                {
                    this.state.hasData == false ?
                    <View style={{flex: 1, paddingTop: 30, justifyContent:'center', alignItems:'center'}}>
                        <ActivityIndicator size="large" color={primaryColor} />
                        <Text style={{marginTop:5, fontSize: 16}}>Fetching Gifts...</Text>
                    </View> 
                    :
                    <FlatList
                        data={this.state.gifts}
                        key={item=>item.id}
                        extraData={this.state}
                        renderItem={(item)=>{
                            return(
                                <Card>
                                    <Row>
                                        <Col style={{padding: 10}}>
                                            <Row size={10} style={{justifyContent:'flex-end'}}>
                                                {
                                                    item.item.thanked == 0 ?
                                                    <TouchableOpacity onPress={this.sayThankyou.bind(this, item.item.id, item.item.contact_no)}
                                                    style={{backgroundColor: '#f6b93b', borderRadius: 5, 
                                                    paddingHorizontal: 10, paddingVertical: 6}}>
                                                        <Text style={{color: 'white'}}>Say Thank You</Text>
                                                    </TouchableOpacity> :
                                                    <Text style={{color:'#f6b93b'}}>Already Thanked</Text>
                                                }
                                            </Row>
                                            <Row size={40}>
                                                <Label style={{fontSize: 22, color: '#f6b93b', fontWeight: 'bold'}}>
                                                    {item.item.title}
                                                </Label>
                                            </Row>
                                            <Row size={10}>
                                            </Row>
                                            <Row size={45}>
                                                <Col>
                                                    <View style={{flex: 1, flexDirection: 'row'}}>
                                                        <View style={{flex: 0.8}}>
                                                            <Text style={{fontSize: 18, fontWeight: 'bold', color: 'black'}}>{item.item.fullname}</Text>
                                                        </View>
                                                        <View style={{flex: 0.2}}>
                                                            <View 
                                                            style={{justifyContent:'center', alignItems: 'center',
                                                            borderWidth: 2, borderColor: '#f6b93b', borderRadius: 20, 
                                                            height: 30}}>
                                                                <Text style={{fontSize: 18, fontWeight: 'bold', color: '#f6b93b'}}>£{item.item.donation}</Text>
                                                            </View>
                                                        </View>
                                                    </View>
                                                    
                                                    {item.item.email ?<Text style={{fontSize: 16, fontWeight: 'bold', color: 'gray'}}>{item.item.email}</Text> : null}
                                                    {item.item.country ? <Text style={{fontSize: 16, fontWeight: 'bold', color: 'gray'}}>{item.item.country}</Text> : null}
                                                    {item.item.comment ? <Text style={{fontSize: 16, fontWeight: 'bold', color: 'gray'}}>{item.item.comment}</Text> : null}
                                                </Col>
                                                {/* <Col size={20} style={{justifyContent:'flex-end'}}>
                                                    <View 
                                                    style={{ justifyContent:'center', alignItems: 'center', borderWidth: 2, borderColor: '#f6b93b', borderRadius: 20, height: 30}}>
                                                        <Text style={{fontSize: 18, fontWeight: 'bold', color: '#f6b93b'}}>£{item.item.donation}</Text>
                                                    </View>
                                                    
                                                </Col> */}
                                            </Row>
                                        </Col>
                                    </Row>
                                </Card>
                            )
                        }}
                    />

                }
                {this._renderLoader()}
            </Content>
        );
    }
}

const mapStateToProps = (state) => {
    return{
        loggedUser: state.loggedUser,
        getAllGifts: state.getAllGifts,
        thankyouMessage: state.thankyouMessage
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchGifts: (user) => {
            dispatch(getAllGifts(user))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FavoriteList);