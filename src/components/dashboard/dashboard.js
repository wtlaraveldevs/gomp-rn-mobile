import React, { Component } from 'react';
import { connect } from 'react-redux'
import { Content, Card, CardItem, Text, Label, Button } from 'native-base'
import { ActivityIndicator, FlatList, View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { dashboard } from '../../redux/actions';
import { config } from '../../config/index';
import { Row, Col } from 'react-native-easy-grid'; 
import { YAxis, LineChart, Grid } from 'react-native-svg-charts'
import { primaryColor } from '../../assets/Util/colors';

class Dashboard extends Component {
    state = { 
        dashboard: [],
        hasData: false
     }

    componentWillMount(){
        // fetch the favorite List from the server
        this.props.fetchDashboard({user_id: this.props.loggedUser.id});

    }

    componentWillReceiveProps(nextProps)
    {
        if(nextProps.dashboard != this.state.dashboard)
        {

            // separate dates and values
            var totalGoalChart = nextProps.dashboard[0].totalGoalChart;
            var totalGoalKey = [];
            var totalGoalData = [];
            totalGoalChart.forEach(element => {
                totalGoalData.push(element.value);
                totalGoalKey.push(element.days);
            });

            // separate dates and values
            var totalGOMChart= nextProps.dashboard[0].totalGOMChart;
            var totalGOMKey = [];
            var totalGOMData = [];
            totalGOMChart.forEach(element => {
                totalGOMData.push(element.value);
                totalGOMKey.push(element.days);
            });

            var totalGOMRChart= nextProps.dashboard[0].totalGOMRChart;
            var totalGOMRKey = [];
            var totalGOMRData = [];
            totalGOMRChart.forEach(element => {
                totalGOMRData.push(element.value);
                totalGOMRKey.push(element.days);
            });

            console.log('GOM', totalGOMRData);

            this.setState({dashboard: nextProps.dashboard[0], hasData: true, totalGOMRKey: totalGOMRKey, totalGOMRData: totalGOMRData,
                totalGOMData: totalGOMData, totalGOMKey: totalGOMKey,
                totalGoalData: totalGoalData, totalGoalKey: totalGoalKey
            })
        }
    }

    render() { 
        const stroke = 'rgb(0, 0, 0)';
        return ( 
            <Content padder>
                {
                    this.state.hasData == false ?
                    <View style={{flex: 1, paddingTop: 30, justifyContent:'center', alignItems:'center'}}>
                        <ActivityIndicator size="large" color={primaryColor} />
                        <Text style={{marginTop:5, fontSize: 16}}>Loading data ...</Text>
                    </View> 
                    :
                    <View>
                    <Card>
                        <CardItem style={{backgroundColor: '#3498db'}}>
                            <Row>
                                <Col size={75} style={{justifyContent:'center'}}><Text style={{fontSize: 18, color:'white'}} >Gifts</Text></Col>
                                <Col size={25} style={{ alignItems:'flex-end' }}><Text style={{fontSize: 20, color: 'white'}}>{this.state.dashboard.totalGOM}</Text></Col>
                            </Row>
                        </CardItem>
                        <View style={{ height: 200, flexDirection: 'row', paddingLeft: 10 }}>
                        <YAxis
                            data={ this.state.totalGOMData }
                            contentInset={{ top: 30, bottom: 30 }}
                            svg={{
                                fill: 'grey',
                                fontSize: 10,
                            }}
                            numberOfTicks={5}
                            formatLabel={ value => `${value}` }
                        />
                        <LineChart
                            style={{ flex: 1, marginLeft: 16 }}
                            data={ this.state.totalGOMData }
                            key={ this.state.totalGOMKey }
                            svg={{ stroke }}
                            numberOfTicks={5}
                            contentInset={{ top: 30, bottom: 30 }}
                        >
                            <Grid />
                        </LineChart>
                        </View>
                    </Card>
                    <Card>
                        <CardItem style={{backgroundColor: '#f1c40f'}}>
                            <Row>
                                <Col size={75} style={{justifyContent:'center'}}><Text style={{fontSize: 18, color:'white'}} >Total Gifts of Money Received</Text></Col>
                                <Col size={25} style={{ alignItems:'flex-end' }}><Text style={{fontSize: 20, color: 'white'}}>{this.state.dashboard.totalGOMR}</Text></Col>
                            </Row>
                        </CardItem>
                        <View style={{ height: 200, flexDirection: 'row', paddingLeft: 10 }}>
                        <YAxis
                            data={ this.state.totalGOMRData }
                            contentInset={{ top: 30, bottom: 30 }}
                            svg={{
                                fill: 'grey',
                                fontSize: 10,
                            }}
                            numberOfTicks={5}
                            formatLabel={ value => `${value}` }
                        />
                        <LineChart
                            style={{ flex: 1, marginLeft: 16 }}
                            data={ this.state.totalGOMRData }
                            key={ this.state.totalGOMRKey }
                            svg={{ stroke }}
                            numberOfTicks={5}
                            contentInset={{ top: 30, bottom: 30 }}
                        >
                            <Grid />
                        </LineChart>
                        </View>
                    </Card>
                    <Card>
                        <CardItem style={{backgroundColor: '#e74c3c'}}>
                            <Row>
                                <Col size={75} style={{justifyContent:'center'}}><Text style={{fontSize: 18, color:'white'}} >GYFTmoni Pots</Text></Col>
                                <Col size={25} style={{ alignItems:'flex-end' }}><Text style={{fontSize: 20, color: 'white'}}>{this.state.dashboard.totalGoals}</Text></Col>
                            </Row>
                        </CardItem>
                        <View style={{ height: 200, flexDirection: 'row', paddingLeft: 10 }}>
                        <YAxis
                            data={ this.state.totalGoalData }
                            contentInset={{ top: 30, bottom: 30 }}
                            svg={{
                                fill: 'grey',
                                fontSize: 10,
                            }}
                            numberOfTicks={5}
                            formatLabel={ value => `${value}` }
                        />
                        <LineChart
                            style={{ flex: 1, marginLeft: 16 }}
                            data={ this.state.totalGoalData }
                            key={ this.state.totalGoalKey }
                            svg={{ stroke }}
                            numberOfTicks={5}
                            contentInset={{ top: 30, bottom: 30 }}
                        >
                        </LineChart>
                        </View>
                    </Card>
                    </View>
                }
            </Content>
        );
    }
}

const mapStateToProps = (state) => {
    return{
        loggedUser: state.loggedUser,
        dashboard: state.dashboard
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchDashboard: (user) => {
            dispatch(dashboard(user))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);