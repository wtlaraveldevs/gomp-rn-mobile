import React, {Component} from 'react';
import { 
  TouchableOpacity, StyleSheet, Text, View, Image,
  Dimensions, FlatList, TouchableWithoutFeedback, Share, Platform
} from 'react-native';
import { Container, Content, Header, Button, Form, 
    Input, Title, Item, Label, Toast } from 'native-base';
import { connect } from 'react-redux';
import { config } from '../config/index';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { resetRedirectionToGoals, getGoals, setGOM } from '../redux/actions';
import PaymentGateway from './util/paymentgateway';
import { primaryColor } from '../assets/Util/colors';

import dynamicLinks, { firebase } from '@react-native-firebase/dynamic-links';
import Clipboard from '@react-native-clipboard/clipboard';

class Goals extends Component {

    state = {
        img : '',
        visible: true,
        loader: false,
        favArray: [],
        hasData: false,
        _initial: true,
    }

    componentWillMount(){
        // get Goals from the server.
        this.props.fetchGoals({user_id: this.props.loggedUser.id});
    }

    componentWillReceiveProps(nextProps){
        this.setState({ _initial: false })
        if(nextProps.goals && nextProps.goals.length > 0)
        {
            this.setState({ favArray: nextProps.goals, hasData: true, })
        }
        else{
            this.setState({ favArray: [], hasData: false })
        }

        if(nextProps.fetchPots){
            this.props.fetchGoals({user_id: this.props.loggedUser.id});
        }

    }

    _renderHeader(){
        return(
            <View style={{height: 60 }}>
            <Grid style={{alignItems: 'center', paddingHorizontal: 10}}>
                <Row>
                <Col size={15} style={{justifyContent: 'center', alignItems: 'center'}}>
                    <Button transparent onPress={()=>{
                        this.props.reset();
                        setTimeout(()=>{
                            this.props.navigation.goBack()
                        }, 50)
                    }}>
                    <Icon name="chevron-left" size={30} color={'#FCC438'} />
                    </Button>
                </Col>
                <Col size={65} style={{justifyContent: 'center', alignItems: 'center'}}>
                    <Text style={{fontSize: 20}}>My GYFTmoni Pots</Text>
                </Col>
                <Col size={15} style={{justifyContent: 'center', alignItems: 'center'}}>
                </Col>
                </Row>
            </Grid>
            </View>
        )
    }

    handleShare = (id, image, title, creator) => {
        // alert("Sharing ID " + id);
        dynamicLinks().buildLink({
            link: `https://gyftmoni.com/${id}`,
            android: {
                packageName: "com.gyftmoni"
            },
            ios: {
                bundleId: "com.gyftmoni"
            },
            navigation: {
                forcedRedirectEnabled: true
            },
            // domainUriPrefix is created in your Firebase console
            domainUriPrefix: 'https://gyftmoni.page.link',
            // optional set up which updates Firebase analytics campaign
            // "banner". This also needs setting up before hand
            // analytics: {
            //   campaign: 'banner',
            // },
        })
        .then((link) => {
            console.log("Link Created " + link + " Image " + image);

            var path = `https://www.gyftmoni.com/public/campaigns/small/${image}`;
            var _message = `${title} by ${creator}, click to view the pot or visit our website. ${link}`;

            Share.share({
                url: path,
                message: _message 
            });
        });
        
    }

    CopyLinkToClipboard = (id, image, title, creator) => {
        var shareLink = `https://gyftmoni.page.link/?efr=1&ibi=com%2Egyftmoni&apn=com%2Egyftmoni&link=https%3A%2F%2Fgyftmoni%2Ecom%2F${id}https://www.gyftmoni.com/public/campaigns/small/${image}`;

        var message = `${title} by ${creator}, click to view the pot or visit our website. ${shareLink}`;

        Clipboard.setString(message);

        Toast.show({
            text: 'Copied and ready to paste.',
            textStyle: styles.toastText,
            position: 'top',
            style: styles.toast
        });
    }

    render() {
        return (
            <Container>
                <Content contentContainerStyle={{flex: 1}} >
                    <View style={{width: Dimensions.get('window').width, height: Platform.OS == "android" ? 60 : 85, backgroundColor: '#f6b93b', 
                    justifyContent:'center', alignItems: 'flex-end', flexDirection:'row', paddingBottom: 15}}>
                        {/* <Image source={require('../assets/img/logo.png')} style={{width: 32, height: 32, paddingBottom: 3, 
                            marginRight: 4, tintColor: '#fff'}} /> */}
                        <Image source={require('../assets/img/complete_logo.png')} style={{width: 144, height: 20, tintColor: '#fff'}} />
                    </View>
                    {this._renderHeader()}
                    {
                        this.state.loader ?
                        <PaymentGateway title={"Buy your GOM Pot"} buttonLabel={"Pay £2.00"} navigation={this.props.navigation} purchasePot={true} onHide={()=>{
                            this.setState({ loader: false })
                        }} /> 
                        : null
                    }
                    {
                        this.state._initial ? 
                        null :
                        this.state.hasData ?
                        <FlatList
                            data={this.state.favArray}
                            extraData={this.state}
                            keyExtractor={item => item.id}
                            style={{padding: 10}}
                            renderItem={(item)=>{
                                let url = `${config.smallImg}/${item.item.small_image}`;
                                return(
                                    <TouchableWithoutFeedback onPress={()=>{
                                        // this.props.navigation.navigate('PotView')
                                        this.props.setGM(item.item);
                                        setTimeout(()=>{
                                        this.props.navigation.navigate('PotView');
                                        }, 1000);
                                    }}>
                                    <Row style={{borderBottomColor: '#ddd', borderBottomWidth: 1, paddingBottom:10}}>
                                        <Col size={40} style={{marginRight: 7}}>
                                            <Image source={{uri: url}}
                                                style = {{height: 130, resizeMode : 'stretch', margin: 5 }}/>
                                        </Col>
                                        <Col size={50} style={{padding:5}}>
                                            <Row size={15}>
                                            </Row>
                                            <Row size={70}>
                                                <Label style={{fontSize: 24, color: 'black'}}>
                                                    {item.item.title}
                                                </Label>
                                            </Row>
                                            <Row size={15} style={{flexDirection: 'row'}}>
                                                <TouchableOpacity onPress={() => {
                                                    this.props.setGM(item.item);
                                                    setTimeout(()=>{
                                                        this.props.navigation.navigate('EditPot');
                                                    }, 600);
                                                }}
                                                style={{borderColor: primaryColor, marginRight: 5,
                                                justifyContent:'center', alignItems:'center',
                                                    paddingHorizontal: 15, borderWidth: 1, borderRadius: 10}}>
                                                    <Text style={{color: "blue"}}>Edit</Text>
                                                </TouchableOpacity>
                                                <TouchableOpacity style={{backgroundColor: primaryColor, 
                                                justifyContent:'center', alignItems:'center', marginRight: 5,
                                                    paddingHorizontal: 12, borderRadius: 10}}
                                                    onPress={this.handleShare.bind(this, item.item.token_id, 
                                                    item.item.small_image, item.item.title, item.item.name)}>
                                                    <Text style={{color: "white"}}>Share</Text>
                                                </TouchableOpacity>
                                                <TouchableOpacity style={{backgroundColor: primaryColor, justifyContent:'center', alignItems:'center',
                                                    paddingHorizontal: 10, borderRadius: 10}}
                                                    onPress={this.CopyLinkToClipboard.bind(this, item.item.token_id, 
                                                    item.item.small_image, item.item.title, item.item.name)}>
                                                    <Text style={{color: "white"}}>Copy link</Text>
                                                </TouchableOpacity>
                                            </Row>
                                        </Col>
                                        <Col size={10} style={{justifyContent:'center', alignItems:'center'}}>
                                            <Icon name={"chevron-right"} size={50} color={primaryColor} />
                                        </Col>
                                    </Row>
                                    </TouchableWithoutFeedback>
                                )
                            }}
                        /> :
                        <View style={{width: Dimensions.get('screen').width, height: Dimensions.get('screen').height/1.2,
                            justifyContent: 'center', alignItems: 'center'}}>
                            <View style={{borderWidth: 2, borderColor:'#f45302', padding: 25, borderRadius: 50, marginBottom: 10  }}>
                                <Icon name="emoticon-sad-outline" size={50} color={'#f45302'} />
                            </View>
                            <Text style={{fontSize: 20, marginBottom: 65}}>No GYFTmoni POTS.</Text>
                            <View style={{marginBottom: 15, backgroundColor: '#f45302', height: 2, width: 200}}></View>
                            <View>
                                <Text style={{fontSize: 20, marginBottom: 15}}>To create you first Pot, click here</Text>
                                <Button onPress={()=>{
                                    this.setState({ loader: true })
                                    }} style={{backgroundColor: '#f45302', width: 300, borderRadius: 5, 
                                    justifyContent: 'center', alignContent: 'center'}}
                                >
                                    <Text style={{color: 'white', fontSize: 18}}>Receive</Text>
                                </Button>
                            </View>
                        </View>
                    }
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    bottom:{
        flex: 1, backgroundColor: 'white', 
        paddingTop: 25,
        paddingBottom: 20,
        paddingLeft: 30,
        paddingRight: 30, 
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30
    },
    title:{
        fontSize: 26,
        fontWeight: 'bold',
        color: 'black'
    },
    animated_button : {
        backgroundColor: '#f45302',
        borderRadius: 30,
        width: 350,
    },
    button_label: {
        color: '#fff',
        fontSize: 18
    },
    toast: {
        marginTop: 30
    },
    toastText: {
        textAlign: 'center'
    }
});

const mapStateToProps = (state) => {
    return {
        goals: state.goals,
        loggedUser: state.loggedUser,
        fetchPots: state.fetchPots,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchGoals: (data) =>{
            dispatch(getGoals(data))
        },
        reset: () => {
            dispatch(resetRedirectionToGoals())
        },
        setGM: (data) =>{
            dispatch(setGOM(data))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Goals);
