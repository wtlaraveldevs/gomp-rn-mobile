/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, AsyncStorage, Text, View, Dimensions, Image, 
  ActivityIndicator, Linking} from 'react-native';
import { Container, Content, Header, Button, Left, Body, Right, Title, Icon, Fab } from 'native-base';
import { Grid, Row, Col } from 'react-native-easy-grid';
import Dialog, { DialogFooter, DialogButton, DialogContent } from 'react-native-popup-dialog';
import { connect } from 'react-redux';
import AnimatedBottom from './authencation/AnimatedBottom';
import PaymentGateway from './util/paymentgateway';
import { showLogin, setLoggedUser, getUserInfo, redirectToGoals, setGOM } from '../redux/actions';
import Fee from './fee';
import { config } from '../config';
import { primaryColor } from '../assets/Util/colors';

import Confetti from 'react-native-confetti';
import dynamicLinks from '@react-native-firebase/dynamic-links';

class App extends Component {

  state = {
    visible: false,
    loader: false,
    loading: false,
    isLoggedIn: false,
    showFee: false,
    hasActivePot: false
  }

  componentWillMount(){

    // dynamicLinks().getInitialLink().then((link) => {
    //   console.log("Link in the url " + link);
    // })

    dynamicLinks().onLink((link) => {
      alert("Dynamic Link " + link);
    })

    Linking.getInitialURL().then((link) => {
      // alert("Linking URL " + link);
      if(link){
        var objects = link.split("%2F");
        var potId = objects[objects.length - 1];
        if(potId){
          this.setState({loading: true})
          fetch(`${config.api.searchPot}`, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({ 
              text : potId
            })
          })
          .then(response=> { return response.json() })
          .then((response) => {

              if(response.status == 'success')
              {
                this.props.setSearchedGOM(response.data.data[0]);

                setTimeout(()=>{
                  this.setState({loading: false})
                  this.props.navigation.navigate('PotView');
                }, 3000);
              }
              else{
                this.setState({loading: false})
                alert("POT not found.")
              }
              
          })
          .catch((error) => {
            this.setState({loading: false})
            alert("Unable to find the pot " + error);
          });
        }
      }
    })
    
    AsyncStorage.getItem('isLoggedIn').then((item)=>{
      // console.log("IsloggedIn", item);
      if(item == "true")
      {
        this.setState({ isLoggedIn: true })
      }
      else{
        this.setState({ isLoggedIn: false })
      }
    });

    AsyncStorage.getItem('user').then((user)=>{
      // this.props.setUser(user);
      console.log("User Stored", user);
      if(user)
      {
        console.log("Logging the user again");
        var _user = JSON.parse(user);
        var data = {
          email: _user.email
        }
        console.log("GetUserInfo Data", data);
        this.props.getUserDetails(data)
      }
      // if(user != "" || user != null)
      // {
      //   var _user = JSON.parse(user);
      //   // get active pot status
      //   this.getAcitvePot(_user.id);
      // }
    });
  }

  getAcitvePot(user_id){
    try{
      fetch(`${config.api.hasActivePot}`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          user_id: user_id
        })
      })
      .then((response) => {
        return response.json();
      })
      .then((response) => {
        if(response.status == false){
          console.log("something went wrong on the server. Please check logs.");
        }
        else {
          console.log("Has an active pot? ", response.hasActivePot );
          this.setState({
            hasActivePot: response.hasActivePot
          });
        }
      })
    }
    catch(error)
    {
      alert("Unable to fetch active pot information");
    }
  }


  componentDidMount() {
    // if(this._confettiView) {
    //   this._confettiView.startConfetti();
    // }
    this._confettiView.startConfetti();
  }

  componentWillReceiveProps(nextProps){
    // if(nextProps.isLoggedIn == true)
    // {
      
    // }
    console.log('isLoggedIn', nextProps.isLoggedIn);
    this.setState({isLoggedIn: nextProps.isLoggedIn == true ? true: false})

    // if(nextProps.isLoggedIn && this.props.loggedUser != null)
    // {
    //   this.getAcitvePot(this.props.loggedUser.id);
    // }

    

    if(nextProps.redirectToFavorites)
    {
      this.props.navigation.navigate('Favorites');
    }
    if(nextProps.redirectToGoals)
    {
      this.props.navigation.navigate('Goals');
    }
    else if(nextProps.redirectToMyAccount)
    {
      this.props.navigation.navigate('Account');
    }
  }

  _renderFeeView = () => {
    if(this.state.showFee)
    {
      return <Fee onDismiss={()=>{this.setState({showFee: false})}} />
    }
    else return null;
  }

  _renderLoader = () =>{
    return(
      <View>
        <Dialog
          visible={this.state.loading}
          dialogStyle={{width: 200, padding: 15}}
        >
          <DialogContent>
            <ActivityIndicator size={"large"} color={"golden"} />
            <Text style={{fontSize: 20}}>Please wait...</Text>
          </DialogContent>
        </Dialog>
      </View>
    )
  }

  _renderSearchDialog = () =>{
    return(
      <View style={styles.container}>
        <Dialog
          visible={this.state.visible}
          dialogStyle={{width: '100%'}}
          footer={
            <DialogFooter>
              <DialogButton
                text="CANCEL"
                onPress={() => {
                  this.setState({visible: false})
                }}
              />
              <DialogButton
                text="OK"
                onPress={() => {
                  this.setState({visible: false})
                  // but send a request to api
                }}
              />
            </DialogFooter>
          }
        >
          <DialogContent>
            <Text>welcome</Text>
          </DialogContent>
        </Dialog>
      </View>
    )
  }

  render() {
    const navigation = this.props.navigation;
    return (
      <Container>
        <View style={{width: Dimensions.get('window').width, height: 60, backgroundColor: '#f6b93b', 
        justifyContent:'center', alignItems: 'flex-end', flexDirection:'row', paddingBottom: 15}}>
            <View style={{justifyContent:'center', alignItems: 'center', flexDirection:'row',}}>
              <Image source={require('../assets/img/logo.png')} style={{width: 32, height: 32, paddingBottom: 3, 
                  marginRight: 4, tintColor: '#fff'}} />
              <Image source={require('../assets/img/complete_logo.png')} style={{width: 144, height: 20, tintColor: '#fff'}} />
            </View>
        </View>
        <Content>
        {/* <ImageBackground source={require('../assets/img/background.png')} style={{width: '100%', height: '100%'}} > */}
        {this._renderSearchDialog()}
        {
            this.state.loader ?
            <PaymentGateway title={"Buy your GYFTmoni Pot"} height={470}
            description={"We will send you your unique GYFTmoni Pot ID to share with others so that you can start receiving Gift Of Money"} buttonLabel={"Pay £2.00"} navigation={this.props.navigation} purchasePot={true} onHide={()=>{
                this.setState({ loader: false })
            }} /> 
            : null
        }
        {this._renderFeeView()}
        
        <Grid style={{padding: 15, paddingBottom: 60}}>
          <Col>
            <Row style={{marginBottom: 10}}>
              <Button onPress={()=>{ navigation.navigate("Give") }} style={{backgroundColor: '#f6b93b', justifyContent: 'center',
                alignItems: 'center', borderColor: 'yellow', borderWidth: 1, borderRadius: 7,
                 width: Dimensions.get('screen').width/ 3.25, height: Dimensions.get('screen').height/ 10}}>
                <Text style={{fontSize: 18, textAlign: 'center', color: 'white'}} >{`Send\nGYFTmoni`}</Text>
              </Button>
            </Row>
            <Row style={{marginBottom: 10}}>
              <Button onPress={() => {
                // AsyncStorage.getItem('isLoggedIn',(status)=>{
                //   console.log("checking logging", status);
                //   if(status)
                //   {
                //     this.setState({ loader: true })
                //   }
                //   else{
                //     this.props.showDialog();
                //   }
                // })
                console.log("isLoggedIn", this.state.isLoggedIn);
                console.log("User ", this.props.loggedUser);
                
                if(this.props.loggedUser)
                {
                  if(this.props.loggedUser.active_purchased_pot == '1'){ // User has an active pot.
                    this.props.navigation.navigate("CreatePot")
                  }
                  else {
                    this.setState({ loader: true })
                  }
                }
                else{
                  this.props.showDialog();
                }
              }}
              style={{backgroundColor: '#f6b93b', justifyContent: 'center',
                alignItems: 'center', borderColor: 'yellow', borderWidth: 1, borderRadius: 7,
                 width: Dimensions.get('screen').width/ 3.25, height: Dimensions.get('screen').height/ 10}}>
                <Text style={{fontSize: 18, textAlign: 'center', color: 'white'}} >{`Receive\nGYFTmoni`}</Text>
              </Button>
            </Row>
            <Row style={{marginBottom: 10}}>
              <Button onPress={()=> { 
                if(this.props.loggedUser)
                {
                  this.props.navigation.navigate('Goals')
                }
                else{
                  if(Platform.OS === 'ios' && !this.props.loggedUser)
                  {
                    this.props.navigation.navigate('Login');
                  }
                  else{
                    this.props.showDialog()
                  }
                }
               }} style={{backgroundColor: '#f6b93b', justifyContent: 'center',
                alignItems: 'center', borderColor: 'yellow', borderWidth: 1, borderRadius: 7,
                width: Dimensions.get('screen').width/ 3.25, height: Dimensions.get('screen').height/ 10}}>
                <Text style={{fontSize: 17, textAlign: 'center', color: 'white'}} >{`My GYFTmoni\nPots`}</Text>
              </Button>
              {/** #FCC438 */}
            </Row>
            <Row style={{marginBottom: 10}}>
              <Button onPress={()=> { 
                if(this.props.loggedUser)
                {
                  this.props.navigation.navigate('Wallet')
                }
                else{
                  if(Platform.OS === 'ios' && !this.props.loggedUser)
                  {
                    this.props.navigation.navigate('Login');
                  }
                  else{
                    this.props.showDialog()
                  }
                }
               }} style={{backgroundColor: '#f6b93b', justifyContent: 'center',
                alignItems: 'center', borderColor: 'yellow', borderWidth: 1, borderRadius: 7,
                width: Dimensions.get('screen').width/ 3.25, height: Dimensions.get('screen').height/ 10}}>
                <Text style={{fontSize: 20, textAlign: 'center', color: 'white'}} >Get My Money</Text>
              </Button>
              {/** #FCC438 */}
            </Row>
            <Row style={{marginBottom: 10}}>
              <Button onPress={()=> { 
                if(this.props.loggedUser)
                {
                  this.props.navigation.navigate('DashboardContainer', {
                    viewNumber: 2
                  })
                }
                else{
                  if(Platform.OS === 'ios' && !this.props.loggedUser)
                  {
                    this.props.navigation.navigate('Login');
                  }
                  else{
                    this.props.showDialog()
                  }
                }
               }} style={{backgroundColor: '#f6b93b', justifyContent: 'center',
                alignItems: 'center', borderColor: 'yellow', borderWidth: 1, borderRadius: 7,
                width: Dimensions.get('screen').width/ 3.25, height: Dimensions.get('screen').height/ 10}}>
                <Text style={{fontSize: 18, textAlign: 'center', color: 'white'}} >{`ThankYous\n& Messages`}</Text>
              </Button>
            </Row>
            <Row style={{marginBottom: 10}}>
              <Button onPress={()=> { 
                if(this.props.loggedUser)
                {
                  this.props.navigation.navigate('DashboardContainer', {
                    viewNumber: 0
                  })
                }
                else{
                  if(Platform.OS === 'ios' && !this.props.loggedUser)
                  {
                    this.props.navigation.navigate('Login');
                  }
                  else{
                    this.props.showDialog()
                  }
                }
               }} style={{backgroundColor: '#f6b93b', justifyContent: 'center',
                alignItems: 'center', borderColor: 'yellow', borderWidth: 1, borderRadius: 7,
                width: Dimensions.get('screen').width/ 3.25, height: Dimensions.get('screen').height/ 10}}>
                <Text style={{fontSize: 20, textAlign: 'center', color: 'white'}} >Dashboard</Text>
              </Button>
            </Row>
            {/* <Row style={{marginBottom: 10}}>
              <Button onPress={()=> {alert('This feature is under development.')}} style={{backgroundColor: '#f6b93b', justifyContent: 'center',
                alignItems: 'center', borderColor: 'yellow', borderWidth: 1, borderRadius: 7,
                 width: Dimensions.get('screen').width/ 3.25, height: Dimensions.get('screen').height/ 10}}>
                <Text style={{fontSize: 20, textAlign: 'center', color: 'white'}} >Messages</Text>
              </Button>
            </Row> */}
            <Row style={{marginBottom: 10}}>
              <Button onPress={()=>{ navigation.navigate("Search") }} style={{backgroundColor: '#f6b93b', justifyContent: 'center',
                alignItems: 'center', borderColor: 'yellow', borderWidth: 1, borderRadius: 7,
                 width: Dimensions.get('screen').width/ 3.25, height: Dimensions.get('screen').height/ 10}}>
                <Text style={{fontSize: 20, textAlign: 'center', color: 'white'}} >Search</Text>
              </Button>
            </Row>
            <Row style={{marginBottom: 10}}>
              <Button onPress={()=> { this.setState({showFee: true}) }} style={{backgroundColor: '#f6b93b', justifyContent: 'center',
                alignItems: 'center', borderColor: 'yellow', borderWidth: 1, borderRadius: 7,
                 width: Dimensions.get('screen').width/ 3.25, height: Dimensions.get('screen').height/ 10}}>
                <Text style={{fontSize: 20, textAlign: 'center', color: 'white'}} >Fees</Text>
              </Button>
            </Row>
            <Row style={{marginBottom: 10}}>
              <Button onPress={()=>{ navigation.navigate("howitworks") }} style={{backgroundColor: '#f6b93b', justifyContent: 'center',
                alignItems: 'center', borderColor: 'yellow', borderWidth: 1, borderRadius: 7,
                 width: Dimensions.get('screen').width/ 3.25, height: Dimensions.get('screen').height/ 10}}>
                <Text style={{fontSize: 20, textAlign: 'center', color: 'white'}} >{`How It\nWorks`}</Text>
              </Button>
            </Row>
          </Col>
          <Col>
          </Col>
        </Grid>
        {/* </ImageBackground> */}
          {this._renderLoader()}
        </Content>
        <Confetti 
          ref={(node) => this._confettiView = node}
        />
        <Fab
          active={this.state.active}
          style={{ backgroundColor: primaryColor, marginBottom: 15, zIndex: 20 }}
          position="bottomRight"
          onPress={() => this.props.navigation.navigate("howitworks") }>
          <Icon name="help" color={"#fff"} />
        </Fab>
        <AnimatedBottom navigation={this.props.navigation} />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

const mapStateToProps = (state) => {
  return {
    isLoggedIn: state.isLoggedIn,
    loggedUser: state.loggedUser,
    redirectToFavorites: state.redirectToFavorites,
    redirectToGoals: state.redirectToGoals,
    redirectToMyAccount: state.redirectToMyAccount
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    showDialog: () => {
      dispatch(showLogin())
    },
    setUser: (user) => {
      dispatch(setLoggedUser(user));
    },
    getUserDetails: (data) => {
      dispatch(getUserInfo(data))
    },
    setSearchedGOM : (data) => {
      dispatch(setGOM(data))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
