import React, { Component } from 'react';
 
import {
  StyleSheet,
  Text,
  Animated,
  Easing,
  Dimensions,
  TouchableWithoutFeedback,
  TouchableOpacity
} from 'react-native';

const ScalingButton = (props) => {
    var scaleValue = new Animated.Value(0);
    this.xTranslate = new Animated.Value(0);
    this.width = new Animated.Value(0);

    function scale() {
        scaleValue.setValue(0);
        Animated.timing(
            scaleValue,
            {
              toValue: 1,
              duration: 300,
              easing: Easing.linear
            }
        ).start();

        // this.xTranslate.setValue(0); // reset the animated value
        // Animated.spring(this.xTranslate, {
        //   toValue: 1,
        //   friction: 6 
        // }).start();

        // this.width.setValue(0);
        // Animated.timing(
        //     scaleValue,
        //     {
        //       toValue: 1,
        //       duration: 300,
        //       easing: Easing.ease
        //     }
        // ).start();

    }

    const buttonScale = scaleValue.interpolate({
        inputRange: [0, 0.5, 1],
        outputRange: [1, 1.1, 1]
    });

    let negativeWidth = - Dimensions.get('screen').width/2 + 20;
      let modalMoveY = this.xTranslate.interpolate({
        inputRange: [0, 1],
        outputRange: [-negativeWidth, 1]
      });

    let translateStyle = { transform: [{ translateX: modalMoveY }] };
    
    const originalButton = scaleValue.interpolate({
        // inputRange: [0, 0.5, 0.5],
        // outputRange: [1, 1.1, 1]
        inputRange: [0, 1],
        outputRange: [1, 1.2]
    });

    let originalWidth = { transform : [ { scaleX : originalButton } ] }
    // let originalWidth = { width: originalButton };

    return (
        <TouchableOpacity onPress={onPress}>
            <Animated.View style={[ 
                props.noDefaultStyles ? styles.default_button : styles.button, 
                props.styles ? props.styles.button : '',
                {
                    transform: [
                        {scale: buttonScale}
                    ]
                }
                // originalWidth
                ]}
            >
                { getContent() }
            </Animated.View>
        </TouchableOpacity>
    );

    function onPress() {
        scale();
        props.onPress();
    }

    function getContent() {
        if(props.children){
            return props.children;
        }
        return <Text style={props.styles.label}>{ props.label }</Text>;
    }
}

const styles = StyleSheet.create({
    default_button: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 20,
        borderWidth: 1,
        borderColor: '#eee',
        margin: 20
    },
});
 
export default ScalingButton;
