import React, { Component } from 'react';
 
import {
  StyleSheet,
  Text,
  Animated,
  Easing,
  Dimensions,
  View,
  ActivityIndicator,
  TouchableWithoutFeedback,
  Alert,
  Platform
} from 'react-native';
import Dialog, { DialogFooter, DialogButton, DialogContent } from 'react-native-popup-dialog';
import { Button, Form, 
    Input, Item, Label } from 'native-base';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';
import { buyPot, payWithMangoPay } from '../../redux/actions';
import KeyboardSpacer from 'react-native-keyboard-spacer'

let scaleValue = new Animated.Value(0);

let buttonScale = scaleValue.interpolate({
    inputRange: [0, 0.5, 1],
    outputRange: [1, 1.1, 1]
});

class PaymentGateway extends Component {

    constructor(props){
        super(props);
        this.state = {
            visible: true,
            card: '',
            expiryMM: '',
            expiryYY: '',
            cvc: '',
            inProgress: false
        }
    }
    
    scale() {
        scaleValue.setValue(0);
        Animated.timing(
            scaleValue,
            {
              toValue: 1,
              duration: 300,
              easing: Easing.linear
            }
        ).start();
    }

    componentWillReceiveProps = (nextProps) => {
        if(nextProps.apiStatus == true){
            this.setState({inProgress: false, visible: false});
            setTimeout(()=>{
                if(this.props.purchasePot == true)
                {
                    // Redirect to POT Creating View
                    this.props.navigation.navigate("CreatePot")
                }
                else{
                    this.props.navigation.navigate("PotView")
                    // alert('Something went wrong, Please try again.');
                }
            }, 500);
        }

        if(nextProps.potPurchase.status == true)
        {
            this.setState({inProgress: false, visible: false});
            if(this.props.purchasePot == true)
            {
                // Redirect to POT Creating View
                this.props.navigation.navigate("CreatePot")
            }
            else{
                this.props.navigation.navigate("PotView")
                // alert('Something went wrong, Please try again.');
            }
        }
        else{
            if(nextProps.potPurchase.message != undefined && nextProps.potPurchase.message != '' &&
            nextProps.potPurchase.message != null){
                alert(nextProps.potPurchase.message);
            }
        }

        // else
        // {
            
        //     // if(nextProps.apiMessage != '')
        //     // {
        //     //     alert(nextProps.apiMessage);
        //     // }
        // }
    }

    _handlePayment = () => {
        this.setState({inProgress: true})
        
        console.log(this.props.purchasePot);
        if(this.props.purchasePot == false)
        {
            // call action to send request to server to send gift money to pot.
            let obj = {
                pot_id: this.props.id, //1,
                firstname: this.props.firstname, //'John',
                lastname: this.props.lastname, //'Cena',
                email: this.props.email, //"johncena2@gmail.com",
                amount: this.props.amount, //10,
                phone: this.props.phone, //"123456789",
                message: this.props.message, //"Message from RN",
                card: this.state.card,
                expiry: this.state.expiryMM + this.state.expiryYY,
                cvc: this.state.cvc,
            }
            this.props.sendGift(obj);

        }else{
            // buying POT mechanism
            let obj = {
                user_id: this.props.loggedUser.id,
                card: this.state.card,
                expiry: this.state.expiryMM + this.state.expiryYY,
                cvc: this.state.cvc,
            }
            this.props.buyPOT(obj);
        }
    }

    render(){
        return (
            
                <Dialog
                        containerStyle={{paddingBottom: Platform.OS == "ios" ? 130 : 10}}
                        visible={this.state.visible}
                        dialogStyle={{width: 340, padding: 15, height: this.props.height}}
                        >
                        <DialogContent style={{height: this.props.title ? undefined : 320}}>
                            <View style={{width: '100%', flexDirection: 'row'}}>
                                <Button onPress={() => {
                                    this.setState({visible: false})
                                    setTimeout(()=>{
                                        this.props.onHide();
                                    }, 1000);
                                }}
                                transparent style={{marginTop: 5,
                                width: 50, position: 'absolute', top: -20, right: -50}}>
                                    <Icon name={'close'} size={30} style={{color: '#f6b93b', fontSize: 20}} />
                                </Button>
                                <Label style={{fontSize: 20, fontWeight: "bold"}}>{this.props.title ? this.props.title : 'Pay with MangoPay'}</Label>
                            </View>
                            <View style={{backgroundColor: '#f6b93b', height: 2, marginTop: 10, marginBottom: 20}}></View>
                            {
                                this.props.description ?
                                <View style={{marginBottom:20}}>
                                    <Text style={{textAlign: 'justify', fontSize: 16, marginBottom: 3}}>1. Buy your £2 GYFTmoni Pot and fill in some details.</Text>
                                    <Text style={{fontSize: 16, marginBottom: 3}}>2.  We will send you the unique number of  your GYFTmoni Pot.</Text>
                                    <Text style={{textAlign: 'justify', fontSize: 16, marginBottom: 3}}>3. Share your GYFTmoni Number with others to start receiving Gifts Of Money in your unique GYFTmoni Pot.</Text>
                                </View>
                                : 
                                <View>
                                    <Text style={{textAlign: 'justify'}}>{this.props.description}</Text>
                                </View>
                            }
                            <Form style={{marginBottom: 20}}>
                                <Item regular style={{backgroundColor: 'transparent', borderColor: 'black', borderRadius: 7,  
                                marginBottom: 10,  width: '100%', paddingLeft: 10 }}>
                                <Icon name="credit-card" size={23} style={{marginRight: 5, color: '#f6b93b'}} />
                                <Input placeholderTextColor={'#f6b93b'} keyboardType={"number-pad"} style={{color: 'black'}} 
                                placeholder={"Valid Card Number"} onChangeText = {(text)=>{this.setState({card: text})}} />
                                </Item>
                                <View style={{flexDirection: 'row'}}>
                                <Item regular style={{backgroundColor: 'transparent', borderColor: 'black', borderRadius: 7,
                                marginBottom: 10,  width: '55%', paddingLeft: 10, marginRight: '1%' }}>
                                <Icon name="calendar" size={23} style={{marginRight: 5,color: '#f6b93b'}} />
                                <Input keyboardType={"number-pad"} maxLength={2}  placeholderTextColor={'#f6b93b'} style={{color: 'black'}} 
                                placeholder={"MM"} onChangeText = {(text)=>{ this.setState({expiryMM: text}) }} />
                                <Input keyboardType={"number-pad"} maxLength={2} placeholderTextColor={'#f6b93b'} style={{color: 'black'}} 
                                placeholder={"YY"} onChangeText = {(text)=>{ this.setState({expiryYY: text}) }} />
                                </Item>
                                <Item regular style={{backgroundColor: 'transparent', borderColor: 'black', borderRadius: 7,
                                marginBottom: 10, width: '44%', paddingLeft: 10 }}>
                                    <Icon name="lock" size={23} style={{marginRight: 5,color: '#f6b93b'}} />
                                    <Input keyboardType={"number-pad"} maxLength={4} placeholderTextColor={'#f6b93b'} style={{color: 'black'}} 
                                    placeholder={"CVC"} onChangeText = {(text)=>{ this.setState({cvc: text}) }} />
                                </Item>
                                </View>
                            </Form>
                            {
                                this.state.inProgress ?
                                <View style={{height: 50, backgroundColor: '#f6b93b', 
                                borderRadius: 20, width: '100%', justifyContent:"center", alignItems:"center"}}>
                                    <ActivityIndicator color={"white"} size={"large"} />
                                </View>
                                :
                                <Button onPress={this._handlePayment.bind(this)} 
                                transparent style={{height: 50, backgroundColor: '#f6b93b', borderRadius: 20, width: '100%'}}>
                                    <Text style={{width: '100%', textAlign: 'center', color: 'white', 
                                    fontSize: 20}}>
                                        {this.props.buttonLabel}
                                    </Text>
                                </Button>
                            }
                        </DialogContent>
                        <KeyboardSpacer />
                        </Dialog>
            
        )
    }

    onPress() {
        scale();
        // this.props.onPress();
    }

}

const styles = StyleSheet.create({
    default_button: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 20,
        borderWidth: 1,
        borderColor: '#eee',
        margin: 20
    },
});
 
const mapStateToProps = (state) => {
    return {
        apiStatus: state.apiStatus,
        apiMessage: state.apiMessage,
        loggedUser: state.loggedUser,
        potPurchase: state.potPurchase
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        sendGift: (data) => {
            dispatch(payWithMangoPay(data))
        },
        buyPOT: (data) => {
            dispatch(buyPot(data))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PaymentGateway);
