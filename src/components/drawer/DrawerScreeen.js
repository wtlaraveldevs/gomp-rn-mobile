import React, {Component} from 'react';
import {NavigationActions} from 'react-navigation';
import PropTypes from 'prop-types';
import {ScrollView, Text, View, Image, ImageBackground, TouchableWithoutFeedback, 
  Share, AsyncStorage  } from 'react-native';
import { DrawerActions } from 'react-navigation';
import styles from '../../styles/index';
import { Thumbnail } from 'native-base';
// import { connect } from 'react-redux';
// import { switchToDayView, switchTo3DaysView, switchToWeekView, 
//   switchToMonthView, showAddonDialog, showChangeBranch, 
//   showNewBranch, showBranchManagement, disableFromViewInSendingManual} from '../../actions';
// import Divider from 'react-native-divider';
import Icon from 'react-native-vector-icons/Octicons';

class DrawerScreen extends Component {

  constructor(props)
  {
    super(props);
    this.handleAction = this.handleAction.bind(this);
    this.Share = this.Share.bind(this);
    this.Logout = this.Logout.bind(this);
  }
  
  navigateToScreen = (route) => () => {
    // console.log(route);

    if(route == "ShareManual")
    {
      this.props.setFromViewDisabled(true);
    }
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(DrawerActions.closeDrawer());
    this.props.navigation.dispatch(navigateAction);
  }

  handleAction = (actionName) => () =>
  {
    switch(actionName)
    {
      case "Day":
        this.props.switchToDay();
        this.props.navigation.dispatch(DrawerActions.closeDrawer())
      break;
      case "3Days":
        this.props.switchTo3Days();
        this.props.navigation.dispatch(DrawerActions.closeDrawer())
      break;
      case "Month":
        this.props.switchToMonth();
        this.props.navigation.dispatch(DrawerActions.closeDrawer())
      break;
      case "Addon":
        this.props.AddonDialog();
        this.props.navigation.dispatch(DrawerActions.closeDrawer())
      break;
      // case "ChangeBranch":
      //   this.props.showCBDialog();
      //   this.props.navigation.dispatch(DrawerActions.closeDrawer())
      // break;
      // case "NewBranch":
      //   this.props.showNBDialog();
      //   this.props.navigation.dispatch(DrawerActions.closeDrawer())
      // break;
      case "BranchManagement":
        this.props.showBMDialog();
        this.props.navigation.dispatch(DrawerActions.closeDrawer())
      break;
      default:
        this.props.switchToWeek();
        this.props.navigation.dispatch(DrawerActions.closeDrawer())
      break;
    }
    console.log("Action Clicked " + actionName);
  }

  Share = () =>{
    this.props.navigation.dispatch(DrawerActions.closeDrawer())
    Share.share({
      message: 'Hi, I am using Dental Diary to keep track of my appointments. Download now! http://bit.ly/2jPzvi0',
      url: 'https://apple.co/2wiXDzf',
      title: 'Dental Diary'
    }, {
      // Android only:
      dialogTitle: 'Share goodness',
      // iOS only:
      // excludedActivityTypes: [
      //   'com.apple.UIKit.activity.PostToTwitter'
      // ]
    })
  }

  Logout = () =>{
    this.props.navigation.dispatch(DrawerActions.closeDrawer())
    AsyncStorage.clear();
    this.props.navigation.navigate("Login")
  }

  render () {
    return (
      <View>
        <ScrollView>
          <View>
          <View
            
            style={{
              height: 140,
              alignSelf: "stretch",
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "#1B6BAE",
              marginBottom: 10
            }}>
            {/* <Image
              square
              style={{ height: 80, width: 80, marginBottom: 10 }}
              source={require('../../images/logo.png')}
            /> */}
            <Text style={{color: "#fff", fontSize: 18}}>Dental Diary</Text>
          </View>

            {/* Heading */}
            <Text style={{fontSize: 15, marginLeft: 15}}>Views</Text>

            <TouchableWithoutFeedback onPress={this.handleAction('Day')}>
              <View style={styles.menuItem}>
                
                <Text style={{fontSize: 15}}>
                  Day
                </Text>
              </View> 
            </TouchableWithoutFeedback>

            <TouchableWithoutFeedback onPress={this.handleAction('3Days')}>
              <View style={styles.menuItem}>
              
                <Text style={{fontSize: 15}} > 
                  3 Days
                </Text>
              </View>
            </TouchableWithoutFeedback>

            <TouchableWithoutFeedback onPress={this.handleAction('Week')}>
            <View style={styles.menuItem}>
              <Text style={{fontSize: 15}} >
                 Week
              </Text>
            </View>
            </TouchableWithoutFeedback>

            <TouchableWithoutFeedback onPress={this.handleAction('Month')}>
            <View style={styles.menuItem}>
              <Text style={{fontSize: 15}} >
                 Month
              </Text>
            </View>
            </TouchableWithoutFeedback>

            <TouchableWithoutFeedback onPress={this.navigateToScreen('TeamView')}>
              <View style={styles.menuItem}>
                <Text style={{fontSize: 15, color: '#000'}}>
                  Team
                </Text>
              </View>
            </TouchableWithoutFeedback>
            
            <View style={styles.divider} />

            <Text style={{fontSize: 15, marginLeft: 15}}>
                Settings
            </Text>

            <TouchableWithoutFeedback onPress={this.handleAction('BranchManagement')}>
              <View style={styles.menuItem}>
              {/* <Thumbnail style={{width: 20, height: 20, marginRight: 20, marginLeft: 5}} square 
              source={require('../../images/icon_adons.png')} /> */}
                <Icon style={{marginLeft: 10, marginRight: 20}} size={24} name="repo-forked" />
                <Text style={{fontSize: 15, color: '#000'}}>
                  Branch Management
                </Text>
              </View>
            </TouchableWithoutFeedback>

            {/* <TouchableWithoutFeedback onPress={this.navigateToScreen('Subscription')}>
              <View style={styles.menuItem}>
              <Thumbnail style={{width: 20, height: 20, marginRight: 20, marginLeft: 5}} square 
              source={require('../../images/ic_subscription.png')} />
                <Text style={{fontSize: 15, color: '#000'}}>
                  Subscription
                </Text>
              </View>
            </TouchableWithoutFeedback> */}

            <View style={styles.divider} />

            {/* <TouchableWithoutFeedback onPress={this.Logout}>
              <View style={styles.menuItem}>
              <Thumbnail style={{width: 20, height: 20, marginRight: 20, marginLeft: 5}} square 
              source={require('../../images/ic_password.png')} />
                <Text style={{fontSize: 15, color: '#000'}}>
                  Logout
                </Text>
              </View>
            </TouchableWithoutFeedback> */}

            <TouchableWithoutFeedback onPress={this.navigateToScreen('AboutUs')}>
              <View style={styles.menuItem}>
              {/* <Thumbnail style={{width: 20, height: 20, marginRight: 20, marginLeft: 5}} square 
              source={require('../../images/ic_password.png')} /> */}
              <Icon style={{marginLeft: 5, marginRight: 15}} size={24} name="organization" />
                <Text style={{fontSize: 15, color: '#000'}}>
                  About us
                </Text>
              </View>
            </TouchableWithoutFeedback>

          </View>
        </ScrollView>
      </View>
    );
  }
}

DrawerScreen.propTypes = {
  navigation: PropTypes.object
};

// const mapStateToProps = (state) => {
//   // const orderComplete = _.map(state.ListOfOrders, (val, uid) => {
//   //     return {
//   //       ...val,
//   //       uid
//   //     };
//   // });

//   return { 
//       orderComplete
//    };
// }

const mapDispatchToProps = dispatch => {
  return{
    switchToDay: () => {
      dispatch(switchToDayView())
    },
    switchTo3Days: () => {
      dispatch(switchTo3DaysView())
    },
    switchToWeek: () => {
      dispatch(switchToWeekView())
    },
    switchToMonth: () => {
      dispatch(switchToMonthView())
    },
    AddonDialog: () => {
      dispatch(showAddonDialog(true))
    },
    showCBDialog: () => {
      dispatch(showChangeBranch(true))
    },
    showNBDialog: () => {
      dispatch(showNewBranch(true))
    },
    showBMDialog: () => {
      dispatch(showBranchManagement(true))
    },
    setFromViewDisabled: (x) => {
      dispatch(disableFromViewInSendingManual(x))
    }
  };
}

// export default connect(null, mapDispatchToProps)(DrawerScreen);
export default DrawerScreen;