/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import { ActivityIndicator, StyleSheet, Text, View, Dimensions, Image, ImageBackground, Platform } from 'react-native';
import { Container, Content, Header, Button, Card, CardItem, Left, Body, Right, Input, Title,  Form, Item, Label } from 'native-base';
import { Grid, Row, Col } from 'react-native-easy-grid';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Dialog, { DialogFooter, DialogButton, DialogContent } from 'react-native-popup-dialog';
import { config } from '../config/index';
import { connect } from 'react-redux';
import PotView from './potview';
import { setGOM } from '../redux/actions/index';

class Give extends Component {

  state = {
    loader: false,
    searchText : ''
  }

  _renderLoader = () =>{
    return(
      <View>
        <Dialog
          visible={this.state.loader}
          dialogStyle={{width: 200, padding: 20}}
        >
          <DialogContent>
            <ActivityIndicator size={"large"} color={"golden"} />
            <Text style={{fontSize: 20}}>Please wait...</Text>
          </DialogContent>
        </Dialog>
      </View>
    )
  }

  _handleTextChange = (text) => {
    this.setState({ searchText: text })
  }

  _handleSearch = () => {
    if(this.state.searchText == null || this.state.searchText == '')
    {
      alert('Please provide a valid GM ID to search the Goal');
    }
    else{
      this.setState({loader: true})
      fetch(`${config.api.searchPot}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ 
          text : this.state.searchText 
        })
      })
      .then(response=> { return response.json() })
      .then((response) => {

          if(response.status == 'success')
          {
            this.props.setSearchedGOM(response.data.data[0]);

            setTimeout(()=>{
              this.setState({loader: false})
              this.props.navigation.navigate('PotView');
            }, 3000);
          }
          else{
            this.setState({loader: false})
            alert("POT not found.")
          }
          
      })
      .catch((error) => {
        console.error(error);
      });
    }
  }

  _renderSearchView = () => {
    return (
      <View style={{ height: 320, }}>
        <ImageBackground source={require('../assets/img/template_border_old.jpg')} 
        style={{width:'100%', height: '100%'}} >
        <Card style={{backgroundColor: 'transparent', height: 320, borderRadius: 10, 
        marginTop: 0, marginBottom: 0, marginLeft: 0, marginRight: 0, 
          alignItems: 'center', paddingBottom: 10}}>
            <CardItem style={{backgroundColor:'transparent', marginTop: '7%', width: Dimensions.get('window').width/1.25, marginBottom: 10}}>
                <Body>
                    <Text style={{fontSize: 20, textAlign: 'justify', marginBottom: '10%'}}>
                        Enter the Unique GYFTmoni Number of the person you want to Gift.
                    </Text>
                    <Item regular style={{borderColor: 'black', borderRadius: 5}}>
                      <Input 
                        onChangeText={this._handleTextChange.bind(this)} placeholderTextColor={'gray'}
                        placeholder={"XXXXX"} 
                        style={{}}
                      />
                    </Item>
                </Body>
            </CardItem>
            <CardItem footer style={{justifyContent: 'center', alignItems: 'center', backgroundColor:'transparent'}}>
              <Button transparent onPress={this._handleSearch.bind(this)} 
              style={{width: Dimensions.get('window').width/1.5, borderRadius: 8, justifyContent: 'center', alignItems: 'center',
              backgroundColor: '#f6b93b'}}>
                <Text style={{color: 'white', fontSize: 20}}>SEARCH</Text>
              </Button>
            </CardItem>
        </Card>
        </ImageBackground>
      </View>
    )
  }

  render() {
    return (
      <Container>
        <View style={{width: Dimensions.get('window').width, height: Platform.OS == "android" ? 60 : 85, backgroundColor: '#f6b93b', 
        justifyContent:'center', alignItems: 'flex-end', flexDirection:'row', paddingBottom: 15}}>
            {/* <Image source={require('../assets/img/logo.png')} style={{width: 32, height: 32, paddingBottom: 3, 
                marginRight: 4, tintColor: '#fff'}} /> */}
            <Image source={require('../assets/img/complete_logo.png')} style={{width: 144, height: 20, tintColor: '#fff'}} />
        </View>
        {/* <Header style={{marginLeft: -2, width: '102%', backgroundColor: 'transparent' }}> */}
          <View style={{height: 60,}}>
            <Grid style={{alignItems: 'center', paddingHorizontal: 10}}>
              <Row>
                <Col size={15} style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Button transparent onPress={()=>{this.props.navigation.goBack()}}>
                    <Icon name="chevron-left" size={30} color={'#FCC438'} />
                  </Button>
                </Col>
                <Col size={65} style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Title style={{color: 'black', textAlign: 'center', fontSize: 20}}>Send GYFTmoni</Title>
                </Col>
                <Col size={15} style={{justifyContent: 'center', alignItems: 'center'}}>
                </Col>
              </Row>
            </Grid>
          </View>
        {/* </Header> */}
        <Content padder>
          {this._renderLoader()}
          {this._renderSearchView()}
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  
});

const mapDispatchToProps = (dispatch) => {
  return {
    setSearchedGOM : (data) => {
      dispatch(setGOM(data))
    }
  }
}

export default connect (null,mapDispatchToProps)(Give);
