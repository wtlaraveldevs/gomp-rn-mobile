import React, {Component} from 'react';
import { 
  TouchableOpacity, Image, Text, View, Alert,
  Dimensions, FlatList, ActivityIndicator, SafeAreaView, ScrollView, Platform
} from 'react-native';
import { Container, Content, Header, Button, Form, 
    Input, Title, Item, Label, Textarea } from 'native-base';
import { connect } from 'react-redux';
import { config } from '../config/index';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { fetchWalletRecord } from '../redux/actions';
import Dialog, { DialogContent } from 'react-native-popup-dialog';

class Wallet extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            money: 0,
            hasData: false,
            kycStatus: "",
            bankAccount: '',
            loader: false,
            showWithdrawDialog: false,
            amount: 0
         }
    }

    componentWillMount(){
        this.props.getWallet({user_id: this.props.loggedUser.id});
    }


    componentWillReceiveProps(nextProps)
    {
        if(nextProps.fetchWallet){
            this.props.getWallet({user_id: this.props.loggedUser.id});
        }
        this.setState({
            money: nextProps.wallet.money,
            kycStatus: nextProps.wallet.kycStatus,
            bankAccount: nextProps.wallet.bankaccount_id,
            hasData: true
        })
    }

    _renderHeader(){
        return(
            <View style={{height: 60,}}>
                <Grid style={{alignItems: 'center', paddingHorizontal: 10}}>
                    <Row>
                        <Col size={15} style={{justifyContent: 'center', alignItems: 'center'}}>
                            <Button transparent onPress={()=>{
                                setTimeout(() => {
                                    this.props.navigation.goBack()
                                }, 50);
                            }}>
                            <Icon name="chevron-left" size={30} color={'#FCC438'} />
                            </Button>
                        </Col>
                        <Col size={65} style={{justifyContent: 'center', alignItems: 'center'}}>
                            <Text style={{fontSize: 20}}>GYFTmoni Pot</Text>
                        </Col>
                        <Col size={15} style={{justifyContent: 'center', alignItems: 'center'}}>
                        </Col>
                    </Row>
                </Grid>
            </View>
        )
    }

    initWithdrawal()
    {
        try{
            if(this.state.kycStatus != "SUCCEEDED")
            {
                alert("Please complete the KYC process first. Thank you.")
            }
            else if(this.state.bankAccount == "" || this.state.bankAccount == null)
            {
                alert("Please submit bank details before you initiate the withdrawal process.")
            }
            else if(this.state.amount < 1 || this.state.amount == 0)
            {
                alert("Please provide amount greater than 1.")
            }
            else {
                this.setState({ showWithdrawDialog: false, loader: true })

                var amount = parseFloat(this.state.amount) * 100;

                var data = {
                    user_id: this.props.loggedUser.id,
                    amount: amount
                }

                fetch(`${config.api.withdrawal}`, {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(data)
                })
                .then((response) => { return response.json() })
                .then((response) => {
                    this.setState({ loader: false })
                    if(response.status)
                    {
                        Alert.alert(
                            "",
                            "Withdrawal process has been initiated successfully. You will receive an email once the process is complete. Thank you.",
                            [
                              { text: "OK", onPress: () => this.props.navigation.pop() }
                            ],
                            { cancelable: false }
                        );
                    }
                    else {
                        Alert.alert(
                            "",
                            "Unable to initiate withdrawal process. Please try again or contact the GYFTmoni administration.",
                            [
                              {
                                text: "Cancel",
                                onPress: () => console.log("Cancel Pressed"),
                                style: "cancel"
                              },
                              { text: "OK", onPress: () => console.log("OK Pressed") }
                            ],
                            { cancelable: false }
                        );
                    }
                })
                .catch(err => {
                    this.setState({ loader: false })
                    alert(err);
                    console.log("Withdrawal Error",err);
                })
            }
        }
        catch(err)
        {
            console.log("Error in withdrawal", err);
        }
    }

    _renderLoader = () =>{
        return(
            <View>
                <Dialog
                visible={this.state.loader}
                dialogStyle={{width: 200, padding: 20}}
                >
                <DialogContent>
                    <ActivityIndicator size={"large"} color={"golden"} />
                    <Text style={{fontSize: 20}}>Please wait...</Text>
                </DialogContent>
                </Dialog>
            </View>
        )
    }

    _renderAmountDialog = () =>{
        return(
            <View>
                <Dialog
                visible={this.state.showWithdrawDialog}
                dialogStyle={{width: 300, paddingBottom: 20, paddingTop: 5}}
                >
                <DialogContent>
                    <TouchableOpacity style={{padding: 8, alignSelf: 'flex-end'}}
                    onPress={() => {
                        this.setState({ showWithdrawDialog: false })
                    }}>
                        <Icon name={"close"} color={"gray"} size={30} />
                    </TouchableOpacity>
                    <View style={{width: '100%', borderBottomColor: "#f6b93b", borderBottomWidth: 1, marginBottom: 15}}>
                        <Label style={{fontSize: 20, fontWeight: "bold", color: '#f6b93b'}}>Amount</Label>
                    </View>
                    <View>
                        <Text style={{fontSize: 16}}>Please provide the amount that you want to withdraw.</Text>
                        <Form>
                            <Item style={{marginLeft: 0, marginVertical: 10}}>
                                <View style={{borderColor: "#ddd", borderWidth: 1, paddingLeft: 10,
                                 flexDirection: 'row', alignItems:'center'}}>
                                    <Text style={{fontSize: 18}}>£</Text>
                                    <Input
                                        style={{color: 'black', paddingLeft: 20, paddingRight: 20 }}
                                        placeholder={"00.00"}
                                        onChangeText={(text) => { this.setState({amount: text}) }} 
                                    />
                                </View>
                            </Item>
                            <Button 
                            onPress={this.initWithdrawal.bind(this)} 
                            // onPress={() => { this.setState({showWithdrawDialog: false })}}
                            style={{backgroundColor: "#ff7f50",  justifyContent:'center', width: '100%', alignItems: 'center'}}>
                                <Text style={{color: 'white'}}>Withdraw</Text>
                            </Button>
                        </Form>
                        
                    </View>
                </DialogContent>
                </Dialog>
            </View>
        )
    }

    render() {
        return (
            <Container>
                <View style={{width: Dimensions.get('window').width, height: Platform.OS == "android" ? 60 : 85, backgroundColor: '#f6b93b', 
                justifyContent:'center', alignItems: 'flex-end', flexDirection:'row', 
                paddingBottom: 15,}}>
                    {/* <Image source={require('../assets/img/logo.png')} style={{width: 32, height: 32, paddingBottom: 3, 
                        marginRight: 4, tintColor: '#fff'}} /> */}
                    <Image source={require('../assets/img/complete_logo.png')} style={{width: 144, height: 20, tintColor: '#fff'}} />
                </View>
                <Content contentContainerStyle={{flex: 1}} >
                {this._renderHeader()}
                <ScrollView style={{flex: 1}}>
                {
                    this.state._initial == true ?
                    null :
                    this.state.hasData ? 
                    <View>
                        <View style={{marginHorizontal: 10, paddingHorizontal: 15, marginTop: 15}}>
                            <Text style={{fontSize: 14, textAlign: 'justify'}}>You must provide Proof Of Identification (passport or driving licence) 
                                and Bank Account details below before you can make a withdrawal.<Text style={{color: "red", fontSize: 14}}>Withdrawals may take up to 3 working days to reach your account.</Text>
                            </Text>
                        </View>
                        <TouchableOpacity onPress={() => { 
                            if(this.state.kycStatus == "IN_PROGRESS")
                            {
                                alert("You have submitted the document. We are validating it. It might take upto 24 hours to validate it.")
                            }
                            else if(this.state.kycStatus == "SUCCEEDED")
                            {
                                alert("You account is already validated.");
                            }
                            else{
                                this.props.navigation.navigate("UploadKyc") 
                            }
                            }}>
                            <View style={{flexDirection: 'row', borderWidth: 1, borderColor: '#ddd', margin: 10, paddingHorizontal: 15,
                            paddingVertical: 20, alignItems: 'center'}}>
                                <View style={{flex: 0.9, flexDirection: 'row' }}>
                                    <View style={{flex: 0.5, justifyContent:'center'}}>
                                        <Text>Proof of ID</Text>
                                        <Text style={{fontSize: 12, color: '#ddd'}}>Checking ID may take 24 hours.</Text>
                                    </View>
                                    <View style={{flex: 0.5, justifyContent:'center'}}>
                                        <Text style={{textAlign:'right'}}>{this.state.kycStatus == "IN_PROGRESS" ? "IN PROGRESS" : this.state.kycStatus == "SUCCEEDED" ? "COMPLETED" : "NOT VALIDATED"}</Text>
                                    </View>
                                </View>
                                <View style={{flex: 0.10, alignItems: 'flex-end'}}>
                                    <Icon name="chevron-right" size={30} color={'#FCC438'} />
                                </View>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => { 
                            if(!this.state.bankAccount){
                                this.props.navigation.navigate("SubmitBankInfo") 
                            }
                            else {
                                alert("You have already attach bank details to your account.")
                            }
                        }}>
                            <View style={{flexDirection: 'row', borderWidth: 1, borderColor: '#ddd', margin: 10, paddingHorizontal: 15,
                            paddingVertical: 20}}>
                                <View style={{flex: 0.9, flexDirection: 'row' }}>
                                    <View style={{flex: 0.5, justifyContent:'center'}}>
                                        <Text>Bank Account</Text>
                                    </View>
                                    <View style={{flex: 0.5, justifyContent:'center'}}>
                                        <Text style={{textAlign:'right'}}>{this.state.bankAccount == null ? "NO BANK ATTACHED" : this.state.bankAccount}</Text>
                                    </View>
                                </View>
                                <View style={{flex: 0.10, alignItems: 'flex-end'}}>
                                    <Icon name="chevron-right" size={30} color={'#FCC438'} />
                                </View>
                            </View>
                        </TouchableOpacity>
                        <View style={{margin: 10, height: Dimensions.get('screen').width/1.8, alignItems: 'center', borderWidth: 1, borderColor: '#ddd'}}>
                            <View style={{flex: 0.8, justifyContent:'center', alignItems: 'center' }}>
                                <Text style={{fontSize: 24, color: "#f6b93b"}}>Balance</Text>
                                <Text style={{fontSize: 60, color: "#f6b93b"}}>£ {this.state.money}</Text>
                            </View>
                            <View style={{flex: 0.2 }}>
                                <Button onPress={()=>{
                                    this.setState({showWithdrawDialog: true})
                                }} style={{backgroundColor: "#f6b93b", width: 250, justifyContent:'center', alignItems: 'center'}}>
                                    <Text style={{color: 'white'}}>Withdraw</Text>
                                </Button>
                            </View>
                        </View>
                    </View>
                    :
                    <View style={{width: Dimensions.get('screen').width, height: Dimensions.get('screen').height/1.5,
                    justifyContent: 'center', alignItems: 'center'}}>
                        {/* <View style={{borderWidth: 2, borderColor:'#f45302', padding: 25, borderRadius: 50, marginBottom: 10  }}>
                            <Icon name="heart-broken" size={50} color={'#f45302'} />
                        </View> */}
                        <ActivityIndicator size={"large"} color={"#f6b93b"} />
                        <Text style={{fontSize: 20, marginTop: 10}}>Fetching wallet</Text>
                    </View>
                }
                {this._renderLoader()}
                {this._renderAmountDialog()}
                </ScrollView>
                </Content>
            </Container>
        );
    }
}
 
const mapStateToProps = (state) => {
    return {
        wallet: state.wallet,
        loggedUser: state.loggedUser,
        fetchWallet: state.fetchWallet
    }
}
 
const mapDispatchToProps = dispatch => {
    return {
        getWallet: (data) =>{
            dispatch(fetchWalletRecord(data))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Wallet);