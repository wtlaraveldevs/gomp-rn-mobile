import { AsyncStorage } from "react-native";

const initialState = {
    selected_gom : '',
    isLoggedIn : false,
    loggedUser: null,
    apiStatus: false,
    apiMessage: '',
    apiCount: 0,
    openLoginView: false,
    favorites: [],
    goals: [],
    searchResult: [],
    redirectToFavorites: false,
    redirectToGoals: false,
    redirectToMyAccount: false,
    enableFavorite: 0,

    // account
    userInfo: '',
    registerAttempt: 0,
    regComplete: false,
    regResponse: '',
    updateResponse: '',
    fetchPots: false,

    // dashboard variables
    getAllGoals: [],
    thankyouMessage: '',
    getAllGifts: [],
    dashboard: [],

    newPot: false,
    potPurchase: '',

    // Wallet 
    wallet: [],

    // Bank
    bankResponse: '',
    fetchWallet: false,
    kycResponse: '',

    // countries
    countriesResponse: ''
};

export default  (state = initialState, action) => {
    switch(action.type){
        case 'LOGGED_USER':
            return {
                ...state,
                loggedUser: action.payload //JSON.parse(action.payload)
            }
        case 'LOGGED_OUT':
            return{
                ...state,
                loggedUser: null,
                isLoggedIn: false
            }
        case 'SEARCH_GOM':
            console.log("Object", action.payload);
            return{
                ...state,
                selected_gom: action.payload,
                newPot: false,
            }
        case 'SET_LOGIN':
            if(action.payload.status == 'success')
            {
                return{
                    ...state,
                    isLoggedIn: true,
                    loggedUser: action.payload.model,
                    apiStatus: action.payload.status,
                }
            }
            else{
                var count = parseInt(state.apiCount) + 1;
                return{
                    ...state,
                    isLoggedIn: false,
                    loggedUser: null,
                    apiStatus: action.payload.status,
                    apiMessage: action.payload.message,
                    apiCount: count,
                }
            }
        case 'HANDLE_POT_PURCHASE':
            return {
                ...state,
                potPurchase: action.payload
            }
        case 'API_RESPONSE':
            var count = parseInt(state.apiCount) + 1;
            return {
                ...state,
                apiStatus: action.payload.status,
                apiMessage: action.payload.message,
                apiCount: count
            }
        case 'SHOW_LOGIN':
            return{
                ...state,
                openLoginView: true
            }
        case 'HIDE_LOGIN':
            return{
                ...state,
                openLoginView: false
            }
        case 'HIDE_LOGIN':
            return{
                ...state,
                openLoginView: false
            }
        case 'STORE_FAVORITES':
            return{
                ...state,
                favorites: action.payload,
                redirectToFavorites: false
            }
        case 'REDIRECT_TO_FAVORITES':
            return{
                ...state,
                redirectToFavorites: true
            }
        case 'RESET_REDIRECTION_TO_FAVORITES':
            return {
                ...state,
                redirectToFavorites: false
            }
        case 'ENABLE_FAVORITE':
            return {
                ...state,
                enableFavorite: action.payload
            }
        case 'STORE_GOALS':
            return{
                ...state,
                goals: action.payload,
                fetchPots: false,
                redirectToGoals: false
            }
        case 'REDIRECT_TO_GOALS':
            return{
                ...state,
                redirectToGoals: true
            }
        case 'RESET_REDIRECT_TO_GOALS':
            return{
                ...state,
                redirectToGoals: false
            }
        case 'SEARCH':
            return{
                ...state,
                searchResult: action.payload
            }
        case 'POT_CREATED':
            var user = state.loggedUser;
            user["active_purchased_pot"] = '0';

            console.log("User", user);

            AsyncStorage.setItem('user', JSON.stringify(user));
            
            return {
                ...state,
                newPot: true,
                selected_gom: action.payload,
                loggedUser: user
            }
        case "UPDATE_POT": 
            if(action.payload.status == "success"){
                return {
                    ...state,
                    updateResponse: action.payload,
                    fetchPots: true,
                    selected_gom: action.payload.pot,
                    newPot: false,
                }
            }
            else{
                return {
                    ...state,
                    updateResponse: action.payload 
                }
            }
        case 'USER_INFO':
            return{
                ...state,
                userInfo: action.payload
            }
        case 'REDIRECT_TO_MYACCOUNT':
            return{
                ...state,
                redirectToMyAccount: true,
            }
        case 'RESET_REDIRECTION_MYACCOUNT':
            return{
                ...state,
                redirectToMyAccount: false,
            }
        case 'GET_ALL_GOALS':
            return {
                ...state,
                getAllGoals: action.payload
            }
        case 'GET_ALL_GIFTS':
            return {
                ...state,
                getAllGifts: action.payload.data,
                thankyouMessage: action.payload.thankyou
            }
        case 'UPDATE_THANKYOU':
            var user = state.loggedUser;
            user["thankyou_template"] = action.payload;

            console.log("User", user);

            AsyncStorage.setItem('user', JSON.stringify(user));

            return {
                ...state,
                thankyouMessage: action.payload,
                loggedUser: user
            }
        case 'DASHBOARD':
            return {
                ...state,
                dashboard: action.payload
            }
        case 'SET_REGISTRATION':
            var user = '';
            if(action.payload.status == true)
            {
                user = action.payload.user;
            }
            return{
                ...state,
                regResponse: action.payload,
                loggedUser: user
            }
            // return{
            //     ...state,
            //     registerAttempt: state.regComplete + 1,
            //     regComplete: true,
            //     loggedUser: action.payload
            // }
        case 'CLEAR_REGISTRATION':
            return{
                ...state,
                regResponse: ''
            }
        case 'FAILED_REGISTRATION':
            return{
                ...state,
                registerAttempt: state.regComplete + 1,
                regComplete: false,
            }
        case 'RESET_API_RESPONSE':
            return{
                ...state,
                apiStatus: false,
                apiMessage: ""
            }
        case 'ADD_AMT_TO_POT':
            var pot = state.selected_gom;
            pot["total"] = parseInt(pot["total"]) + parseInt(action.payload)
            return {
                ...state,
                selected_gom: pot,
                newPot: false,
            }
        case 'STORE_WALLET':
            return {
                ...state,
                wallet: action.payload,
                fetchWallet: false,
            }
        case 'BANK_DETAILS_RESPONSE':
            var w = state.wallet;
            if(action.payload.status)
            {
                w.bankaccount_id = action.payload.bankAccount;
            }
            return {
                ...state,
                wallet: w,
                bankResponse: action.payload,
                fetchWallet: true
            }
        case 'KYC_UPLOAD':
            var w = state.wallet;
            if(action.payload.status)
            {
                w.kycStatus = "IN_PROGRESS";
            }
            return {
                ...state,
                wallet: w,
                kycResponse: action.payload,
                fetchWallet: true
            }
        case 'COUNTRIES':
            return {
                ...state,
                countriesResponse: action.payload
            }
        default:
            return state;
    }
}