import { config } from '../../config/index';
import { AsyncStorage } from 'react-native'

export const setLoggedUser = (user) => {
    console.log("Setting user", user);
    return {
        type: 'LOGGED_USER',
        payload: user
    }
}

export const setGOM = (object) => {
    return {
        type: 'SEARCH_GOM',
        payload: object
    }
}

export const logout = () => {
    return {
        type: 'LOGGED_OUT',
    }
}

export const login = (data) => {
    console.log(config.api.login, data);
    return (dispatch) => {
        return fetch(`${config.api.login}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: data
        })
        .then(response=> { 
            console.log('Before JSON Return', response);
            return response.json(); 
        })
        .then((response) => {
            console.log(response);
            if(response.status == 'success'){
                AsyncStorage.setItem('isLoggedIn', "true");
                AsyncStorage.setItem('user', JSON.stringify(response.model));
            }
            // else{
            //     AsyncStorage.setItem('isLoggedIn', "false");
            //     var model = [];
            //     AsyncStorage.setItem('user', JSON.stringify(model));
            // }
            dispatch(setLogins(response));
        })
        .catch((error) => {
            console.error(error);
        });
    }
}

export const setLogins = (object) => {
    return {
        type: 'SET_LOGIN',
        payload: object
    }
}

export const getAllCountries = () => {
    return (dispatch) => {
        return fetch(`${config.api.countries}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({})
        })
        .then(response=> {
            return response.json(); 
        })
        .then((response) => {
            console.log(response);
            dispatch(setCountries(response));
        })
        .catch((error) => {
            console.error(error);
        });
    }
}

export const setCountries = (object) => {
    return {
        type: 'COUNTRIES',
        payload: object
    }
}

export const getUserInfo = (data) => {
    console.log(config.api.getUserInfo, data);
    return (dispatch) => {
        return fetch(`${config.api.getUserInfo}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data)
        })
        .then(response=> { 
            // console.log('Before JSON Return', response);
            return response.json(); 
        })
        .then((response) => {
            // console.log(response);
            if(response.status == 'success'){
                AsyncStorage.setItem('user', JSON.stringify(response.model));
            }
            else{
                AsyncStorage.setItem('user', null);
            }
            dispatch(setLoggedUser(response.model));
        })
        .catch((error) => {
            console.error(error);
        });
    }
}

export const register = (data) => {
    return (dispatch) => {
        return fetch(`${config.api.register}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data)
        })
        .then(response=> { return response.json(); })
        .then((response) => {
            console.log(response);
            if(response.status == true){
                AsyncStorage.setItem('isLoggedIn', "true");
                AsyncStorage.setItem('user', JSON.stringify(response.user));
                // dispatch(handleRegistration(response.user));
            }
            dispatch(setRegistration(response));
            // else{
            //     dispatch(handleFailedRegistration());
            // }
        })
        .catch((error) => {
            console.error(error);
        });
    }
}

export const setRegistration = (object) => {
    return {
        type: 'SET_REGISTRATION',
        payload: object
    }
}

export const clearRegistration = () => {
    return {
        type: 'CLEAR_REGISTRATION'
    }
}

export const handleRegistration = (object) => {
    return {
        type: 'SET_REGISTRATION',
        payload: object
    }
}


export const handleFailedRegistration = () => {
    return {
        type: 'FAILED_REGISTRATION',
    }
}

export const searchPot = (query) => {
    return (dispatch) => {
        return fetch(`${config.api.searchPot}`, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({ 
              text : this.state.searchText 
            })
          })
          .then(response=> { return response.json() })
          .then((response) => {
              if(response.status == 'success')
              {
                dispatch(setGOM(response.data.data[0]));
              }
              else{
                dispatch(ApiResponse(response));
              }
              
          })
          .catch((error) => {
            console.error(error);
          });
    }
}

export const search = (query) => {
    return (dispatch) => {
        return fetch(`${config.api.search}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(query)
        })
        .then(response=> { return response.json(); })
        .then((response) => {
            console.log(response);
            dispatch(handleSearchResult(response.data.data));
        })
        .catch((error) => {
            console.error(error);
        });
    }
}

const handleSearchResult = (data) =>{
    return{
        type: "SEARCH",
        payload: data
    }
}

export const buyPot = (data) => {
    console.log(data);
    return (dispatch) => {
        return fetch(`${config.api.buyGM}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                user_id: data.user_id,
                card: data.card,
                expiry: data.expiry,
                cvc: data.cvc
            })
        })
        .then(response=> { return response.json(); })
        .then((response) => {
            console.log(response);
            dispatch(handlePotPurchase(response));
        })
        .catch((error) => {
            console.error(error);
        });
    }
}

export const handlePotPurchase = (object) => {
    return {
        type: 'HANDLE_POT_PURCHASE',
        payload: object
    }
}

export const ApiResponse = (object) => {
    return {
        type: 'API_RESPONSE',
        payload: object
    }
}

export const resetApiResponse = () => {
    return {
        type: 'RESET_API_RESPONSE',
    }
}



export const payWithMangoPay = (data) => {
    console.log(data);
    return (dispatch) => {
        return fetch(`${config.api.payWithMp}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data)
        })
        .then(response=> { return response.json(); })
        .then((response) => {
            console.log(response);
            if(response.status == true)
            {
                dispatch(UpdatePot(data.amount))
            }
            dispatch(ApiResponse(response));
        })
        .catch((error) => {
            console.error(error);
        });
    }
}

export const showLogin = () => {
    return {
        type: 'SHOW_LOGIN',
    }
}

export const hideLogin = () => {
    return {
        type: 'HIDE_LOGIN',
    }
}

export const fetchFavorites = (data) => {
    return (dispatch) => {
        return fetch(`${config.api.getFavorites}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data)
        })
        .then(response=> { return response.json(); })
        .then((response) => {
            console.log(response);
            dispatch(storeFavorites(response.model));
        })
        .catch((error) => {
            console.error(error);
        });
    }
}

export const storeFavorites = (data) => {
    return {
        type: 'STORE_FAVORITES',
        payload: data
    }
}

export const redirectToFavorites = () => {
    return {
        type: 'REDIRECT_TO_FAVORITES'
    }
}

export const resetRedirectionToFavorites = () => {
    return {
        type: 'RESET_REDIRECTION_TO_FAVORITES'
    }
}

export const setFavorites = (data) => {
    return (dispatch) => {
        return fetch(`${config.api.setFavorites}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data)
        })
        .then(response=> { return response.json(); })
        .then((response) => {
            dispatch(enableFavourite(response));
        })
        .catch((error) => {
            console.error(error);
        });
    }
}

const enableFavourite = (data) => {
    return{
        type: 'ENABLE_FAVORITE',
        payload: data
    }
} 

export const redirectToGoals = () =>{
    return {
        type: 'REDIRECT_TO_GOALS'
    }
}

export const fetchWalletRecord = (data) => {
    return (dispatch) => {
        return fetch(`${config.api.getWallet}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data)
        })
        .then(response=> { return response.json(); })
        .then((response) => {
            console.log(response);
            dispatch(storeWalletInfo(response.data));
        })
        .catch((error) => {
            console.error(error);
        });
    }
}

export const storeWalletInfo = (data) => {
    return {
        type: 'STORE_WALLET',
        payload: data
    }
}

export const submitBankDetails = (data) => {
    return (dispatch) => {
        return fetch(`${config.api.submitBank}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data)
        })
        .then(response=> { return response.json(); })
        .then((response) => {
            console.log(response);
            dispatch(bankDetailsResponse(response));
        })
        .catch((error) => {
            console.error(error);
        });
    }
}

export const bankDetailsResponse = (data) => {
    return {
        type: 'BANK_DETAILS_RESPONSE',
        payload: data
    }
}

export const uploadKycDocument = (data) => {

    const formData = new FormData();
    formData.append("kyc_doc", {type: 'image/jpg', uri:data.kyc_doc, name:'uploaded.jpg'});
    formData.append("user_id", data.user_id);

    return (dispatch) => {
        return fetch(`${config.api.uploadKyc}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
            },
            body: formData
        })
        .then(response=> { return response.json(); })
        .then((response) => {
            console.log(response);
            dispatch(kycUploadResponse(response));
        })
        .catch((error) => {
            console.error(error);
        });
    }
}

export const kycUploadResponse = (data) => {
    return {
        type: 'KYC_UPLOAD',
        payload: data
    }
}

export const resetRedirectionToGoals = () =>{
    return {
        type: 'RESET_REDIRECT_TO_GOALS'
    }
}

export const getGoals = (data) => {
    return (dispatch) => {
        return fetch(`${config.api.getGoals}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data)
        })
        .then(response=> { return response.json(); })
        .then((response) => {
            console.log(response);
            dispatch(storeGoals(response.model));
        })
        .catch((error) => {
            console.error(error);
        });
    }
}

const storeGoals = (data) => {
    return {
        type: 'STORE_GOALS',
        payload: data
    }
}

export const redirectToMyAccount = () => {
    return {
        type: 'REDIRECT_TO_MYACCOUNT'
    }
}

export const resetRedirectionToMyAccount = () => {
    return {
        type: 'RESET_REDIRECTION_MYACCOUNT'
    }
}

export const getAccountInfo = (query) => {
    return (dispatch) => {
        return fetch(`${config.api.getAccount}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(query)
        })
        .then(response=> { return response.json(); })
        .then((response) => {
            console.log(response);
            dispatch(handleAccountInfo(response.data[0]));
        })
        .catch((error) => {
            console.error(error);
        });
    }
}

const handleAccountInfo = (data) =>{
    return{
        type: "USER_INFO",
        payload: data
    }
}

export const updateAccountInfo = (query) => {
    return (dispatch) => {
        return fetch(`${config.api.updateAccount}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(query)
        })
        .then(response=> { return response.json(); })
        .then((response) => {
            console.log(response);
            dispatch(handleUpdateAccountInfo(response.data.data));
        })
        .catch((error) => {
            console.error(error);
        });
    }
}

const handleUpdateAccountInfo = (data) =>{
    return{
        type: "USER_INFO",
        payload: data
    }
}

export const createPot = (data) => {
    
    const formData = new FormData();
    formData.append("title", data.title);
    formData.append("description", data.description);
    formData.append("photo", {type: 'image/jpg', uri:data.photo, name:'uploaded.jpg'});
    formData.append("location", data.location);
    formData.append("categories_id", data.categories_id);
    formData.append("user_id", data.user_id);

    console.log('FormData', formData);

    return (dispatch) => {
        return fetch(`${config.api.createPot}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
            },
            body: formData
        })
        .then(response=> { return response.json(); })
        .then((response) => {
            console.log(response);
            if(response.status == "success")
            {
                dispatch(sentPotCreation(response.pot[0]));
            }
            else{
                dispatch(ApiResponse(response))
            }
        })
        .catch((error) => {
            console.error(error);
        });
    }
}

const sentPotCreation = (data) => {
    return{
        type: 'POT_CREATED',
        payload: data
    }
}

export const updatePotDetails = (data) => {
    
    console.log('Photo', data.hasPhoto);

    const formData = new FormData();
    formData.append("id", data.id);
    formData.append("title", data.title);
    formData.append("description", data.description);
    if(data.hasPhoto){
        formData.append("photo", {type: 'image/jpg', uri:data.photo, name:'uploaded.jpg'});
    }
    formData.append("location", data.location);
    formData.append("categories_id", data.categories_id);
    formData.append("user_id", data.user_id);

    console.log('FormData', formData);

    return (dispatch) => {
        return fetch(`${config.api.updatePot}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
            },
            body: formData
        })
        .then(response=> { return response.json(); })
        .then((response) => {
            console.log(response);
            // if(response.status == "success")
            // {
            //     dispatch(updatePotResponse(response.pot[0]));
            // }
            // else{
            //     dispatch(updatePotResponse(response))
            // }
            dispatch(updatePotResponse(response));
        })
        .catch((error) => {
            console.error(error);
        });
    }
}

const updatePotResponse = (data) => {
    return{
        type: 'UPDATE_POT',
        payload: data
    }
}

export const getAllGoals = (query) => {
    return (dispatch) => {
        return fetch(`${config.api.getAllGoals}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(query)
        })
        .then(response=> { return response.json(); })
        .then((response) => {
            // console.log(response);
            if(response.status == true){
                dispatch(handleAllGoals(response.data));
            }
            
        })
        .catch((error) => {
            console.error(error);
        });
    }
}

const handleAllGoals = (data) => {
    return {
        type: 'GET_ALL_GOALS',
        payload: data
    }
}

export const getAllGifts = (query) => {
    return (dispatch) => {
        return fetch(`${config.api.getAllGifts}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(query)
        })
        .then(response=> { return response.json(); })
        .then((response) => {
            if(response.status == true){
                dispatch(handleAllGifts(response));
            }
            
        })
        .catch((error) => {
            console.error(error);
        });
    }
}

const handleAllGifts = (data) => {
    return {
        type: 'GET_ALL_GIFTS',
        payload: data
    }
}

export const updateThankyouTemplate = (message) => {
    return {
        type: 'UPDATE_THANKYOU',
        payload: message
    }
}

export const dashboard = (query) => {
    return (dispatch) => {
        return fetch(`${config.api.dashboard}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(query)
        })
        .then(response=> { return response.json(); })
        .then((response) => {
            if(response.status == true){
                dispatch(handleDashboard(response.data));
            }
            
        })
        .catch((error) => {
            console.error(error);
        });
    }
}

const handleDashboard = (data) => {
    return {
        type: 'DASHBOARD',
        payload: data
    }
}

export const UpdatePot = (amount) => {
    return {
        type: "ADD_AMT_TO_POT",
        payload: amount
    }
}