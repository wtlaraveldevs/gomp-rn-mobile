/*  File created specifically to add such constants that are used throughout the application so editing/changing them would be easy. Written by Ilya Friedman */

const config = {}

// Choose one or the other below: 
config.rootPath = 'https://gyftmoni.com'
config.LocalRootPath = 'http://192.168.10.6:8080'

// Large Image
config.largeImg = `${config.rootPath}/public/campaigns/large`

// Small Image
config.smallImg = `${config.rootPath}/public/campaigns/small`

// Authentication and Registration API
config.api = {}
config.api.root = `${config.rootPath}/api`

// Get Active Countries
config.api.countries = `${config.rootPath}/api/countries`

// Authentication
config.api.login = `${config.rootPath}/api/login`
config.api.register = `${config.rootPath}/api/register`
config.api.updateToken = `${config.rootPath}/api/updateToken`
config.api.getUserInfo = `${config.rootPath}/api/getUserInfo` // POST

config.api.getAccount = `${config.rootPath}/api/getAccount`
config.api.updateAccount = `${config.rootPath}/api/updateAccount`
config.api.search = `${config.api.root}/search`
config.api.searchPot = `${config.api.root}/searchPot`

config.api.buyGM = `${config.api.root}/purchase` // POST

// Has Active Pot
config.api.hasActivePot = `${config.api.root}/has_active_pot` // POST

config.api.payWithMp = `${config.api.root}/payWithMp` // POST

config.api.getFavorites = `${config.api.root}/getFavorites`
config.api.setFavorites = `${config.api.root}/setFavorites`

config.api.getGoals = `${config.api.root}/getGoals`

config.api.createPot = `${config.api.root}/creatPot` // POST
config.api.updatePot = `${config.api.root}/updatePot` // POST

// Dashboard Apis
config.api.getAllGoals = `${config.api.root}/getAllGoals`
config.api.getAllGifts = `${config.api.root}/getAllGifts`
config.api.dashboard = `${config.api.root}/dashboard`

// Wallet
config.api.getWallet = `${config.api.root}/getWalletRecord` // POST

// Bank Details
config.api.submitBank = `${config.api.root}/submit_bank_info` // POST

config.api.uploadKyc = `${config.api.root}/upload_kyc_api` // POST

config.api.withdrawal = `${config.api.root}/payout` // POST

config.api.thankyou = `${config.api.root}/saythankyou` // POST

config.api.updateThankyouFlag = `${config.api.root}/updateThankyouStatus` // POST

config.api.updateTemplate = `${config.api.root}/update_template` // POST

// forgot password 
config.api.forgotpassword = `${config.api.root}/forgotpassword/email` // POST

// token verification
config.api.tokenverification = `${config.api.root}/tokenVerification` // POST

// Reset password
config.api.resetpassword = `${config.api.root}/resetPassword` // POST

export default config
