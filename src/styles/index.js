import {StyleSheet, Platform} from 'react-native';

export default styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    heading: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    menuItem:{
        padding: 12,
        flex: 1,
        flexDirection: 'row'
    },
    divider:{
        borderWidth: 0.5,
        borderColor: '#d6d7da',
        marginTop: 10,
        marginBottom: 10,
    }
});