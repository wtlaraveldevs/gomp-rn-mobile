/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */   

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  SafeAreaView
} from 'react-native';
import { Container, StyleProvider, Root  } from "native-base";
import getTheme from './src/native-base-theme/components';
import material from './src/native-base-theme/variables/material';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reducers from './src/redux/reducers/index'; 
import {
  createStackNavigator, createAppContainer, 
  createDrawerNavigator,
} from 'react-navigation';
// import { DrawerActions } from 'react-navigation';
import thunk from 'redux-thunk'
import { fromRight, fromBottom } from 'react-navigation-transitions';

// Screens
import Main from './src/components/main';
import Give from './src/components/give';
import Login from './src/components/authencation/login';
import Search from './src/components/search/search';
import DrawerScreen from './src/components/drawer/DrawerScreeen';
import PotView from './src/components/potview';
import Payment from './src/components/payment';
import Favorites from './src/components/favorites';
import Goals from './src/components/goals';
import CreatePot from './src/components/createpot';
import EditPot from './src/components/editpot';
import Account from './src/components/account'
import Register from './src/components/authencation/register';
import Wallet from './src/components/wallet';
import SubmitBankInfo from './src/components/submitBankInfo';
import UploadKyc from './src/components/uploadKyc';
import ThankyouTemplate from './src/components/dashboard/thankyouTemplate';
import ForgotPassword from './src/components/authencation/forgotpassword';
import CodeVerification from './src/components/authencation/CodeVerification';
import ResetPassword from './src/components/authencation/ResetPassword';
import HowItWorks from './src/components/staticViews/howitworks';

// Dashboard Container.
import DashboardContainer from './src/components/dashboard/dashboardContainer'

import { YellowBox } from 'react-native';

YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);

console.disableYellowBox = true;

const store = createStore(reducers, applyMiddleware(thunk));


const DrawerStack = createDrawerNavigator({
  Main: { screen: Main },
  },
  {
    contentComponent: DrawerScreen,
    drawerWidth: 250
	}
)

// const MenuImage = ({navigation}) => {
//   if(!navigation.state.isDrawerOpen){
//       return <Image source={require('./src/images/menu-button.png')}/>
//   }else{
//       return <Image source={require('./src/images/left-arrow.png')}/>
//   }
// }

const handleCustomTransition = ({ scenes }) => {
  const prevScene = scenes[scenes.length - 2];
  const nextScene = scenes[scenes.length - 1];
 
  // Custom transitions go there
  if (prevScene
    && prevScene.route.routeName === 'PotView'
    && nextScene.route.routeName === 'Payment') {
    return fromBottom();
  }
  return fromRight();
}

const App = createStackNavigator(
  {
      // Main: { screen: Main }, 
      Register: { screen: Register },
      Give: { screen: Give }, 
      Login: { screen: Login },
      Search: {screen: Search},
      DrawerStack: { screen: DrawerStack },
      PotView : { screen: PotView },
      Payment: { screen: Payment },
      Favorites: { screen: Favorites },
      Goals: { screen: Goals },
      CreatePot: {screen: CreatePot},
      EditPot: { screen: EditPot },
      Account: { screen: Account },
      DashboardContainer: { screen: DashboardContainer },
      Wallet: { screen: Wallet },
      SubmitBankInfo: { screen: SubmitBankInfo },
      UploadKyc: { screen: UploadKyc },
      thankyouTemplate: { screen: ThankyouTemplate },
      forgotpassword: { screen: ForgotPassword },
      verificationcode: { screen: CodeVerification },
      resetpassword: { screen: ResetPassword },
      howitworks: { screen: HowItWorks }
  },
  {
      headerMode: "none",
      initialRouteName: "DrawerStack", // for live: 
      transitionConfig: (nav) => handleCustomTransition(nav)
	}
);

const AppContainer = createAppContainer(App);

export default () => (
  
  //<SafeAreaView style={{flex: 1, backgroundColor: "#2196F3"}}>
  <StyleProvider style={getTheme(material)}>
    <Container>
      <Provider store={store}>
        <Root>
          <AppContainer />
        </Root>
      </Provider>
    </Container>
  </StyleProvider>
  //</SafeAreaView>
);
